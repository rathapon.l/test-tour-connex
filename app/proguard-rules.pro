# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-dontobfuscate

# OkHttp
# JSR 305 annotations are for embedding nullability information.
-dontwarn javax.annotation.**
# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase
# Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn org.codehaus.mojo.animal_sniffer.*
# OkHttp platform used only on JVM and when Conscrypt dependency is available.
-dontwarn okhttp3.internal.platform.ConscryptPlatform

# Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

# Retrofit
-keepattributes Signature, InnerClasses
-keepclassmembers,allowshrinking,allowobfuscation interface * {
    @retrofit2.http.* <methods>;
}
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn javax.annotation.**
-dontwarn kotlin.Unit
-dontwarn retrofit2.-KotlinExtensions

# Gson
-keepattributes Signature
-keepattributes *Annotation*
-dontwarn sun.misc.**
-keep class com.google.gson.examples.android.model.** { *; }
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

#realtimeblurview
-keep class androidx.renderscript.** { *; }

#htmltextview
-dontwarn org.htmlcleaner.**

#chatkit
-keep class * extends com.stfalcon.chatkit.messages.MessageHolders$OutcomingTextMessageViewHolder {
     public <init>(android.view.View, java.lang.Object);
     public <init>(android.view.View);
 }
-keep class * extends com.stfalcon.chatkit.messages.MessageHolders$IncomingTextMessageViewHolder {
     public <init>(android.view.View, java.lang.Object);
     public <init>(android.view.View);
 }
-keep class * extends com.stfalcon.chatkit.messages.MessageHolders$IncomingImageMessageViewHolder {
     public <init>(android.view.View, java.lang.Object);
     public <init>(android.view.View);
 }
-keep class * extends com.stfalcon.chatkit.messages.MessageHolders$OutcomingImageMessageViewHolder {
     public <init>(android.view.View, java.lang.Object);
     public <init>(android.view.View);
 }

 #eventbus
 -keepattributes *Annotation*
 -keepclassmembers class * {
     @org.greenrobot.eventbus.Subscribe <methods>;
 }
 -keep enum org.greenrobot.eventbus.ThreadMode { *; }

 # Only required if you use AsyncExecutor
 -keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
     <init>(java.lang.Throwable);
 }

 # Line Because, ApisFragment update field value of LineApiClientBuilder with reflection API.
 -keep class com.linecorp.linesdk.api.LineApiClientBuilder { *;}
 -keep class org.spongycastle.crypto.* {*;}
 -keep class org.spongycastle.crypto.agreement.** {*;}
 -keep class org.spongycastle.crypto.digests.* {*;}
 -keep class org.spongycastle.crypto.ec.* {*;}
 -keep class org.spongycastle.crypto.encodings.* {*;}
 -keep class org.spongycastle.crypto.engines.* {*;}
 -keep class org.spongycastle.crypto.macs.* {*;}
 -keep class org.spongycastle.crypto.modes.* {*;}
 -keep class org.spongycastle.crypto.paddings.* {*;}
 -keep class org.spongycastle.crypto.params.* {*;}
 -keep class org.spongycastle.crypto.prng.* {*;}
 -keep class org.spongycastle.crypto.signers.* {*;}
 -keep class org.spongycastle.jcajce.provider.asymmetric.* {*;}
 -keep class org.spongycastle.jcajce.provider.asymmetric.util.* {*;}
 -keep class org.spongycastle.jcajce.provider.asymmetric.dh.* {*;}
 -keep class org.spongycastle.jcajce.provider.asymmetric.ec.* {*;}
 -keep class org.spongycastle.jcajce.provider.digest.** {*;}
 -keep class org.spongycastle.jcajce.provider.keystore.** {*;}
 -keep class org.spongycastle.jcajce.provider.symmetric.** {*;}
 -keep class org.spongycastle.jcajce.spec.* {*;}
 -keep class org.spongycastle.jce.** {*;}
 -keep class io.jsonwebtoken.** {*;}
 -dontwarn javax.naming.**

# PDFPreview
-keep class com.shockwave.**

# Matisse
-dontwarn com.bumptech.glide.**