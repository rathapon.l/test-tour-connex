package com.tourconnex.android

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import com.google.firebase.messaging.FirebaseMessaging
import com.tourconnex.android.domain.common.Currency
import com.tourconnex.android.domain.trip.Trip
import com.tourconnex.android.domain.trip.TripTraveller
import com.tourconnex.android.domain.users.User
import com.tourconnex.android.framework.preference.ConversationManagePreference
import com.tourconnex.android.framework.preference.ExchangePreference
import com.tourconnex.android.framework.preference.TripManagePreference
import com.tourconnex.android.framework.preference.UserPreference
import java.util.*
import android.R.attr.versionCode
import androidx.core.content.pm.PackageInfoCompat.getLongVersionCode
import android.os.Build

class TourConnex {
    companion object {
        private var papapay: TourConnex? = null

        /**
         * Gets session instance.
         */
        fun getInstance(): TourConnex {
            if (papapay == null) {
                papapay = TourConnex()
            }
            return papapay!!
        }

        fun getVersionName(activity: Activity?): String? {
            var versionName: String? = null
            try {
                val packageInfo = activity?.packageManager?.getPackageInfo(activity?.packageName, 0)
                packageInfo?.let {
                    var versionCode = 0
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        versionCode = packageInfo.longVersionCode.toInt()
                    } else {
                        versionCode = packageInfo.versionCode
                    }
                    //versionName = activity?.getString(R.string.more_app_version, it.versionName)
                    versionName = activity?.getString(R.string.more_app_version, "0.$versionCode")
                }
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }

            return versionName
        }
    }

    private lateinit var userPreference: UserPreference
    private lateinit var tripManagePreference: TripManagePreference
    private lateinit var conversationManagePreference: ConversationManagePreference
    private lateinit var exchangePreference: ExchangePreference
    private var locale: Locale? = null

    /**
     * Initial session app context.
     */
    fun init(context: Context) {
        userPreference = UserPreference(context)
        tripManagePreference = TripManagePreference(context)
        conversationManagePreference = ConversationManagePreference(context)
        exchangePreference = ExchangePreference(context)
    }

    /**`
     * Sets token.
     *
     * @param token the token
     */
    fun setToken(token: String?) {
        userPreference.setToken(token)
    }

    /**
     * Gets token.
     *
     * @return the token
     */
    fun getToken(): String? {
        return userPreference.getToken()
    }

    /**
     * Sets token.
     *
     * @param refreshToken the token
     */
    fun setRefreshToken(refreshToken: String?) {
        userPreference.setRefreshToken(refreshToken)
    }

    /**
     * Gets token.
     *
     * @return the refresh token
     */
    fun getRefreshToken(): String? {
        return userPreference.getRefreshToken()
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    fun setUser(user: User?) {
        userPreference.setUser(user)
        subscribeIfNeeded()
    }

    private fun subscribeIfNeeded() {
        if (getEnableNotification()) {
            getUser()?.let { FirebaseMessaging.getInstance().subscribeToTopic(it.id) }
        }
    }
    private fun unsubscribeUser() {
        getUser()?.let { FirebaseMessaging.getInstance().unsubscribeFromTopic(it.id) }
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    fun getUser(): User? {
        return userPreference.getUser()
    }

    fun setActiveTrip(trip: TripTraveller?) {
        tripManagePreference.setActiveTrip(trip, getUser()?.id)
    }

    fun getActiveTrip(): TripTraveller? {
        return tripManagePreference.getActiveTrip(getUser()?.id)
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    fun setLatestSeenMessageId(conversationId: String?, latestSeenMessageId: String?) {
        val user = getUser()
        conversationManagePreference.setLatestSeenMessageId(conversationId, user?.id, latestSeenMessageId)
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    fun getLatestSeenMessageId(conversationId: String?): String? {
        val user = getUser()
        return conversationManagePreference.getLatestSeenMessageId(conversationId, user?.id)
    }

    fun setBaseCurrency(currency: Currency) {
        exchangePreference.setBaseCurrency(currency)
    }

    fun getBaseCurrency(): Currency? {
        return exchangePreference.getBaseCurrency()
    }

    fun setResultCurrency(currency: Currency) {
        exchangePreference.setResultCurrency(currency)
    }

    fun getResultCurrency(): Currency? {
        return exchangePreference.getResultCurrency()
    }

    fun setEnableLocation(enableLocation: Boolean) {
        userPreference.setEnableLocation(enableLocation)
    }

    fun getEnableLocation(): Boolean {
        return userPreference.getEnableLocation()
    }

    fun setEnableNotification(enableNotification: Boolean) {
        userPreference.setEnableNotification(enableNotification)
        if (enableNotification) {
            subscribeIfNeeded()
        } else {
            unsubscribeUser()
        }
    }

    fun getEnableNotification():Boolean {
        return userPreference.getEnableNotification()
    }

    fun logout() {
        unsubscribeUser()
        userPreference.clear()
        tripManagePreference.clearCurrentUserActiveTrip()
        conversationManagePreference.clearCurrentConversation()
    }
}