package com.tourconnex.android.color

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Type
import kotlinx.android.synthetic.main.activity_color_selection.*
import kotlinx.android.synthetic.main.view_toolbar.*

interface OnColorSelectionActivityResult {
    fun onSelectColorType(colorType: Type?)
    fun onCancel()
}

class ColorSelectionActivity : AbstractActivity() {

    companion object {

        private const val RESULT_COLOR_TYPE = "RESULT_COLOR_TYPE"

        fun create(context: Context): Intent {
            return Intent(context, ColorSelectionActivity::class.java)
        }

        fun onActivityResult(resultCode: Int, data: Intent?, onColorSelectionActivityResult: OnColorSelectionActivityResult) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val colorType = data?.getParcelableExtra<Type>(RESULT_COLOR_TYPE)
                    onColorSelectionActivityResult.onSelectColorType(colorType)
                }
                Activity.RESULT_CANCELED -> {
                    onColorSelectionActivityResult.onCancel()
                }
            }
        }
    }

    private val adapter =  ColorAdapter {
       didSelectColorType(it)
    }

    private lateinit var colorTypes: ArrayList<Type>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_color_selection)

        setSupportActionBar(actionBarView)

        supportActionBar?.title = getString(R.string.colors_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recyclerView.adapter = adapter

        colorTypes = prepareColorTypes()
        adapter.addAll(colorTypes)
    }

    private fun prepareColorTypes(): ArrayList<Type> {
        var colorLists = ArrayList<Type>()
        colorLists.add(Type("#75A6F0", 1))
        colorLists.add(Type("#FDBBD6", 2))
        colorLists.add(Type("#FCE186", 3))
        colorLists.add(Type("#C1F17E", 4))
        colorLists.add(Type("#F07575", 5))

        return colorLists
    }

    private fun didSelectColorType(colorType: Type?) {
        setResult(Activity.RESULT_OK, Intent().apply {
            putExtra(RESULT_COLOR_TYPE, colorType)
        })

        finish()
    }

    private class ColorAdapter(val completionBlock: (Type?) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<TravellerViewHolder>() {
        val items = ArrayList<Type>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TravellerViewHolder {
            val viewHolder = TravellerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list_group_color, parent, false), completionBlock)

            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: TravellerViewHolder, position: Int) {
            holder.bindData(items[position])
        }

        fun addAll(entities: ArrayList<Type>?) {
            entities?.let {
                items.clear()
                items.addAll(it)
            }
            notifyDataSetChanged()
        }
    }

    private class TravellerViewHolder(itemView: View, completionBlock: ((Type?) -> Unit)) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        private var type: Type? = null

        init {
            itemView.setOnClickListener { _ ->
                type?.let {
                    completionBlock(it)
                }
            }
        }

        fun bindData(item: Type) {
            type = item
            item.name?.let {
                if (it.startsWith("#")) {
                    val color = Color.parseColor(it)
                    itemView.setBackgroundColor(color)
                }
            }

        }

    }
}
