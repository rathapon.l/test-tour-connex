package com.tourconnex.android.conversation

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder
import com.appunite.appunitevideoplayer.PlayerActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import com.stfalcon.chatkit.commons.ImageLoader
import com.stfalcon.chatkit.messages.MessageInput
import com.stfalcon.chatkit.messages.MessagesListAdapter
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.conversation.*
import com.tourconnex.android.domain.conversation.form.CreateMessageTextForm
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.framework.http.ProgressBarOwn
import kotlinx.android.synthetic.main.activity_conversation.*
import kotlinx.android.synthetic.main.view_toolbar.*
import retrofit2.Call
import retrofit2.Response
import com.stfalcon.chatkit.messages.MessageHolders
import com.tourconnex.android.conversation.adapter.*
import com.tourconnex.android.domain.users.Traveller
import com.tourconnex.android.domain.users.User
import com.tourconnex.android.framework.libs.Glide4Engine
import com.tourconnex.android.framework.manager.ConversationManager
import com.tourconnex.android.images.ImagePreviewActivity
import com.tourconnex.android.images.ImageViewPagerActivity
import com.tourconnex.android.user.GuideProfileActivity
import com.tourconnex.android.user.TravellerProfileActivity
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

interface OnConversationActivityResult {
    fun onReadMessage(message: Message?, conversation: Conversation?)
}

class ConversationActivity : AbstractActivity() {

    companion object {
        private const val CONVERSATION_ID_EXTRA = "CONVERSATION_ID_EXTRA"
        private const val CONVERSATION_TITLE_EXTRA = "CONVERSATION_TITLE_EXTRA"
        private const val CONVERSATION_ASSOCIATE_EXTRA = "CONVERSATION_ASSOCIATE_EXTRA"

        private const val TYPE_READ = 1

        private const val RESULT_MESSAGE = "RESULT_MESSAGE"
        private const val RESULT_CONVERSATION = "RESULT_CONVERSATION"
        private const val RESULT_TYPE = "RESULT_TYPE"

        private const val REQUEST_PHOTO_GALLERY = 112
        private const val REQUEST_CAMERA = 113
        private const val REQUEST_RECORD_VIDEO = 114
        private const val REQUEST_RECORD_AUDIO = 115

        private const val RC_CAMERA_AND_EXTERNAL_STORAGE = 164
        private const val RC_VIDEO_CAMERA_AND_EXTERNAL_STORAGE = 165
        private const val RC_EXTERNAL_STORAGE = 166
        private const val RC_AUDIO_RECORD = 167

        private const val MIME_VIDEO = "video/*"
        private const val MIME_IMAGE = "image/*"
        private const val MIME_AUDIO = "audio/*"

        fun create(
            context: Context,
            conversationId: String?,
            title: String?,
            users: ArrayList<Traveller>?
        ): Intent {
            return Intent(context, ConversationActivity::class.java).apply {
                putExtra(CONVERSATION_ID_EXTRA, conversationId)
                putParcelableArrayListExtra(CONVERSATION_ASSOCIATE_EXTRA, users)
                putExtra(CONVERSATION_TITLE_EXTRA, title)
            }
        }

        fun onActivityResult(
            resultCode: Int,
            data: Intent?,
            onConversationActivityResult: OnConversationActivityResult
        ) {
            if (resultCode == Activity.RESULT_OK) {
                val type = data?.getIntExtra(RESULT_TYPE, TYPE_READ) ?: TYPE_READ
                val conversation = data?.getParcelableExtra<Conversation>(RESULT_CONVERSATION)
                val message = data?.getParcelableExtra<Message>(RESULT_MESSAGE)

                when (type) {
                    TYPE_READ -> {
                        onConversationActivityResult.onReadMessage(message, conversation)
                    }
                }
            }
        }
    }

    lateinit var conversationId: String
    private var title: String? = null
    private var users: ArrayList<Traveller>? = null

    private var conversation: Conversation? = null

    private var callConversation: Call<Conversation>? = null
    private var callCreateMessage: Call<Message>? = null
    private var callMessages: Call<PageMessage>? = null

    private var adapter: MessagesListAdapter<TourTextMessage>? = null

    private var ids = ArrayList<String>()
    private var nextPageId: String? = null

    private var audioPath: String? = null
    private var isNeedToRefresh = false

    private val imageLoader = ImageLoader { imageView, url, payload ->
        imageView?.let {
            Picasso.get().load(url).into(imageView)
        }
    }

    private val attachListener = object : MessageInput.AttachmentsListener {
        override fun onAddAttachments() {
            openAttachments()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_conversation)

        conversationId = intent?.getStringExtra(CONVERSATION_ID_EXTRA) ?: ""
        title = intent?.getStringExtra(CONVERSATION_TITLE_EXTRA)
        users = intent?.getParcelableArrayListExtra<Traveller>(CONVERSATION_ASSOCIATE_EXTRA)

        setSupportActionBar(actionBarView)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = title


        val holdersConfig = MessageHolders()
            .setIncomingTextConfig(
                IncomingMessageViewHolder::class.java,
                R.layout.custom_incoming_text_message,
                null
            )
            .setOutcomingTextConfig(
                OutcomingMessageViewHolder::class.java,
                R.layout.custom_outcoming_text_message,
                null
            )
            .setIncomingImageConfig(
                ImageMessageViewHolder::class.java,
                R.layout.custom_incoming_image_message,
                null
            )
            .setOutcomingImageConfig(
                ImageMessageOutComingViewHolder::class.java,
                R.layout.custom_outcoming_image_message,
                null
            )

        val contentChecker = object : MessageHolders.ContentChecker<TourTextMessage> {
            override fun hasContentFor(message: TourTextMessage?, type: Byte): Boolean {
                when (type) {
                    MessageType.VOICE.toByte() -> {
                        return (message?.message?.type?.toInt() == MessageType.VOICE && message?.message?.audio != null)
                    }

                    MessageType.VIDEO.toByte() -> {
                        return (message?.message?.type?.toInt() == MessageType.VIDEO && message?.message?.video != null)
                    }
                }

                return false
            }
        }

        holdersConfig.registerContentType(
            MessageType.VOICE.toByte(),
            VoiceMessageViewHolder::class.java,
            R.layout.custom_incoming_voice_message,
            VoiceMessageOutComingViewHolder::class.java,
            R.layout.custom_outcoming_voice_message,
            contentChecker
        )

        holdersConfig.registerContentType(
            MessageType.VIDEO.toByte(),
            VideoMessageViewHolder::class.java,
            R.layout.custom_incoming_video_message,
            VideoMessageOutComingViewHolder::class.java,
            R.layout.custom_outcoming_video_message,
            contentChecker
        )

        val userId = TourConnex.getInstance().getUser()?.id ?: ""
        adapter = MessagesListAdapter(userId, holdersConfig, imageLoader)
        messagesList.setAdapter(adapter)

        adapter?.setLoadMoreListener { _, _ ->
            if (nextPageId != null) {
                fetchMessage(nextPageId)
            }
        }

        adapter?.setOnMessageClickListener {
            handleMessageClick(it)
        }

        adapter?.setDateHeadersFormatter(MessageHeaderDateFormat(this))

        adapter?.registerViewClickListener(R.id.messageUserAvatar) { view, message ->
            if (message.traveller?.guideType == 1 || message?.traveller?.user?.isGuide == true) {
                message?.traveller?.let {
                    openGuide(it)
                }
            } else {
                message?.traveller?.let {
                    openTraverll(it)
                }
            }
        }

        input.setInputListener { input ->
            createMessage(input.toString())
            true
        }

        input.setAttachmentsListener(attachListener)

        fetchConversation()
    }


    override fun onStart() {
        if (isNeedToRefresh) {
            isNeedToRefresh = false
            getFreshMessage()
        } else {
            isNeedToRefresh = true
        }

        ConversationManager.getInstance().currentConversationId = conversationId
        super.onStart()
    }

    override fun onStop() {
        ConversationManager.getInstance().currentConversationId = null
        super.onStop()
    }

    private fun fetchConversation() {
        callConversation?.cancel()
        callConversation = getApiClient().getConversation(conversationId)
        callConversation?.enqueue(object : MainThreadCallback<Conversation>(this) {
            override fun onSuccess(result: Conversation?) {
                this@ConversationActivity.conversation = result
                fetchMessage(null)
            }
        })
    }

    private fun fetchMessage(nextPageId: String?) {
        fetchMessagesWithNoHandle(nextPageId, nextPageId == null) { nextPageId, pageMessage ->
            pageMessage?.entities?.let { messages ->
                val tourMessages = messages.map {
                    val user = getUserFromSenderId(it.senderId)
                    TourTextMessage(
                        user,
                        it,
                        user?.user?.getFullName(),
                        user?.user?.profilePicture?.thumbnailImageUrl
                    )
                }
                adapter?.addToEnd(tourMessages, false)
                ids.addAll(tourMessages.map { it.id })
                if (nextPageId == null) {
                    tourMessages?.firstOrNull()?.let {
                        readMessage(it.message)
                    }
                }
            }

            this.nextPageId = pageMessage?.pageInformation?.nextPageId
        }
    }

    private fun getUserFromSenderId(senderId: String?): Traveller? {
        return users?.filter { it.user?.id == senderId }?.firstOrNull()
    }

    private fun handleMessageClick(message: TourTextMessage) {
        when (message.message.type?.toInt()) {
            MessageType.IMAGE -> {
                startActivity(ImageViewPagerActivity.create(this, conversationId, message.message))
            }
            MessageType.VOICE -> {
                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.setDataAndType(Uri.parse(message.message.audio?.fileUrl ?: ""), "audio/*")
                startActivity(intent)
            }

            MessageType.VIDEO -> {
                startActivity(
                    PlayerActivity.getVideoPlayerIntent(
                        this,
                        message.message.video?.fileUrl ?: "",
                        "Video", R.drawable.ic_playback
                    )
                )
            }
        }
    }

    private fun fetchMessagesWithNoHandle(
        nextPageId: String?,
        showLoading: Boolean,
        completionBlock: ((String?, PageMessage?) -> Unit)
    ) {
        var loading: ProgressBarOwn? = null
        if (showLoading) {
            loading = simpleProgressBar
        }

        callMessages?.cancel()
        callMessages = getApiClient().getConversationMessages(conversationId, nextPageId)
        callMessages?.enqueue(object : MainThreadCallback<PageMessage>(this, loading) {
            override fun onSuccess(result: PageMessage?) {
                completionBlock?.let {
                    it(nextPageId, result)
                }
            }

            override fun onError(response: Response<PageMessage>?, error: Error?) {

            }
        })
    }

    private fun createMessage(text: String?) {
        val form = CreateMessageTextForm(1, text)

        callCreateMessage?.cancel()
        callCreateMessage = getApiClient().createMessageTextConversation(conversationId, form)
        callCreateMessage?.enqueue(object : MainThreadCallback<Message>(this, simpleProgressBar) {
            override fun onSuccess(result: Message?) {
                result?.let {
                    didCreateMessage(it)
                }
            }

            override fun onError(response: Response<Message>?, error: Error?) {

            }
        })
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun getConversationRetriveEvent(s: ConversationRetriveEvent) {
        getFreshMessage()
    }

    private fun getFreshMessage() {
        fetchMessagesWithNoHandle(null, false) { nextPageId, pageMessage ->
            pageMessage?.entities?.let { messages ->
                val tourMessages = messages.map {
                    val user = getUserFromSenderId(it.senderId)
                    TourTextMessage(
                        user,
                        it,
                        user?.user?.getFullName(),
                        user?.user?.profilePicture?.thumbnailImageUrl
                    )
                }
                val filteredMessages = tourMessages.filter { !ids.contains(it.id) }
                filteredMessages?.forEach {
                    adapter?.addToStart(it, true)
                    ids.add(it.id)
                }

                filteredMessages?.firstOrNull()?.let {
                    readMessage(it.message)
                }
            }
        }
    }

    private fun readMessage(message: Message?) {
        setResult(Activity.RESULT_OK, Intent().apply {
            putExtra(RESULT_CONVERSATION, conversation)
            putExtra(RESULT_MESSAGE, message)
        })

        TourConnex.getInstance().setLatestSeenMessageId(conversationId, message?.id)
    }

    private val onImagePickedCallback = object : DefaultCallback() {
        override fun onImagesPicked(
            files: MutableList<File>,
            p1: EasyImage.ImageSource?,
            type: Int
        ) {
            val f = files.firstOrNull()
            f?.let {
                when (type) {
                    REQUEST_CAMERA -> didSelectFile(it, MessageType.IMAGE)
                    REQUEST_RECORD_VIDEO -> didSelectFile(it, MessageType.VIDEO)
                }
            }
        }
    }

    private fun didSelectFile(file: File, type: Int) {
        createMediaMessage(file, type)
    }

    private fun createMediaMessage(file: File, type: Int) {
        val messaegType = RequestBody.create(MediaType.parse("text/plain"), type.toString())
        var mimeType = MIME_AUDIO
        var thumbnailMultipartBody: MultipartBody.Part? = null
        when (type) {
            MessageType.VIDEO -> {
                mimeType = MIME_VIDEO
                val thumbnailFile = getThumbnailFile(file)
                val body = RequestBody.create(MediaType.parse(MIME_IMAGE), thumbnailFile)
                thumbnailMultipartBody =
                    MultipartBody.Part.createFormData("image", thumbnailFile?.name, body)
            }
            MessageType.VOICE -> {
                mimeType = MIME_AUDIO
            }
            MessageType.IMAGE -> {
                mimeType = MIME_IMAGE
            }
        }

        val file = createRequestBody(file, mimeType)
        callCreateMessage?.cancel()
        callCreateMessage = getApiClient().createMediaMessageConversation(
            conversationId,
            messaegType,
            file,
            thumbnailMultipartBody
        )
        callCreateMessage?.enqueue(object : MainThreadCallback<Message>(this, simpleProgressBar) {
            override fun onSuccess(result: Message?) {
                result?.let {
                    didCreateMessage(it)
                }
            }

            override fun onError(response: Response<Message>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    private fun getThumbnailFile(file: File): File? {
        val newFile = File(this.cacheDir, "videoThumbnail")
        newFile.createNewFile()

        val bitmap = ThumbnailUtils.createVideoThumbnail(
            file.absolutePath,
            MediaStore.Video.Thumbnails.MINI_KIND
        )

        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bos)
        val bitmapdata = bos.toByteArray()

        val fos = FileOutputStream(newFile)
        fos.write(bitmapdata)
        fos.flush()
        fos.close()

        return newFile
    }

    private fun didCreateMessage(message: Message) {
        val item = TourTextMessage(null, message, null, null)
        adapter?.addToStart(item, true)
        ids.add(item.id)
        readMessage(message)
    }

    private fun openTraverll(traveller: Traveller) {
        val activeTrip = TourConnex.getInstance().getActiveTrip()
        startActivity(
            TravellerProfileActivity.create(
                this,
                traveller,
                activeTrip?.trip?.id,
                traveller.id,
                null
            )
        )
    }

    private fun openGuide(guide: Traveller) {
        val activeTrip = TourConnex.getInstance().getActiveTrip()
        startActivity(GuideProfileActivity.create(this, guide, activeTrip?.trip?.id))
    }

    private fun openAttachments() {
        val builder = AlertDialog.Builder(this)
        builder.setItems(R.array.document_selection_option) { dialog, which ->
            when (which) {
                0 -> openCamera()
                1 -> openGallery()
                2 -> openVideo()
                3 -> openRecordAudio()
            }
            dialog.dismiss()
        }
        builder.show()
    }

    private fun createRequestBody(file: File, mimeType: String): MultipartBody.Part {
        val body = RequestBody.create(MediaType.parse(mimeType), file)
        return MultipartBody.Part.createFormData("file", file.name, body)
    }

    @AfterPermissionGranted(RC_EXTERNAL_STORAGE)
    private fun openGallery() {
        val perms = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            Matisse.from(this)
                .choose(MimeType.ofAll())
                .countable(false)
                .maxSelectable(1)
                .imageEngine(Glide4Engine())
                .forResult(REQUEST_PHOTO_GALLERY)
//            val getIntent = Intent(Intent.ACTION_GET_CONTENT)
//            getIntent.type = "image/* video/*"
//
//            val pickIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//            pickIntent.type = "image/* video/*"
//
//            val chooserIntent = Intent.createChooser(getIntent, "Select Image")
//            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))
//
//        startActivityForResult(chooserIntent, REQUEST_PHOTO_GALLERY)
        } else {
            EasyPermissions.requestPermissions(
                this, getString(R.string.message_camera_permission),
                RC_EXTERNAL_STORAGE, *perms
            )
        }
    }


    @AfterPermissionGranted(RC_AUDIO_RECORD)
    private fun openRecordAudio() {
        val perms =
            arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            audioPath = "${Environment.getExternalStorageDirectory()}/recorded_audio.wav"
            val color = ResourcesCompat.getColor(resources, R.color.colorPrimary, null)
            val requestCode = REQUEST_RECORD_AUDIO
            AndroidAudioRecorder.with(this)
                // Required
                .setFilePath(audioPath ?: "")
                .setColor(color)
                .setRequestCode(requestCode)


                // Start recording
                .record()
        } else {
            EasyPermissions.requestPermissions(
                this, getString(R.string.message_audio_permission),
                RC_AUDIO_RECORD, *perms
            )
        }

    }

    @AfterPermissionGranted(RC_VIDEO_CAMERA_AND_EXTERNAL_STORAGE)
    private fun openVideo() {
        val perms = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            EasyImage.openCameraForVideo(this, REQUEST_RECORD_VIDEO)
        } else {
            EasyPermissions.requestPermissions(
                this, getString(R.string.message_camera_permission),
                RC_VIDEO_CAMERA_AND_EXTERNAL_STORAGE, *perms
            )
        }
    }

    @AfterPermissionGranted(RC_CAMERA_AND_EXTERNAL_STORAGE)
    private fun openCamera() {
        val perms = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            EasyImage.openCameraForImage(this, REQUEST_CAMERA)
        } else {
            EasyPermissions.requestPermissions(
                this, getString(R.string.message_camera_permission),
                RC_CAMERA_AND_EXTERNAL_STORAGE, *perms
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_PHOTO_GALLERY -> {
                if (data != null) {
                    val selectedUri = Matisse.obtainResult(data).first()
                    val columns =
                        arrayOf(MediaStore.Images.Media.DATA, MediaStore.Images.Media.MIME_TYPE)
                    selectedUri?.let {
                        val cursor = contentResolver.query(it, columns, null, null, null)
                        cursor?.moveToFirst()

                        val pathColumnIndex = cursor?.getColumnIndex(columns[0]) ?: -1
                        val mimeTypeColumnIndex = cursor?.getColumnIndex(columns[1]) ?: -1

                        var contentPath: String? = null
                        if (pathColumnIndex != -1) {
                            contentPath = cursor?.getString(pathColumnIndex)
                        }
                        var mimeType: String? = null
                        if (mimeTypeColumnIndex != -1) {
                            mimeType = cursor?.getString(mimeTypeColumnIndex)
                        }

                        cursor?.close()

                        if (mimeType?.startsWith("image") == true) {
                            didSelectFile(File(contentPath ?: ""), MessageType.IMAGE)
                        } else if (mimeType?.startsWith("video") == true) {
                            didSelectFile(File(contentPath ?: ""), MessageType.VIDEO)
                        }
                    }
                }
            }

            REQUEST_RECORD_AUDIO -> {
                if (resultCode == Activity.RESULT_OK) {
                    val file = File(audioPath)
                    didSelectFile(file, MessageType.VOICE)
                }
            }
        }

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, onImagePickedCallback)
    }

    override fun onDestroy() {
        callConversation?.cancel()
        callCreateMessage?.cancel()
        callMessages?.cancel()
        super.onDestroy()
    }
}
