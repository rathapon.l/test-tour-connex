package com.tourconnex.android.conversation.adapter

import android.graphics.Bitmap
import android.graphics.Canvas
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.stfalcon.chatkit.messages.MessageHolders
import com.stfalcon.chatkit.utils.RoundedImageView
import com.tourconnex.android.R
import com.tourconnex.android.domain.conversation.TourTextMessage
import kotlinx.android.synthetic.main.custom_incoming_image_message.view.*

open class ImageMessageViewHolder(itemView: View, payload: Any?) : MessageHolders.IncomingImageMessageViewHolder<TourTextMessage>(itemView, payload){

    override fun onBind(message: TourTextMessage) {
        super.onBind(message)

       // var overlayImageView = itemView.findViewById<RoundedImageView>(R.id.overlayImageView)

        itemView.image.setCorners(0, R.dimen.message_bubble_corners_radius, R.dimen.message_bubble_corners_radius, R.dimen.message_bubble_corners_radius)
//        overlayImageView.scaleType = ImageView.ScaleType.FIT_XY

        itemView.usernameTextView.text = message.username
        //overlayImageView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.dark_transparent))
        //Glide.with(itemView.context).load(message?.message.images?.firstOrNull()?.largeImageUrl).into(image)
    }
}

open class ImageMessageOutComingViewHolder(itemView: View, payload: Any?) : MessageHolders.OutcomingImageMessageViewHolder<TourTextMessage>(itemView, payload){

    override fun onBind(message: TourTextMessage) {
        super.onBind(message)

        itemView.image.setCorners(R.dimen.message_bubble_corners_radius, 0, R.dimen.message_bubble_corners_radius, R.dimen.message_bubble_corners_radius)

//        var overlayImageView = itemView.findViewById<RoundedImageView>(R.id.overlayImageView)
//        overlayImageView.setCorners(R.dimen.message_bubble_corners_radius, R.dimen.message_bubble_corners_radius, 0, R.dimen.message_bubble_corners_radius)
//        overlayImageView.scaleType = ImageView.ScaleType.FIT_XY
//        val bmp = Bitmap.createBitmap(1000,1000, Bitmap.Config.ARGB_8888)
//        val canvas = Canvas(bmp)
//        canvas.drawColor(ContextCompat.getColor(itemView.context, R.color.dark_transparent))
//        overlayImageView.setImageBitmap(bmp)
        //overlayImageView.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.dark_transparent))
       // Glide.with(itemView.context).load(message?.message.images?.firstOrNull()?.largeImageUrl).into(image)
    }
}
