package com.tourconnex.android.conversation.adapter

import android.view.View
import com.stfalcon.chatkit.messages.MessageHolders
import com.tourconnex.android.domain.conversation.TourTextMessage
import kotlinx.android.synthetic.main.custom_incoming_text_message.view.*

open class IncomingMessageViewHolder(itemView: View, payload: Any?) : MessageHolders.IncomingTextMessageViewHolder<TourTextMessage>(itemView, payload){

    override fun onBind(message: TourTextMessage) {
        super.onBind(message)

//        time.setText(message.getStatus() + " " + time.text)
        itemView.usernameTextView.text = message.username


    }
}

open class OutcomingMessageViewHolder(itemView: View, payload: Any?) : MessageHolders.OutcomingTextMessageViewHolder<TourTextMessage>(itemView, payload){

    override fun onBind(message: TourTextMessage) {
        super.onBind(message)

    }
}