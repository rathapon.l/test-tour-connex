package com.tourconnex.android.conversation.adapter

import android.view.View
import com.stfalcon.chatkit.messages.MessageHolders
import com.tourconnex.android.domain.conversation.TourTextMessage
import kotlinx.android.synthetic.main.custom_incoming_voice_message.view.*

open class VoiceMessageViewHolder(itemView: View, payload: Any?) : MessageHolders.IncomingTextMessageViewHolder<TourTextMessage>(itemView, payload){

    override fun onBind(message: TourTextMessage) {
        super.onBind(message)

        itemView.usernameTextView.text = message.username
        //time.setText(message.getStatus() + " " + time.text)
    }
}

open class VoiceMessageOutComingViewHolder(itemView: View, payload: Any?) : MessageHolders.OutcomingTextMessageViewHolder<TourTextMessage>(itemView, payload){

    override fun onBind(message: TourTextMessage) {
        super.onBind(message)

        //time.setText(message.getStatus() + " " + time.text)
    }
}