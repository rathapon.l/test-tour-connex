package com.tourconnex.android.domain.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.webkit.URLUtil
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.tourconnex.android.R
import com.tourconnex.android.domain.api.ApiClient
import com.tourconnex.android.framework.http.ProgressBarOwn
import com.tourconnex.android.framework.services.ServiceLocator
import com.tourconnex.android.domain.common.Error
import androidx.fragment.app.Fragment
import com.tourconnex.android.MainActivity
import com.tourconnex.android.TourConnex
import com.tourconnex.android.conversation.ConversationActivity
import com.tourconnex.android.domain.notification.NotificationType
import com.tourconnex.android.domain.users.User
import com.tourconnex.android.framework.manager.ConversationManager
import com.tourconnex.android.framework.messaging.DisplayInAppNotificationEvent
import com.tourconnex.android.sos.GuideSOSActivity
import com.zeugmasolutions.localehelper.LocaleHelperActivityDelegateImpl
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.aviran.cookiebar2.CookieBar
import java.util.*
import kotlin.collections.ArrayList
import android.media.RingtoneManager
import com.tourconnex.android.domain.users.Traveller


@SuppressLint("Registered")
open class AbstractActivity : AppCompatActivity() {

    private val localeDelegate = LocaleHelperActivityDelegateImpl()

    protected fun getApiClient(): ApiClient {
        return ServiceLocator.getInstance().getService(ApiClient::class.java)
    }

    protected val simpleProgressBar: ProgressBarOwn by lazy<ProgressBarOwn> {
        return@lazy object : ProgressBarOwn {
            override fun show() {
                progressDialog.show()
            }

            override fun hide() {
                progressDialog.dismiss()
            }
        }
    }

    private val progressDialog: AlertDialog by lazy {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Processing...")
        builder.setCancelable(false)
        return@lazy builder.create()
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun showErrorDialog(error: Error?, completionBlock :(() -> Unit)? = null) {
        showMessageDialog(error?.message)
    }

    fun showMessageDialog(message: String?, completionBlock :(() -> Unit)? = null) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(message)
        builder.setPositiveButton(R.string.common_ok) { dialog, _ ->
            completionBlock?.let {
                it()
            }
            dialog.dismiss()
        }
        builder.show()
    }

    fun finishWithoutAnimation() {
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        finish()
    }

    protected fun addFragment(containerViewId: Int, fragment: Fragment, fragmentTag: String) {
        supportFragmentManager
            .beginTransaction()
            .add(containerViewId, fragment, fragmentTag)
            .disallowAddToBackStack()
            .commit()
    }

    protected fun replaceFragment(containerViewId: Int, fragment: Fragment, fragmentTag: String, backStackStateName: String?) {
        supportFragmentManager
            .beginTransaction()
            .replace(containerViewId, fragment, fragmentTag)
            .addToBackStack(backStackStateName)
            .commit()
    }

    protected fun removeFragmentIfNeeded(fragmentTag: String) {
        val fragment = supportFragmentManager.findFragmentByTag(fragmentTag)
        fragment?.let {
            supportFragmentManager.beginTransaction().remove(it).commit()
        }
    }

    fun openWebView(url: String) {
        val builder = CustomTabsIntent.Builder()
        builder.setToolbarColor(ContextCompat.getColor(this, R.color.white))
        val customTabsIntent = builder.build()
        var urlToParse = url
        if (!url.startsWith("http://") || !url.startsWith("https://")) {
            urlToParse = "http://$url"
        }

        if (URLUtil.isValidUrl(urlToParse)) {
            customTabsIntent.launchUrl(this, Uri.parse(urlToParse))
        }
    }

    override fun onStart() {
        EventBus.getDefault().register(this)
        super.onStart()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(localeDelegate.attachBaseContext(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        localeDelegate.onCreate(this)
    }

    override fun onResume() {
        super.onResume()
        localeDelegate.onResumed(this)
    }

    override fun onPause() {
        super.onPause()
        localeDelegate.onPaused()
    }

    open fun updateLocale(locale: Locale) {
        localeDelegate.setLocale(this, locale)
    }

    override fun recreate() {
        // override because no need to recreate
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun getInAppNotificationEvent(s: DisplayInAppNotificationEvent) {
        val remoteMessage = s.remoteMessage
        val contentId = s.contentId
        val type = s.type
        CookieBar.build(this)
            .setTitle(remoteMessage?.notification?.title)
            .setTitleColor(R.color.colorPrimary)
            .setIcon(R.mipmap.ic_launcher)
            .setMessage(remoteMessage?.notification?.body)
            .setMessageColor(R.color.textBlack)
            .setBackgroundColor(R.color.colorPrimaryLight)
            .setActionColor(R.color.colorPrimary)
            .setAction(R.string.common_view) {
                CookieBar.dismiss(this)

                if (!contentId.isNullOrEmpty()) {
                    onHandleNotificationType(type, contentId)
                }
            }
            .setEnableAutoDismiss(false)
            .setSwipeToDismiss(true)
            .show()

        try {
            val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val r = RingtoneManager.getRingtone(applicationContext, notification)
            r.play()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        handleIntent(intent)
    }

    fun handleIntent(intent: Intent?) {
        val contentId = intent?.getStringExtra("content_id")
        val type = intent?.getIntExtra("type", -1) ?: -1
        if (contentId != null && type != -1) {
            onHandleNotificationType(type, contentId)
        }
    }

    private fun onHandleNotificationType(type: Int, contentId: String) {
        when (type) {
            NotificationType.CONVERSATION_VIDEO, NotificationType.CONVERSATION_AUDIO, NotificationType.CONVERSATION_IMAGE, NotificationType.CONVERSATION_TEXT -> {
                handleConversationType(type, contentId)
            }
            NotificationType.ANNOUNCEMENT -> {
                handleAnnouncement()
            }
            NotificationType.SOS -> {
                if (TourConnex.getInstance().getUser()?.isGuide == true) {
                    openSOSList()
                }
            }
            else -> {

            }
        }
    }


    fun openSOSList() {
        startActivity(GuideSOSActivity.create(this))
    }


    private fun handleAnnouncement() {
        val mainActivity = this as? MainActivity
        mainActivity?.gotoTabId(R.id.item_chat)
    }

    private fun handleConversationType(type: Int, contentId: String) {
        if (ConversationManager.getInstance().currentConversationId != contentId) {
            val activeTrip = TourConnex.getInstance().getActiveTrip()
            ConversationManager.getInstance().getConversationAssociateItems(this, activeTrip?.id ?: "") { conversationAssociateItem, error ->

                if (conversationAssociateItem != null) {
                    val associateUsers = ArrayList<Traveller>()
                    conversationAssociateItem.guides?.let { associateUsers.addAll(it.mapNotNull { it }) }
                    conversationAssociateItem.travellers?.let { associateUsers.addAll(it.mapNotNull { it }) }

                    startActivity(ConversationActivity.create(this, contentId, "", associateUsers))
                } else if (error != null) {
                    showErrorDialog(error)
                }

            }

        }
    }
}