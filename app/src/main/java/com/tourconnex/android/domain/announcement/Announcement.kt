package com.tourconnex.android.domain.announcement

import android.os.Parcel
import android.os.Parcelable
import java.util.*

data class Announcement(
    val id: String?,
    val updatedAt: Date?,
    val createdAt: Date?,
    val message: String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readSerializable() as? Date,
        parcel.readSerializable() as? Date,
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeSerializable(updatedAt)
        parcel.writeSerializable(createdAt)
        parcel.writeString(message)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Announcement> {
        override fun createFromParcel(parcel: Parcel): Announcement {
            return Announcement(parcel)
        }

        override fun newArray(size: Int): Array<Announcement?> {
            return arrayOfNulls(size)
        }
    }
}