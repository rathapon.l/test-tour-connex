package com.tourconnex.android.domain.api

import com.tourconnex.android.domain.announcement.Announcement
import com.tourconnex.android.domain.announcement.PageAnnouncement
import com.tourconnex.android.domain.announcement.form.CreateAnnouncementForm
import com.tourconnex.android.domain.common.Hotel
import com.tourconnex.android.domain.common.LoginResponse
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.common.form.UpdateUserTripLocationForm
import com.tourconnex.android.domain.conversation.Conversation
import com.tourconnex.android.domain.conversation.Message
import com.tourconnex.android.domain.conversation.PageConversation
import com.tourconnex.android.domain.conversation.PageMessage
import com.tourconnex.android.domain.conversation.form.CreateConversationForm
import com.tourconnex.android.domain.conversation.form.CreateMessageTextForm
import com.tourconnex.android.domain.exchange.ExchangeRateResponse
import com.tourconnex.android.domain.schedule.PageSchedule
import com.tourconnex.android.domain.schedule.form.CreateScheduleForm
import com.tourconnex.android.domain.traveller.TravellerCheckInForm
import com.tourconnex.android.domain.traveller.form.SetTravellerGroupColorForm
import com.tourconnex.android.domain.trip.*
import com.tourconnex.android.domain.trip.form.*
import com.tourconnex.android.domain.users.PageGuide
import com.tourconnex.android.domain.users.PageTraveller
import com.tourconnex.android.domain.users.Traveller
import com.tourconnex.android.domain.users.User
import com.tourconnex.android.domain.users.form.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiClient {

    @GET("me")
    fun getMe(): Call<User>

    @POST("guest/login")
    fun loginWithFacebook(@Body form: LoginForm): Call<LoginResponse>

    @POST("guest/login")
    fun loginWithEmail(@Body form: LoginWithEmailForm): Call<LoginResponse>

    @Multipart
    @POST("guest/register")
    fun register(
        @Part("first_name") firstName: RequestBody,
        @Part("last_name") lastName: RequestBody,
        @Part("email") email: RequestBody,
        @Part("password") password: RequestBody,
        @Part image: MultipartBody.Part?
    ): Call<LoginResponse>

    @GET("trips")
    fun getTrips(@Query("citizen_id") citizedId: String?, @Query("passport_number") passportNumber: String?): Call<TripTravellerPage>

    @GET("trips/{tripId}")
    fun getTrip(@Path("tripId") tripId: String): Call<TripTraveller>

    @GET("trips/{tripId}/preview")
    fun getTripPreview(@Path("tripId") tripId: String): Call<TripTraveller>

    @GET("trips/{tripId}/guides")
    fun getTripGuides(@Path("tripId") tripId: String): Call<PageGuide>

    @GET("trips/{tripId}/travellers")
    fun getTripTravellers(@Path("tripId") tripId: String): Call<PageTraveller>

    @GET("travellers/{travellerId}")
    fun getTraveller(@Path("travellerId") travellerId: String?): Call<Traveller>

    @GET("users/{userId}")
    fun getUser(@Path("userId") userId: String): Call<User>

    @GET("hotels/{hotelId}")
    fun getHotel(@Path("hotelId") hotelId: String?): Call<Hotel>

    @POST("trips/{tripId}/location")
    fun updateUserTripLocation(@Path("tripId") tripId: String?, @Body form: UpdateUserTripLocationForm): Call<SimpleResponse>

    @POST("trips/{tripId}/sos")
    fun sendSOS(@Path("tripId") tripId: String?, @Body form: SendSOSForm): Call<SOS>

    @DELETE("trips/{tripId}/sos/{sosId}")
    fun cancelSOS(@Path("tripId") tripId: String?, @Path("sosId") sosId: String?): Call<SimpleResponse>

    @GET("trips/{tripId}/sos")
    fun getSOS(@Path("tripId") tripId: String?, @Query("state") state: Int): Call<PageSOS>

    @GET("me/sos")
    fun getMySOS(@Query("trip_id") tripId: String?): Call<SOS>

    @POST("trips/{tripId}/sos/{sosId}/action")
    fun takeActionSos(@Path("tripId") tripId: String?, @Path("sosId") sosId: String?): Call<SimpleResponse>

    @GET("conversations")
    fun getConversations(@Query("trip_id") tripId: String?): Call<PageConversation>

    @GET("conversations/{conversationsId}")
    fun getConversation(@Path("conversationsId") conversationsId: String?): Call<Conversation>

    @GET("conversations/{conversationId}/messages")
    fun getConversationMessages(
        @Path("conversationId") conversationId: String?, @Query("next_page_id") nextPageId: String? = null, @Query(
            "size"
        ) size: Int? = 20
    ): Call<PageMessage>

    @GET("conversations/{conversationId}/messages")
    fun getConversationMessagesByType(
        @Path("conversationId") conversationId: String?,
        @Query("conversation_type") conversationType: Int,
        @Query("next_page_id") nextPageId: String? = null,
        @Query("reverse") reverse: Boolean? = null,
        @Query("size") size: Int? = 20
    ): Call<PageMessage>

    @POST("conversations/{conversationId}/messages")
    fun createMessageTextConversation(@Path("conversationId") conversationId: String?, @Body form: CreateMessageTextForm): Call<Message>

    @Multipart
    @POST("conversations/{conversationId}/messages")
    fun createMediaMessageConversation(
        @Path("conversationId") conversationId: String?,
        @Part("message_type") key: RequestBody,
        @Part file: MultipartBody.Part,
        @Part thumbnail: MultipartBody.Part?
    ): Call<Message>


    @POST("conversations")
    fun createConversation(@Body form: CreateConversationForm): Call<Conversation>

    @GET("exchange")
    fun getExchangeRates(): Call<ExchangeRateResponse>

    @POST("trips/{tripId}/manageSOS")
    fun manageSOS(@Path("tripId") tripId: String?, @Body form: ManageSOSForm): Call<SimpleResponse>

    @POST("trips/{tripId}/manageRating")
    fun manageRating(@Path("tripId") tripId: String?, @Body form: ManageSOSForm): Call<SimpleResponse>

    @POST("trips/{tripId}/checkIn")
    fun checkIn(@Path("tripId") tripId: String?, @Body form: CheckInForm): Call<SimpleResponse>

    @POST("trips/{tripId}/travellers/{travellerId}/checkin")
    fun travellerCheckIn(@Path("tripId") tripId: String?, @Path("travellerId") travellerId: String?, @Body form: TravellerCheckInForm): Call<SimpleResponse>

    @POST("trips/{tripId}/travellers/{travellerId}/color")
    fun setTravellerGroupColor(@Path("tripId") tripId: String?, @Path("travellerId") travellerId: String?, @Body form: SetTravellerGroupColorForm): Call<SimpleResponse>

    @GET("trips/{tripId}/feedback")
    fun getFeedback(@Path("tripId") tripId: String?): Call<PageFeedback>

    @POST("trips/{travellerId}/feedback")
    @Headers("Content-Type: application/json")
    fun sendFeedback(@Path("travellerId") travellerId: String?, @Body form: SendFeedbackForm): Call<SimpleResponse>

    @PATCH("me")
    fun editProfile(@Body form: EditProfileForm): Call<User>

    @POST("trips/{tripId}/announcements")
    fun createAnnouncement(@Path("tripId") tripId: String?, @Body form: CreateAnnouncementForm): Call<Announcement>

    @GET("trips/{tripId}/announcements")
    fun getAnnouncements(@Path("tripId") tripId: String?): Call<PageAnnouncement>

    @DELETE("trips/{tripId}/announcements/{announcementId}")
    fun deleteAnnouncement(@Path("tripId") tripId: String?, @Path("announcementId") announcementId: String?): Call<SimpleResponse>

    @POST("trips/{tripId}/push")
    fun setSchedule(@Path("tripId") tripId: String?, @Body form: CreateScheduleForm): Call<SimpleResponse>

    @GET("trips/{tripId}/push")
    fun getSchedules(@Path("tripId") tripId: String?): Call<PageSchedule>

    @DELETE("trips/{tripId}/push/{pushId}")
    fun deleteSchedule(@Path("tripId") tripId: String?, @Path("pushId") pushId: String?): Call<SimpleResponse>

    @PUT("trips/{tripId}/days/{dayId}/rooms")
    @Headers("Content-Type: application/json")
    fun editRoom(@Path("tripId") tripId: String?, @Path("dayId") dayId: Int?, @Body form: EditRoomForm): Call<SimpleResponse>

    @POST("guest/resetPassword")
    fun resetPassword(@Body form: ResetPasswordForm): Call<SimpleResponse>

    @PATCH("me")
    fun editGuideProfile(@Body form: EditGuideProfileForm): Call<User>

    @Multipart
    @PATCH("me")
    fun editGuideProfileImage(@Part image: MultipartBody.Part?): Call<User>
}
