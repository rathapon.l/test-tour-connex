package com.tourconnex.android.domain.common

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


data class Currency(
    val symbol: String?,
    val name: String?,
    @SerializedName("symbol_native") val symbolNative: String?,
    @SerializedName("decimal_digits") val decimalDigits: Int?,
    val rounding: Double?,
    val code: String?,
    @SerializedName("name_plural") val namePlural: String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readValue(Double::class.java.classLoader) as? Double,
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(symbol)
        parcel.writeString(name)
        parcel.writeString(symbolNative)
        parcel.writeValue(decimalDigits)
        parcel.writeValue(rounding)
        parcel.writeString(code)
        parcel.writeString(namePlural)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Currency> {
        override fun createFromParcel(parcel: Parcel): Currency {
            return Currency(parcel)
        }

        override fun newArray(size: Int): Array<Currency?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return name ?: ""//"${code ?: ""}(${symbol ?: ""})" ?: ""
    }
}
