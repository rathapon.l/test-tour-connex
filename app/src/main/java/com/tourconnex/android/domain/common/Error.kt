package com.tourconnex.android.domain.common

data class Error(val code: Int?, val message: String?)