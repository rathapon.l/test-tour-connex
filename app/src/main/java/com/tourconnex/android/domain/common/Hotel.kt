package com.tourconnex.android.domain.common

import android.os.Parcel
import android.os.Parcelable
import com.tourconnex.android.domain.trip.Image
import java.util.*

data class Hotel(
    var id: String?,
    var name: String?,
    var description: String?,
    var address: String?,
    var phoneNumber: String?,
    var coverPhoto: Image?,
    var mainPhoto: Image?,
    var updatedAt: Date?,
    var createdAt: Date?,
    var location: Location?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(Image::class.java.classLoader),
        parcel.readParcelable(Image::class.java.classLoader),
        parcel.readSerializable() as? Date,
        parcel.readSerializable() as? Date,
        parcel.readParcelable(Location::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(address)
        parcel.writeString(phoneNumber)
        parcel.writeParcelable(coverPhoto, flags)
        parcel.writeParcelable(mainPhoto, flags)
        parcel.writeSerializable(updatedAt)
        parcel.writeSerializable(createdAt)
        parcel.writeParcelable(location, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Hotel> {
        override fun createFromParcel(parcel: Parcel): Hotel {
            return Hotel(parcel)
        }

        override fun newArray(size: Int): Array<Hotel?> {
            return arrayOfNulls(size)
        }
    }
}