package com.tourconnex.android.domain.common

import android.media.session.MediaSession
import android.os.Parcel
import android.os.Parcelable
import com.tourconnex.android.domain.users.User

data class LoginResponse(val token: Token?, val user: User?) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readParcelable(MediaSession.Token::class.java.classLoader),
        parcel.readParcelable(User::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(token, flags)
        parcel.writeParcelable(user, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LoginResponse> {
        override fun createFromParcel(parcel: Parcel): LoginResponse {
            return LoginResponse(parcel)
        }

        override fun newArray(size: Int): Array<LoginResponse?> {
            return arrayOfNulls(size)
        }
    }
}