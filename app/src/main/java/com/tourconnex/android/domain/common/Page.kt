package com.tourconnex.android.domain.common

open class Page<T> {
    var pageInformation: PageInformation? = null
    var entities: ArrayList<T>? = null
}