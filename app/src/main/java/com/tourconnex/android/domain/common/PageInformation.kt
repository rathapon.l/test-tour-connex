package com.tourconnex.android.domain.common

import android.os.Parcel
import android.os.Parcelable

data class PageInformation(val size: Int,
                           val numberOfItems: Int,
                           val nextPageId: String?): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(size)
        parcel.writeInt(numberOfItems)
        parcel.writeString(nextPageId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PageInformation> {
        override fun createFromParcel(parcel: Parcel): PageInformation {
            return PageInformation(parcel)
        }

        override fun newArray(size: Int): Array<PageInformation?> {
            return arrayOfNulls(size)
        }
    }
}