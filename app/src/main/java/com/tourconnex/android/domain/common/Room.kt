package com.tourconnex.android.domain.common

import android.os.Parcel
import android.os.Parcelable
import com.tourconnex.android.domain.traveller.TravellerRoom

data class Room(
    var roomNumber: String?,
    var remark: String?,
    var travellers: List<TravellerRoom>?,
    var lestestRoomNumber: String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.createTypedArrayList(TravellerRoom),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(roomNumber)
        parcel.writeString(remark)
        parcel.writeTypedList(travellers)
        parcel.writeString(lestestRoomNumber)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Room> {
        override fun createFromParcel(parcel: Parcel): Room {
            return Room(parcel)
        }

        override fun newArray(size: Int): Array<Room?> {
            return arrayOfNulls(size)
        }
    }
}