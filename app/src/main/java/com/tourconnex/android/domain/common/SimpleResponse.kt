package com.tourconnex.android.domain.common

data class SimpleResponse(val message: String)