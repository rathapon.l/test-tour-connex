package com.tourconnex.android.domain.common.form

data class UpdateUserTripLocationForm(
    val latitude: Double?,
    val longitude: Double?
)