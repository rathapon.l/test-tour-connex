package com.tourconnex.android.domain.conversation

import android.os.Parcel
import android.os.Parcelable

data class Conversation(
    val id: String?,
    val updatedAt: String?,
    val createdAt: String?,
    val tripId: String?,
    val name: String?,
    var type: Int?,
    var userIds: ArrayList<String>?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readSerializable() as? ArrayList<String>
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(updatedAt)
        parcel.writeString(createdAt)
        parcel.writeString(tripId)
        parcel.writeString(name)
        parcel.writeValue(type)
        parcel.writeSerializable(userIds)
    }

    override fun describeContents(): Int {
        return 0
    }

    fun getConversationType(): Int {
        when (type) {
            1 -> return CONVERSATION_TYPE_ONE_ON_ONE
            2 -> return  CONVERSATION_TYPE_GROUP
        }

        return CONVERSATION_TYPE_UNKNOWN
    }

    companion object CREATOR : Parcelable.Creator<Conversation> {
        const val CONVERSATION_TYPE_UNKNOWN     = 0
        const val CONVERSATION_TYPE_ONE_ON_ONE  = 1
        const val CONVERSATION_TYPE_GROUP       = 2

        override fun createFromParcel(parcel: Parcel): Conversation {
            return Conversation(parcel)
        }

        override fun newArray(size: Int): Array<Conversation?> {
            return arrayOfNulls(size)
        }
    }
}
