package com.tourconnex.android.domain.conversation

import com.tourconnex.android.domain.users.Guide
import com.tourconnex.android.domain.users.Traveller

data class ConversationAssociteItem(
    val travellers: List<Traveller>?,
    val guides: List<Traveller>?
)