package com.tourconnex.android.domain.conversation

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.messaging.RemoteMessage

data class ConversationRetriveEvent(
    val message: RemoteMessage?
)