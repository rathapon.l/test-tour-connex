package com.tourconnex.android.domain.conversation

import android.os.Parcel
import android.os.Parcelable
import com.tourconnex.android.domain.common.Audio
import com.tourconnex.android.domain.common.FileItem
import com.tourconnex.android.domain.common.Video
import com.tourconnex.android.domain.trip.Image
import java.util.*

data class Message(val id: String?,
                   val updatedAt: Date?,
                   val createdAt: Date?,
                   val senderId: String?,
                   val type: Long?,
                   val message: String?,
                   val video: Video?,
                   var audio: Audio?,
                   val images: ArrayList<Image>?): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readSerializable() as? Date,
        parcel.readSerializable() as? Date,
        parcel.readString(),
        parcel.readValue(Long::class.java.classLoader) as? Long,
        parcel.readString(),
        parcel.readParcelable(Video::class.java.classLoader) as? Video,
        parcel.readParcelable(Audio::class.java.classLoader) as? Audio,
        parcel.readArrayList(Image::class.java.classLoader) as? ArrayList<Image>
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeSerializable(updatedAt)
        parcel.writeSerializable(createdAt)
        parcel.writeString(senderId)
        parcel.writeValue(type)
        parcel.writeString(message)
        parcel.writeParcelable(audio, flags)
        parcel.writeParcelable(video, flags)
        parcel.writeList(images)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Message> {
        override fun createFromParcel(parcel: Parcel): Message {
            return Message(parcel)
        }

        override fun newArray(size: Int): Array<Message?> {
            return arrayOfNulls(size)
        }
    }
}