package com.tourconnex.android.domain.conversation

import android.content.Context
import com.stfalcon.chatkit.utils.DateFormatter
import com.tourconnex.android.R
import java.util.*

data class MessageHeaderDateFormat(val context: Context): DateFormatter.Formatter {
    override fun format(date: Date?): String {
        if (DateFormatter.isToday(date)) {
            return context.getString(R.string.message_today_date_title)
        } else if (DateFormatter.isYesterday(date)) {
            return context.getString(R.string.message_yesterday_date_title)
        } else {
            return DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH_YEAR)
        }
    }
}