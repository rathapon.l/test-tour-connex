package com.tourconnex.android.domain.conversation

class MessageType {
    companion object {
        const val TEXT          = 1
        const val IMAGE         = 2
        const val VIDEO         = 3
        const val VOICE         = 4
    }
}