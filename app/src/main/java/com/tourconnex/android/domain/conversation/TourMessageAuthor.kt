package com.tourconnex.android.domain.conversation

import com.stfalcon.chatkit.commons.models.IUser

data class TourMessageAuthor(
        val profileImage: String?,
        val senderId: String?,
        val username: String?
        ): IUser {
    override fun getAvatar(): String {
        return profileImage ?: ""
    }

    override fun getName(): String {
        return username ?: ""
    }

    override fun getId(): String {
        return senderId ?: ""
    }
}