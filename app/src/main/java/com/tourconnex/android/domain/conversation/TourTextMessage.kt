package com.tourconnex.android.domain.conversation

import com.stfalcon.chatkit.commons.models.IMessage
import com.stfalcon.chatkit.commons.models.IUser
import com.stfalcon.chatkit.commons.models.MessageContentType
import com.tourconnex.android.domain.common.FileItem
import com.tourconnex.android.domain.users.Traveller
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class TourTextMessage(
    val traveller: Traveller?,
    val message: Message,
    val username: String?,
    val profileImage: String?
) : IMessage, MessageContentType, MessageContentType.Image {

    override fun getImageUrl(): String? {
        return if (message.type?.toInt() == MessageType.IMAGE) message.images?.firstOrNull()?.largeImageUrl else null
    }

    override fun getId(): String {
        return message.id ?: ""
    }

    override fun getCreatedAt(): Date {
        val date = message.createdAt ?: Date()
        var dateString = toISO8601UTC(date)
        var convertedDate = fromISO8601UTC(dateString)
        return convertedDate ?: Date()
    }

    override fun getUser(): IUser {
        return TourMessageAuthor(profileImage, message.senderId, username)
    }

    override fun getText(): String {
        return message.message ?: ""
    }

    fun toISO8601UTC(date: Date): String {
        val tz = TimeZone.getDefault()
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")
        df.timeZone = tz
        return df.format(date)
    }

    fun fromISO8601UTC(dateStr: String): Date? {
        val tz = TimeZone.getTimeZone("GMT")
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")
        df.timeZone = tz

        try {
            return df.parse(dateStr)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return null
    }
}