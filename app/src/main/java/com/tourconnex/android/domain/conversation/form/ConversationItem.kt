package com.tourconnex.android.domain.conversation.form

import com.tourconnex.android.domain.conversation.Conversation

class ConversationItem() {
    var conversiontion: Conversation? = null
    var displayname: String? = null
    var displayImageUrl: String? = null
    var isFetchtedLatestMessage = false
    var fetchingCounter = 0
    var latestMessage: String? = null
    var numberOfUnreadMessage = 0
}