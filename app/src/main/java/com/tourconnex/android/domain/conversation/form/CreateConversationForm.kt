package com.tourconnex.android.domain.conversation.form

data class CreateConversationForm(
    val tripId: String?,
    var userId: String?,
    var isGuide: Boolean
)