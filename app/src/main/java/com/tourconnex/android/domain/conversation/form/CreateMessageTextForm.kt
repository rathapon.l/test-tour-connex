package com.tourconnex.android.domain.conversation.form

data class CreateMessageTextForm(
    val messageType: Int,
    val message: String?
)