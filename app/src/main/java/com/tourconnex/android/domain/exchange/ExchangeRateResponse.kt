package com.tourconnex.android.domain.exchange

import android.os.Parcel
import android.os.Parcelable
import com.tourconnex.android.domain.common.ExchangeRate
import java.util.*
import kotlin.collections.ArrayList

data class ExchangeRateResponse(
    val exchangeRates: ArrayList<ExchangeRate>?,
    val lastUpdate: Date?,
    val powerBy: String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.createTypedArrayList(ExchangeRate),
        parcel.readSerializable() as? Date,
        parcel.readString()
    )
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(exchangeRates)
        parcel.writeSerializable(lastUpdate)
        parcel.writeString(powerBy)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ExchangeRateResponse> {
        override fun createFromParcel(parcel: Parcel): ExchangeRateResponse {
            return ExchangeRateResponse(parcel)
        }

        override fun newArray(size: Int): Array<ExchangeRateResponse?> {
            return arrayOfNulls(size)
        }
    }
}
