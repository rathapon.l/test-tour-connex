package com.tourconnex.android.domain.flight

import android.os.Parcel
import android.os.Parcelable

data class FlightInfo(val departSeat: String?, val returnSeat: String?): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(departSeat)
        parcel.writeString(returnSeat)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FlightInfo> {
        override fun createFromParcel(parcel: Parcel): FlightInfo {
            return FlightInfo(parcel)
        }

        override fun newArray(size: Int): Array<FlightInfo?> {
            return arrayOfNulls(size)
        }
    }
}