package com.tourconnex.android.domain.flight

data class FlightItem(val flight: Flight?, val isDepartureFight: Boolean, val flightInfo: FlightInfo?)