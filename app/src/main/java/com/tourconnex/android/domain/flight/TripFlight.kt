package com.tourconnex.android.domain.flight

import android.os.Parcel
import android.os.Parcelable

data class TripFlight(
    val departFlights: List<Flight>?,
    val returnFlights: List<Flight>?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.createTypedArrayList(Flight),
        parcel.createTypedArrayList(Flight)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(departFlights)
        parcel.writeTypedList(returnFlights)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TripFlight> {
        override fun createFromParcel(parcel: Parcel): TripFlight {
            return TripFlight(parcel)
        }

        override fun newArray(size: Int): Array<TripFlight?> {
            return arrayOfNulls(size)
        }
    }
}