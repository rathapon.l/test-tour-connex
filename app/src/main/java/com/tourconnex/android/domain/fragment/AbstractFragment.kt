package com.tourconnex.android.domain.fragment

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.tourconnex.android.R
import com.tourconnex.android.domain.api.ApiClient
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.framework.services.ServiceLocator

open class AbstractFragment : Fragment() {
    protected fun getApiClient(): ApiClient {
        return ServiceLocator.getInstance().getService(ApiClient::class.java)
    }

    fun showErrorDialog(error: Error?, completionBlock :(() -> Unit)? = null) {
        showMessageDialog(error?.message)
    }

    fun showMessageDialog(message: String?, completionBlock :(() -> Unit)? = null) {
        val builder = AlertDialog.Builder(activity!!)
        builder.setMessage(message)
        builder.setPositiveButton(R.string.common_ok) { dialog, _ ->
            completionBlock?.let {
                it()
            }
            dialog.dismiss()
        }
        builder.show()
    }

    protected fun addFragment(containerViewId: Int, fragment: Fragment, fragmentTag: String) {
        childFragmentManager
            .beginTransaction()
            .add(containerViewId, fragment, fragmentTag)
            .disallowAddToBackStack()
            .commit()
    }

    protected fun replaceFragment(containerViewId: Int, fragment: Fragment, fragmentTag: String, backStackStateName: String?) {
        childFragmentManager
            .beginTransaction()
            .replace(containerViewId, fragment, fragmentTag)
            .addToBackStack(backStackStateName)
            .commit()
    }

    protected fun removeFragmentIfNeeded(fragmentTag: String) {
        val fragment = childFragmentManager.findFragmentByTag(fragmentTag)
        fragment?.let {
            childFragmentManager.beginTransaction().remove(it).commit()
        }
    }

    fun createProgressDialog(context: Context, title: String? = "Loading..."): AlertDialog {
        val llPadding = 30
        val ll = LinearLayout(context)
        ll.orientation = LinearLayout.HORIZONTAL
        ll.setPadding(llPadding, llPadding, llPadding, llPadding)
        ll.gravity = Gravity.CENTER
        var llParam = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)
        llParam.gravity = Gravity.CENTER
        ll.layoutParams = llParam

        val progressBar = ProgressBar(context)
        progressBar.isIndeterminate = true
        progressBar.setPadding(0, 0, llPadding, 0)
        progressBar.layoutParams = llParam

        llParam = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT)
        llParam.gravity = Gravity.CENTER
        val tvText = TextView(context)
        tvText.text = title
        tvText.setTextColor(Color.parseColor("#000000"))
        tvText.textSize = 20f
        tvText.layoutParams = llParam

        ll.addView(progressBar)
        ll.addView(tvText)

        val builder = AlertDialog.Builder(context)
        builder.setCancelable(false)
        builder.setView(ll)

        val dialog = builder.create()
        val window = dialog.window
        if (window != null) {
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog.window!!.attributes)
            layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
            layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            dialog.window!!.attributes = layoutParams
        }

        return dialog
    }
}
