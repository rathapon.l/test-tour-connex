package com.tourconnex.android.domain.notification

class NotificationType {
    companion object {
        const val MESSAGE               = 1
        const val ASSIGN_GUIDE          = 2
        const val ASSIGN_TRAVELLER      = 3
        const val SOS                   = 4
        const val SOS_TAKE_ACTION       = 5
        const val ANNOUNCEMENT          = 6
        const val SCHEDULE              = 7
        const val CONVERSATION_TEXT     = 1001
        const val CONVERSATION_IMAGE    = 1002
        const val CONVERSATION_VIDEO    = 1003
        const val CONVERSATION_AUDIO    = 1004
    }
}