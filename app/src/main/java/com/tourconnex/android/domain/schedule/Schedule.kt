package com.tourconnex.android.domain.schedule

import android.os.Parcel
import android.os.Parcelable
import java.util.*

data class Schedule(
    val id: String?,
    val updatedAt: Date?,
    val createdAt: Date?,
    val tripId: String?,
    val userId: String?,
    val message: String?,
    val time: Date?,
    val active: Boolean?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readSerializable() as? Date,
        parcel.readSerializable() as? Date,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readSerializable() as? Date,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean
    )


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeSerializable(updatedAt)
        parcel.writeSerializable(updatedAt)
        parcel.writeString(tripId)
        parcel.writeString(userId)
        parcel.writeString(message)
        parcel.writeSerializable(time)
        parcel.writeValue(active)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Schedule> {
        override fun createFromParcel(parcel: Parcel): Schedule {
            return Schedule(parcel)
        }

        override fun newArray(size: Int): Array<Schedule?> {
            return arrayOfNulls(size)
        }
    }
}