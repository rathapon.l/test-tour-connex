package com.tourconnex.android.domain.schedule.form

data class CreateScheduleForm(
    val message: String?,
    val time: Int?
)