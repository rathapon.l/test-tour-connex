package com.tourconnex.android.domain.sos

import com.tourconnex.android.domain.trip.SOS
import com.tourconnex.android.domain.users.User

data class SOSItem(val sos: SOS?, var state: Int, var actorUSer: User?)