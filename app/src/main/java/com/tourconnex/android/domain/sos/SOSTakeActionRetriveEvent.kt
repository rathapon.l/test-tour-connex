package com.tourconnex.android.domain.sos

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.messaging.RemoteMessage

data class SOSTakeActionRetriveEvent(
    val message: RemoteMessage?
)