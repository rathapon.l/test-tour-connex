package com.tourconnex.android.domain.sos.form

data class CancelSOSForm(
    val tripId: String?
)