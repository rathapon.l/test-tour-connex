package com.tourconnex.android.domain.traveller

data class TravellerCheckInForm(
    val userId: String?
)