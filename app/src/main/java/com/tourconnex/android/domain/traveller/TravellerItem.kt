package com.tourconnex.android.domain.traveller

import com.tourconnex.android.domain.users.Traveller

data class TravellerItem(
    val traveller: Traveller?,
    var colorName: String?
)