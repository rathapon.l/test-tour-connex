package com.tourconnex.android.domain.traveller

import android.os.Parcel
import android.os.Parcelable
import java.util.*

data class TravellerRoom(
    var id: String?,
    var title: String?,
    var firstName: String?,
    var lastName: String?,
    var createdAt: Date?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readSerializable() as? Date
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(title)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeSerializable(createdAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TravellerRoom> {
        override fun createFromParcel(parcel: Parcel): TravellerRoom {
            return TravellerRoom(parcel)
        }

        override fun newArray(size: Int): Array<TravellerRoom?> {
            return arrayOfNulls(size)
        }
    }

    fun getFullName(): String? {
        val title = title ?: ""
        val firstName = firstName ?: ""
        val lastName = lastName ?: ""

        return "$title $firstName $lastName"
    }
}