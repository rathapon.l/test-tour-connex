package com.tourconnex.android.domain.traveller.form

data class SetTravellerGroupColorForm(
    val colorType: Int
)