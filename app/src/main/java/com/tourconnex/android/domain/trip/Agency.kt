package com.tourconnex.android.domain.trip

import android.os.Parcel
import android.os.Parcelable

data class Agency (
    val id: String?,
    val updatedAt: String?,
    val createdAt: String?,
    val name: String?,
    val description: String?,
    val link: String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(updatedAt)
        parcel.writeString(createdAt)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(link)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Agency> {
        override fun createFromParcel(parcel: Parcel): Agency {
            return Agency(parcel)
        }

        override fun newArray(size: Int): Array<Agency?> {
            return arrayOfNulls(size)
        }
    }
}
