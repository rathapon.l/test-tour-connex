package com.tourconnex.android.domain.trip

import com.google.firebase.messaging.RemoteMessage

data class AssignGuideEvent(
    val message: RemoteMessage?
)