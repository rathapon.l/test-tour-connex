package com.tourconnex.android.domain.trip

import com.google.firebase.messaging.RemoteMessage

data class AssignTravellerEvent(
    val message: RemoteMessage?,
    val contentId: String?
)