package com.tourconnex.android.domain.trip

import android.os.Parcel
import android.os.Parcelable
import com.tourconnex.android.domain.common.Hotel
import com.tourconnex.android.domain.common.Room

data class Day (
    val id: Int?,
    val title: String?,
    val hotel: Hotel?,
    var rooms: List<Room>?,
    var images: List<Image>?,
    val programs: List<Program>?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readParcelable(Hotel::class.java.classLoader),
        parcel.createTypedArrayList(Room),
        parcel.createTypedArrayList(Image),
        parcel.createTypedArrayList(Program)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(title)
        parcel.writeParcelable(hotel, flags)
        parcel.writeTypedList(rooms)
        parcel.writeTypedList(images)
        parcel.writeTypedList(programs)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Day> {
        override fun createFromParcel(parcel: Parcel): Day {
            return Day(parcel)
        }

        override fun newArray(size: Int): Array<Day?> {
            return arrayOfNulls(size)
        }
    }
}