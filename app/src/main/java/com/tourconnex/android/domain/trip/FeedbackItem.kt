package com.tourconnex.android.domain.trip

data class FeedbackItem(
    val feedback: Feedback?,
    var rating: Int
)