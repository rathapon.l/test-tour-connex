package com.tourconnex.android.domain.trip

import android.os.Parcel
import android.os.Parcelable
import java.util.*

data class Image (
    val id: String?,
    val createdAt: Date?,
    val thumbnailImageUrl: String?,
    val largeImageUrl: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readSerializable() as? Date,
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeSerializable(createdAt)
        parcel.writeString(thumbnailImageUrl)
        parcel.writeString(largeImageUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Image> {
        override fun createFromParcel(parcel: Parcel): Image {
            return Image(parcel)
        }

        override fun newArray(size: Int): Array<Image?> {
            return arrayOfNulls(size)
        }
    }
}