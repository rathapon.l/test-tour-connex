package com.tourconnex.android.domain.trip

import android.os.Parcel
import android.os.Parcelable
import com.tourconnex.android.domain.common.FileItem

data class Insurance(
    val name: String? = null,
    val description: String? = null,
    val file: FileItem? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(FileItem::class.java.classLoader)
    ) 

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeParcelable(file, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Insurance> {
        override fun createFromParcel(parcel: Parcel): Insurance {
            return Insurance(parcel)
        }

        override fun newArray(size: Int): Array<Insurance?> {
            return arrayOfNulls(size)
        }
    }
}