package com.tourconnex.android.domain.trip

import android.os.Parcel
import android.os.Parcelable
import com.tourconnex.android.domain.common.Location
import com.tourconnex.android.domain.users.Traveller
import com.tourconnex.android.domain.users.User
import java.util.*

data class SOS (
    val id: String?,
    val tripId: String?,
    val userId: String?,
    val user: User?,
    val travellerId: String?,
    val traveller: Traveller?,
    val reactionUserId: String?,
    val reactionUser: User?,
    val location: Location?,
    val state: Int,
    val updatedAt: Date?,
    val createdAt: Date?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(User::class.java.classLoader),
        parcel.readString(),
        parcel.readParcelable(TripTraveller::class.java.classLoader),
        parcel.readString(),
        parcel.readParcelable(User::class.java.classLoader),
        parcel.readParcelable(Location::class.java.classLoader),
        parcel.readInt(),
        parcel.readSerializable() as? Date,
        parcel.readSerializable() as? Date
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(tripId)
        parcel.writeString(userId)
        parcel.writeParcelable(user, flags)
        parcel.writeString(travellerId)
        parcel.writeParcelable(traveller, flags)
        parcel.writeString(reactionUserId)
        parcel.writeParcelable(reactionUser, flags)
        parcel.writeParcelable(location, flags)
        parcel.writeInt(state)
        parcel.writeSerializable(updatedAt)
        parcel.writeSerializable(createdAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SOS> {

        const val STATE_CANCEL          = -1
        const val STATE_WAITING         = 0
        const val STATE_DONE            = 1

        override fun createFromParcel(parcel: Parcel): SOS {
            return SOS(parcel)
        }

        override fun newArray(size: Int): Array<SOS?> {
            return arrayOfNulls(size)
        }
    }
}
