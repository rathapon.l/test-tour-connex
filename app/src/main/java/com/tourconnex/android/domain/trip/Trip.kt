package com.tourconnex.android.domain.trip

import android.os.Parcel
import android.os.Parcelable
import com.tourconnex.android.domain.common.FileItem
import com.tourconnex.android.domain.flight.TripFlight
import java.util.*

data class Trip (
    val id: String?,
    val updatedAt: Date?,
    val createdAt: Date?,
    val coverPhoto: Image?,
    val title: String?,
    val description: String?,
    val destination: String?,
    val numberOfTravellers: Int?,
    val startDateTime: Date?,
    val endDateTime: Date?,
    val days: List<Day>?,
    val tripFlight: List<TripFlight>?,
    val insurance: Insurance?,
    val tripFile: FileItem?,
    val sos: Boolean?,
    val rating: Boolean?,
    val summaryDay: String?,
    val periodDay: String?,
    val periodNight: String?,
    val period: String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readSerializable() as? Date,
        parcel.readSerializable() as? Date,
        parcel.readParcelable(Image::class.java.classLoader),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readSerializable() as? Date,
        parcel.readSerializable() as? Date,
        parcel.createTypedArrayList(Day),
        parcel.createTypedArrayList(TripFlight),
        parcel.readParcelable(Insurance::class.java.classLoader),
        parcel.readParcelable(FileItem::class.java.classLoader),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeSerializable(updatedAt)
        parcel.writeSerializable(createdAt)
        parcel.writeParcelable(coverPhoto, flags)
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(destination)
        parcel.writeValue(numberOfTravellers)
        parcel.writeSerializable(startDateTime)
        parcel.writeSerializable(endDateTime)
        parcel.writeTypedList(days)
        parcel.writeTypedList(tripFlight)
        parcel.writeParcelable(insurance, flags)
        parcel.writeParcelable(tripFile, flags)
        parcel.writeValue(sos)
        parcel.writeValue(rating)
        parcel.writeString(summaryDay)
        parcel.writeString(periodDay)
        parcel.writeString(periodNight)
        parcel.writeString(period)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Trip> {
        override fun createFromParcel(parcel: Parcel): Trip {
            return Trip(parcel)
        }

        override fun newArray(size: Int): Array<Trip?> {
            return arrayOfNulls(size)
        }
    }
}
