package com.tourconnex.android.domain.trip

import com.tourconnex.android.domain.common.Hotel
import com.tourconnex.android.domain.common.Room

data class TripHotelItem(
    val day: Day?,
    val hotel: Hotel?,
    val room: Room?
)