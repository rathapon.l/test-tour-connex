package com.tourconnex.android.domain.trip

import java.util.*

interface TripInterface {
    val id: String?
    val trip: Trip?
    val updatedAt: Date?
    val createdAt: Date?
}