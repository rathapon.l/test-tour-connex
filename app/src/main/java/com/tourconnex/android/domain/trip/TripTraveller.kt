package com.tourconnex.android.domain.trip

import android.os.Parcel
import android.os.Parcelable
import com.tourconnex.android.domain.flight.FlightInfo
import com.tourconnex.android.domain.flight.TripFlight
import java.util.*

data class TripTraveller(
    override val id: String?,
    override val trip: Trip?,
    override val updatedAt: Date?,
    override val createdAt: Date?,
    val checkedIn: Boolean?,
    val isPreview: Boolean?,
    val guideType: Int,
    val flight: TripFlight?,
    val flightInfo: FlightInfo?,
    val agency: Agency?
) : TripInterface, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readParcelable(Trip::class.java.classLoader),
        parcel.readSerializable() as? Date,
        parcel.readSerializable() as? Date,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readInt(),
        parcel.readParcelable(TripFlight::class.java.classLoader),
        parcel.readParcelable(FlightInfo::class.java.classLoader),
        parcel.readParcelable(Agency::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeParcelable(trip, flags)
        parcel.writeSerializable(updatedAt)
        parcel.writeSerializable(createdAt)
        parcel.writeValue(checkedIn)
        parcel.writeValue(isPreview)
        parcel.writeInt(guideType)
        parcel.writeParcelable(flight, flags)
        parcel.writeParcelable(flightInfo, flags)
        parcel.writeParcelable(agency, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TripTraveller> {
        override fun createFromParcel(parcel: Parcel): TripTraveller {
            return TripTraveller(parcel)
        }

        override fun newArray(size: Int): Array<TripTraveller?> {
            return arrayOfNulls(size)
        }
    }

    fun isTourLeader(): Boolean {
        return guideType == 1
    }
}