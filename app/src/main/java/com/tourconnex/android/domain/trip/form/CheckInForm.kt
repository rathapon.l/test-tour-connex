package com.tourconnex.android.domain.trip.form

data class CheckInForm(
    val guideId: String?,
    val userId: String?
)