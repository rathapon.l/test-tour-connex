package com.tourconnex.android.domain.trip.form

import com.tourconnex.android.domain.common.Room

data class EditRoomForm(
    val rooms: ArrayList<Room>?
)