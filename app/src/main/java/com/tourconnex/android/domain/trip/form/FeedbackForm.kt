package com.tourconnex.android.domain.trip.form

data class FeedbackForm(
    val type: Int,
    val rating: Int
)