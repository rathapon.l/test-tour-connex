package com.tourconnex.android.domain.trip.form

data class ManageRatingForm(
    val isOn: Boolean
)