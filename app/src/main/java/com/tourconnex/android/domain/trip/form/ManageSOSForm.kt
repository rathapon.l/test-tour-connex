package com.tourconnex.android.domain.trip.form

data class ManageSOSForm(
    val isOn: Boolean
)