package com.tourconnex.android.domain.trip.form

data class SendFeedbackForm(
    var feedback: List<FeedbackForm>,
    var suggested: String?
)