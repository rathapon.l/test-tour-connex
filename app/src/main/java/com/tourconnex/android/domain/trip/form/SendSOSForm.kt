package com.tourconnex.android.domain.trip.form

data class SendSOSForm(
    val latitude: Double?,
    val longitude: Double?
)