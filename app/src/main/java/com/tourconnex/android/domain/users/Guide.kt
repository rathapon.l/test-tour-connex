package com.tourconnex.android.domain.users

import android.os.Parcel
import android.os.Parcelable
import com.tourconnex.android.domain.common.Location
import java.util.*

data class Guide(
    override val id: String?,
    override val user: User?,
    override val updatedAt: Date?,
    override val createdAt: Date?,
    override val location: Location?,
    override val title: String?,
    override val firstName: String?,
    override val lastName: String?,
    override val mobileNo: String?,
    override val nationality: String?,
    override val dateOfBirth: Date?,
    override val passportNumber: String?,
    override val passportCountryIssue: String?,
    override val passportIssueDate: Date?,
    override val passportExpiryDate: Date?
) : UserInterface, Parcelable {
    override fun getFullName(): String? {
        val title = title ?: ""
        val firstName = firstName ?: ""
        val lastName = lastName ?: ""

        return "$title $firstName $lastName"
    }

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readParcelable(User::class.java.classLoader),
        parcel.readSerializable() as? Date,
        parcel.readSerializable() as? Date,
        parcel.readParcelable(Location::class.java.classLoader),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readSerializable() as? Date,
        parcel.readString(),
        parcel.readString(),
        parcel.readSerializable() as? Date,
        parcel.readSerializable() as? Date
    )


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeParcelable(user, flags)
        parcel.writeSerializable(updatedAt)
        parcel.writeSerializable(createdAt)
        parcel.writeParcelable(location, flags)
        parcel.writeString(title)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(mobileNo)
        parcel.writeString(nationality)
        parcel.writeSerializable(dateOfBirth)
        parcel.writeString(passportNumber)
        parcel.writeString(passportCountryIssue)
        parcel.writeSerializable(passportIssueDate)
        parcel.writeSerializable(passportExpiryDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Guide> {
        override fun createFromParcel(parcel: Parcel): Guide {
            return Guide(parcel)
        }

        override fun newArray(size: Int): Array<Guide?> {
            return arrayOfNulls(size)
        }
    }
}