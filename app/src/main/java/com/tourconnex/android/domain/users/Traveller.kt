package com.tourconnex.android.domain.users

import android.os.Parcel
import android.os.Parcelable
import com.tourconnex.android.domain.common.Location
import com.tourconnex.android.domain.common.Type
import java.util.*

data class Traveller(
    override val id: String?,
    override val user: User?,
    override val updatedAt: Date?,
    override val createdAt: Date?,
    override val title: String?,
    override val firstName: String?,
    override val lastName: String?,
    override val mobileNo: String?,
    override val nationality: String?,
    override val dateOfBirth: Date?,
    override val passportNumber: String?,
    override val passportCountryIssue: String?,
    override val passportIssueDate: Date?,
    override val passportExpiryDate: Date?,
    override val location: Location?,
    val guideType: Int,
    val colorType: Type?,
    val roomRequest: String?,
    val seatRequest: String?,
    val specialRequest: String?
) : UserInterface, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readParcelable(User::class.java.classLoader),
        parcel.readSerializable() as? Date,
        parcel.readSerializable() as? Date,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readSerializable() as? Date,
        parcel.readString(),
        parcel.readString(),
        parcel.readSerializable() as? Date,
        parcel.readSerializable() as? Date,
        parcel.readParcelable(Location::class.java.classLoader),
        parcel.readInt(),
        parcel.readParcelable(Type::class.java.classLoader),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeParcelable(user, flags)
        parcel.writeSerializable(createdAt)
        parcel.writeSerializable(updatedAt)
        parcel.writeString(title)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(mobileNo)
        parcel.writeString(nationality)
        parcel.writeSerializable(dateOfBirth)
        parcel.writeString(passportNumber)
        parcel.writeString(passportCountryIssue)
        parcel.writeSerializable(passportIssueDate)
        parcel.writeSerializable(passportExpiryDate)
        parcel.writeParcelable(location, flags)
        parcel.writeInt(guideType)
        parcel.writeParcelable(colorType, flags)
        parcel.writeString(roomRequest)
        parcel.writeString(seatRequest)
        parcel.writeString(specialRequest)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Traveller> {
        override fun createFromParcel(parcel: Parcel): Traveller {
            return Traveller(parcel)
        }

        override fun newArray(size: Int): Array<Traveller?> {
            return arrayOfNulls(size)
        }
    }

    override fun getFullName(): String? {
        val title = title ?: ""
        val firstName = firstName ?: ""
        val lastName = lastName ?: ""

        return "$title $firstName $lastName"
    }
}