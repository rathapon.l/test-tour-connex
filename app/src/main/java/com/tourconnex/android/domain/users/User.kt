package com.tourconnex.android.domain.users

import android.os.Parcel
import android.os.Parcelable
import com.tourconnex.android.domain.trip.Image

data class User(
    val id: String?,
    val email: String?,
    val firstName: String?,
    val lastName: String?,
    val profilePicture: Image?,
    val isGuide: Boolean,
    val title: String?,
    val citizenId: String?,
    val passportNumber: String?,
    val nationality: String?,
    val passportCountryIssue: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(Image::class.java.classLoader),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(email)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeParcelable(profilePicture, flags)
        parcel.writeByte(if (isGuide) 1 else 0)
        parcel.writeString(title)
        parcel.writeString(citizenId)
        parcel.writeString(passportNumber)
        parcel.writeString(nationality)
        parcel.writeString(passportCountryIssue)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }

    fun getFullName(): String? {
        val title = title ?: ""
        val firstName = firstName ?: ""
        val lastName = lastName ?: ""

        return "$title $firstName $lastName"
    }
}