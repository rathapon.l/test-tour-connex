package com.tourconnex.android.domain.users

import com.tourconnex.android.domain.common.Location
import java.util.*

interface UserInterface {
    val id: String?
    val user: User?
    val updatedAt: Date?
    val createdAt: Date?
    val location: Location?
    val title: String?
    val firstName: String?
    val lastName: String?
    val mobileNo: String?
    val nationality: String?
    val dateOfBirth: Date?
    val passportNumber: String?
    val passportCountryIssue: String?
    val passportIssueDate: Date?
    val passportExpiryDate: Date?

    fun getFullName(): String?
}