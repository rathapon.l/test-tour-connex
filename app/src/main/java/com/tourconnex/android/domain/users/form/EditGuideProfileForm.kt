package com.tourconnex.android.domain.users.form

data class EditGuideProfileForm(
    val email: String?,
    val firstName: String?,
    val lastName: String?,
    val passportNumber: String?,
    val nationality: String?,
    val passportCountryIssue: String?
)