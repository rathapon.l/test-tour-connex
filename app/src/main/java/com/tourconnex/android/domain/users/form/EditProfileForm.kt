package com.tourconnex.android.domain.users.form

data class EditProfileForm(
    val citizenId: String?,
    val passportNumber: String?
)