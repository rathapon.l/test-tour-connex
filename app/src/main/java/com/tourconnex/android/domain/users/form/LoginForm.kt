package com.tourconnex.android.domain.users.form

data class LoginForm(val token: String, val type: String = "facebook")