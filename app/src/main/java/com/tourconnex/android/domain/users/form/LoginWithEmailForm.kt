package com.tourconnex.android.domain.users.form

data class LoginWithEmailForm(val email: String, val password: String, val type: String = "email")