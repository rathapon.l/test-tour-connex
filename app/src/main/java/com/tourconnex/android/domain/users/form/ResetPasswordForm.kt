package com.tourconnex.android.domain.users.form

data class ResetPasswordForm(
    val email: String?
)