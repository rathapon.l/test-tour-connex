package com.tourconnex.android.domain.users.form

data class TokenForm(val refreshToken: String)