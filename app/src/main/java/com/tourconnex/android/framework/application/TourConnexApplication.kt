package com.tourconnex.android.framework.application

import android.content.Context
import android.content.res.Configuration
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.multidex.MultiDexApplication
import com.tourconnex.android.TourConnex
import com.tourconnex.android.framework.services.ServiceLocatorUtils
import com.crashlytics.android.Crashlytics
import com.tourconnex.android.framework.manager.ExchangeRateManager
import com.zeugmasolutions.localehelper.LocaleHelperApplicationDelegate
import io.fabric.sdk.android.Fabric

class TourConnexApplication: MultiDexApplication(), LifecycleObserver {

    fun isRunningOnForegroud(): Boolean {
        return runningOnForeground
    }

    private var runningOnForeground: Boolean = false
    private var localeAppDelegate = LocaleHelperApplicationDelegate()

    override fun onCreate() {
        super.onCreate()
        TourConnex.getInstance().init(this)
        ServiceLocatorUtils.init(this)
        Fabric.with(this, Crashlytics())
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        ExchangeRateManager.getInstance().getExchagngeRates(this, null)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        runningOnForeground = false
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        runningOnForeground = true
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(localeAppDelegate.attachBaseContext(base))
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        localeAppDelegate.onConfigurationChanged(this)
    }
}
