package com.tourconnex.android.framework.http

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.tourconnex.android.framework.services.ServiceLocator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.tourconnex.android.domain.common.Error

abstract class MainThreadCallback<T>(private val context: Context?, private val progressBarOwn: ProgressBarOwn?) :
    Callback<T> {
    constructor(context: Context?) : this(context, null)

    init {
        progressBarOwn?.show()
    }


    abstract fun onSuccess(result: T?)

    override fun onResponse(call: Call<T>?, response: Response<T>?) {
        progressBarOwn?.hide()
        if (response != null && response.isSuccessful) {
            onSuccess(response.body())
        } else {
            onError(response, if (defaultErrorHandler()) tryReadError(response) else null)
        }
    }

    override fun onFailure(call: Call<T>?, t: Throwable?) {
        Log.e("error response", t?.message ?: "")
        progressBarOwn?.hide()
    }

    open fun onError(response: Response<T>?, error: Error?) {

    }

    open fun defaultErrorHandler(): Boolean {
        return true
    }

    private fun tryReadError(response: Response<T>?): Error? {
        return try {
            ServiceLocator.getInstance().getService(Gson::class.java)
                .fromJson(response?.errorBody()?.string(), Error::class.java)
        } catch (exception: Exception) {
            null
        }
    }

}