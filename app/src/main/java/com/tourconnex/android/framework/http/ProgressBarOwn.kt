package com.tourconnex.android.framework.http

interface ProgressBarOwn {
    fun show()

    fun hide()
}