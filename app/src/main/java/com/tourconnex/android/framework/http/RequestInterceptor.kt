package com.tourconnex.android.framework.http

import com.tourconnex.android.TourConnex
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

/**
 * The type Request interceptor.
 */
class RequestInterceptor
/**
 * Instantiates a new Request interceptor.
 */
    (private val userAgent: String) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val builder = original.url().newBuilder()

        val rBuilder = original.newBuilder().url(builder.build())
        rBuilder.headers(original.headers())
        rBuilder.header("User-Agent", userAgent)
        TourConnex.getInstance().getToken()?.let {
            rBuilder.header("Authorization", "Bearer $it")
        }
        return chain.proceed(rBuilder.build())
    }
}