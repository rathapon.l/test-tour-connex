package com.tourconnex.android.framework.http

import android.content.Context
import com.google.gson.Gson
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.common.Token
import com.tourconnex.android.domain.users.form.TokenForm
import com.tourconnex.android.framework.services.ServiceLocator
import okhttp3.*
import java.io.Reader

class TokenAuthenticator(private val context: Context) : Authenticator {

    companion object {
        val JSON = MediaType.parse("application/json; charset=utf-8")
    }

    override fun authenticate(route: Route?, response: Response?): Request? {
        if (response?.code() == 401) {
            TourConnex.getInstance().getRefreshToken()?.let {
                val refreshCall = refreshAccessToken(it)
                val refreshResponse = getOkhttp().newCall(refreshCall).execute()
                if (refreshResponse != null && refreshResponse.code() == 200) {
                    val token = parseToken(refreshResponse.body()?.charStream())
                    if (token != null) {
                        TourConnex.getInstance().setRefreshToken(token.refreshToken)
                        TourConnex.getInstance().setToken(token.accessToken)
                        return response.request().newBuilder()
                            .header("Authorization", "Bearer ${token.accessToken}")
                            .build()
                    }
                    return null
                } else {
                    null
                }
            }
        }
        return null
    }

    private fun refreshAccessToken(refreshToken: String): Request {
        val body = RequestBody.create(JSON, getGson().toJson(TokenForm(refreshToken)))
        return Request.Builder().url(context.getString(R.string.baseUrl) + "token")
            .post(body)
            .build()
    }

    private fun parseToken(reader: Reader?): Token? {
        return getGson().fromJson(reader, Token::class.java)
    }

    private fun getGson(): Gson {
        return ServiceLocator.getInstance().getService(Gson::class.java)
    }

    private fun getOkhttp(): OkHttpClient {
        return ServiceLocator.getInstance().getService(OkHttpClient::class.java)
    }
}