package com.tourconnex.android.framework.manager

import android.content.Context
import com.tourconnex.android.domain.api.ApiClient
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.conversation.ConversationAssociteItem
import com.tourconnex.android.domain.conversation.PageMessage
import com.tourconnex.android.domain.users.Guide
import com.tourconnex.android.domain.users.PageGuide
import com.tourconnex.android.domain.users.PageTraveller
import com.tourconnex.android.domain.users.Traveller
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.framework.http.ProgressBarOwn
import com.tourconnex.android.framework.services.ServiceLocator
import retrofit2.Call
import retrofit2.Response

class ConversationManager {
    companion object {
        private var manager: ConversationManager? = null

        /**
         * Gets session instance.
         */
        fun getInstance(): ConversationManager {
            if (manager == null) {
                manager = ConversationManager()
            }

            return manager!!
        }
    }

    private var callGuides: Call<PageGuide>? = null
    private var callTravellers: Call<PageTraveller>? = null
    var currentConversationId: String?= null

    fun getConversationAssociateItems(context: Context, tripId: String, completionBlock: ((ConversationAssociteItem?, Error?) -> Unit)?) {
        var error: Error? = null
        var travellers: List<Traveller>? = null
        var guides: List<Traveller>? = null
        val dispatchGroup = DispatchGroup()

        dispatchGroup.enter()
        getGuidesTrip(context, tripId) { retrievedGuides, retrievedError ->
            guides = retrievedGuides
            error = retrievedError
            dispatchGroup.leave()
        }

        dispatchGroup.enter()
        getTravellersTrip(context, tripId) { retrievedTraveller, retrievedError ->
            travellers = retrievedTraveller
            error = retrievedError
            dispatchGroup.leave()
        }

        dispatchGroup.notify {
            if (error != null) {
                completionBlock?.let { it(null, error)}
            } else {
                completionBlock?.let { it(ConversationAssociteItem(travellers, guides), null) }
            }
        }
    }

    private fun getGuidesTrip(context: Context, tripId: String, completionBlock: ((List<Traveller>?, Error?) -> Unit)?) {
        callGuides?.cancel()
        callGuides = getApiClient().getTripGuides(tripId)
        callGuides?.enqueue(object: MainThreadCallback<PageGuide>(context) {
            override fun onSuccess(result: PageGuide?) {
                completionBlock?.let { it(result?.entities, null) }
            }

            override fun onError(response: Response<PageGuide>?, error: Error?) {
                completionBlock?.let { it(null, error) }
            }
        })
    }

    private fun getTravellersTrip(context: Context, tripId: String, completionBlock: ((List<Traveller>?, Error?) -> Unit)?) {
        callTravellers?.cancel()
        callTravellers = getApiClient().getTripTravellers(tripId)
        callTravellers?.enqueue(object : MainThreadCallback<PageTraveller>(context) {
            override fun onSuccess(result: PageTraveller?) {
                completionBlock?.let { it(result?.entities, null) }
            }

            override fun onError(response: Response<PageTraveller>?, error: Error?) {
                completionBlock?.let { it(null, error) }
            }
        })
    }

    private fun getApiClient(): ApiClient {
        return ServiceLocator.getInstance().getService(ApiClient::class.java)
    }
}