package com.tourconnex.android.framework.manager

import android.content.Context
import com.tourconnex.android.domain.api.ApiClient
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.ExchangeRate
import com.tourconnex.android.domain.exchange.ExchangeRateResponse
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.framework.services.ServiceLocator
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class ExchangeRateManager {
    companion object {
        private var exchangeRateManager: ExchangeRateManager? = null

        /**
         * Gets session instance.
         */
        fun getInstance(): ExchangeRateManager {
            if (exchangeRateManager == null) {
                exchangeRateManager = ExchangeRateManager()
            }
            return exchangeRateManager!!
        }
    }

    private var exchangeRates: ArrayList<ExchangeRate>? = null
    private var lastUpdate: Date? = null
    private var powerBy: String? = null

    private var callExchange: Call<ExchangeRateResponse>? = null

    protected fun getApiClient(): ApiClient {
        return ServiceLocator.getInstance().getService(ApiClient::class.java)
    }

    fun getExchagngeRates(context: Context, completionBlock: ((ArrayList<ExchangeRate>?, Date?, String?, Error?) -> Unit)?) {
        exchangeRates?.let { exchangeRates ->
            completionBlock?.let { it(exchangeRates, lastUpdate, powerBy, null) }
            return
        }

        callExchange?.cancel()
        callExchange = getApiClient().getExchangeRates()
        callExchange?.enqueue(object : MainThreadCallback<ExchangeRateResponse>(context) {
            override fun onSuccess(result: ExchangeRateResponse?) {
                this@ExchangeRateManager.exchangeRates = result?.exchangeRates
                this@ExchangeRateManager.lastUpdate = result?.lastUpdate
                this@ExchangeRateManager.powerBy = result?.powerBy
                completionBlock?.let { it(result?.exchangeRates, result?.lastUpdate, result?.powerBy, null) }
            }

            override fun onError(response: Response<ExchangeRateResponse>?, error: Error?) {
                completionBlock?.let { it(null, null, null, error) }
            }
        })
    }
}