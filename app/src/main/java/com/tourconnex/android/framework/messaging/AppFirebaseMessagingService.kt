package com.tourconnex.android.framework.messaging

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.tourconnex.android.MainActivity
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.conversation.ConversationRetriveEvent
import com.tourconnex.android.domain.notification.NotificationType
import com.tourconnex.android.domain.sos.SOSTakeActionRetriveEvent
import com.tourconnex.android.domain.trip.AssignGuideEvent
import com.tourconnex.android.domain.trip.AssignTravellerEvent
import com.tourconnex.android.framework.application.TourConnexApplication
import com.tourconnex.android.framework.manager.ConversationManager
import org.greenrobot.eventbus.EventBus



class AppFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        remoteMessage?.notification?.body?.let {
            handleRecieveMessage(remoteMessage)
        }
    }

    private fun handleRecieveMessage(remoteMessage: RemoteMessage?) {
        val tourConextApplication =  application as? TourConnexApplication
        val notificationData = remoteMessage?.data
        val type = notificationData?.get("type")?.toInt() ?: NotificationType.MESSAGE
        val contentId = notificationData?.get("content_id")
        if (tourConextApplication?.isRunningOnForegroud() == true) {
            when (type) {
                NotificationType.CONVERSATION_VIDEO, NotificationType.CONVERSATION_AUDIO, NotificationType.CONVERSATION_IMAGE, NotificationType.CONVERSATION_TEXT -> {
                    if (ConversationManager.getInstance().currentConversationId != contentId) {
                        EventBus.getDefault().post(DisplayInAppNotificationEvent(remoteMessage, type, contentId))
                    }

                    EventBus.getDefault().post(ConversationRetriveEvent(remoteMessage))
                }
                NotificationType.ASSIGN_GUIDE -> {
                    EventBus.getDefault().post(AssignGuideEvent(remoteMessage))
                    EventBus.getDefault().post(DisplayInAppNotificationEvent(remoteMessage, type, contentId))
                }
                NotificationType.ASSIGN_TRAVELLER -> {
                    EventBus.getDefault().post(AssignTravellerEvent(remoteMessage, contentId))
                    EventBus.getDefault().post(DisplayInAppNotificationEvent(remoteMessage, type, contentId))
                }
                NotificationType.SOS_TAKE_ACTION -> {
                    EventBus.getDefault().post(SOSTakeActionRetriveEvent(remoteMessage))
                }
                else -> {
                    EventBus.getDefault().post(DisplayInAppNotificationEvent(remoteMessage, type, contentId))
                }
            }
        } else {
            when (type) {
                NotificationType.CONVERSATION_VIDEO, NotificationType.CONVERSATION_AUDIO, NotificationType.CONVERSATION_IMAGE, NotificationType.CONVERSATION_TEXT -> {
                    if (TourConnex.getInstance().getActiveTrip() != null) {
                        remoteMessage?.let {
                            createNotification(it)
                        }
                    }
                }
                else -> {
                    remoteMessage?.let {
                        createNotification(it)
                    }
                }
            }
        }
    }

    private fun createNotification(remoteMessage: RemoteMessage) {
        val notification = remoteMessage.notification

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

        val intent = PendingIntent.getActivity(this, 0,
            notificationIntent, 0)

        val notificationBuilder = NotificationCompat.Builder(this, getString(R.string.default_notification_channel_id))
            .setContentIntent(intent)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(notification!!.title)
            .setContentText(notification.body)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(getString(R.string.default_notification_channel_id),
                "Used for updated news.",
                NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(1542, notificationBuilder.build())
    }
}