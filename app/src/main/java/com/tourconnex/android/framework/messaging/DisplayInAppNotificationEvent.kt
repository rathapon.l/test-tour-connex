package com.tourconnex.android.framework.messaging

import com.google.firebase.messaging.RemoteMessage

data class DisplayInAppNotificationEvent(
    val remoteMessage: RemoteMessage?,
    val type: Int,
    val contentId: String?
)