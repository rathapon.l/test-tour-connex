package com.tourconnex.android.framework.preference

import android.content.Context

class AppPreference
/**
 * Instantiates a new App preference.
 *
 * @param context the context
 */
    (context: Context) {
    private val simplePreference: SimplePreference

    init {
        this.simplePreference = SimplePreference(context, APP_PREF)
    }

    companion object {
        private val APP_PREF = "app-pref"
    }

}
