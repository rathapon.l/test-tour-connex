package com.tourconnex.android.framework.preference

import android.content.Context

class ConversationManagePreference
/**
 * Instantiates a new User preference.
 *
 * @param context the context
 */
    (context: Context) {
    companion object {
        private const val CONVERSATION_MANAGE_PREFS = "CONVERSATION_MANAGE_PREFS"
        private const val CONVERSATION_ = "CONVERSATION_"
    }

    private val pref: SimplePreference
    private var map = HashMap<String, String?>()

    init {
        this.pref = SimplePreference(context, CONVERSATION_MANAGE_PREFS)
//        pref.getAll()?.let {
//            for (entry in it.entries) {
//                map[entry.key] = entry.value.toString()
//            }
//        }
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    fun setLatestSeenMessageId(conversationId: String?, userId: String?, latestSeenMessageId: String?) {
        val key = getTripByUserIdKey(conversationId, userId)
        map[key] = latestSeenMessageId
        pref.setString(key, latestSeenMessageId)
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    fun getLatestSeenMessageId(conversationId: String?, userId: String?): String? {
        val key = getTripByUserIdKey(conversationId, userId)
        var latestSeenMessageId = map[key]
        if (latestSeenMessageId == null) {
            latestSeenMessageId = pref.getString(key, null)
            map[key] = latestSeenMessageId
        }

        return latestSeenMessageId
    }

    private fun getTripByUserIdKey(conversationId: String?, userId: String?): String {
        val id = userId ?: ""
        return "$CONVERSATION_${conversationId ?: ""}_$id"
    }

    /**
     * Clear.
     */
    fun clear() {
        pref.clear()
    }

    fun clearCurrentConversation() {
        map = HashMap()
    }
}