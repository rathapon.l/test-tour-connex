package com.tourconnex.android.framework.preference

import android.content.Context
import com.tourconnex.android.domain.common.Currency

class ExchangePreference
/**
 * Instantiates a new User preference.
 *
 * @param context the context
 */
    (context: Context) {
    companion object {
        private const val EXCHANGE_PREFS = "EXCHANGE_PREFS"
        private const val BASE_CURRENCY_KEY = "BASE_CURRENCY_KEY"
        private const val RESULT_CURRENCY_KEY = "RESULT_CURRENCY_KEY"
    }

    private val pref: SimplePreference
    private val defaultCurrency = Currency("฿", "Thai Baht", "฿", 2, 0.0, "THB", "Thai baht")
    private var baseCurrency: Currency? = null
    private var resultCurrency: Currency? = null


    init {
        this.pref = SimplePreference(context, EXCHANGE_PREFS)
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    fun setBaseCurrency(currency: Currency) {
        this.baseCurrency = currency
        pref.setObject(BASE_CURRENCY_KEY, currency)
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    fun getBaseCurrency(): Currency? {
        if (baseCurrency == null) {
            val retriveCurrency = pref.getObject(BASE_CURRENCY_KEY, Currency::class.java)
            if (retriveCurrency != null) {
                baseCurrency = retriveCurrency
            } else {
                baseCurrency = defaultCurrency
            }
        }

        return baseCurrency
    }

    fun setResultCurrency(currency: Currency) {
        this.resultCurrency = currency
        pref.setObject(RESULT_CURRENCY_KEY, currency)
    }

    fun getResultCurrency(): Currency? {
        if (resultCurrency == null) {
            val retriveCurrency = pref.getObject(Companion.RESULT_CURRENCY_KEY, Currency::class.java)
            if (retriveCurrency != null) {
                resultCurrency = retriveCurrency
            } else {
                resultCurrency = defaultCurrency
            }
        }

        return resultCurrency
    }

    /**
     * Clear.
     */
    fun clear() {
        baseCurrency = defaultCurrency
        resultCurrency = defaultCurrency
        pref.clear()
    }
}