package com.tourconnex.android.framework.preference

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.tourconnex.android.framework.services.ServiceLocator

/**
 * The type Simple preference.
 */
class SimplePreference
/**
 * Instantiates a new Simple preference.
 *
 * @param context the context
 * @param name    the name
 */
    (context: Context, name: String) {
    private val preference: SharedPreferences

    private val gson: Gson
        get() = ServiceLocator.getInstance().getService(Gson::class.java)

    init {
        preference = context.getSharedPreferences(name, Context.MODE_PRIVATE)
        val ss = preference.all
    }

    fun getAll(): Map<String, *>? {
        return preference.all
    }

    /**
     * Gets string.
     *
     * @param key          the key
     * @param defaultValue the default value
     * @return the string
     */
    fun getString(key: String, defaultValue: String?): String? {
        return preference.getString(key, defaultValue)
    }

    /**
     * Sets string.
     *
     * @param key   the key
     * @param value the value
     */
    fun setString(key: String, value: String?) {
        preference.edit().putString(key, value).apply()
    }

    /**
     * Sets boolean.
     *
     * @param key   the key
     * @param value the value
     */
    fun setBoolean(key: String, value: Boolean?) {
        preference.edit().putBoolean(key, value!!).apply()
    }

    /**
     * Gets boolean.
     *
     * @param key          the key
     * @param defaultValue the default value
     * @return the boolean
     */
    fun getBoolean(key: String, defaultValue: Boolean?): Boolean? {
        return preference.getBoolean(key, defaultValue!!)
    }

    /**
     * Sets integer.
     *
     * @param key   the key
     * @param value the value
     */
    fun setInteger(key: String, value: Int?) {
        preference.edit().putInt(key, value!!).apply()
    }

    /**
     * Gets integer.
     *
     * @param key          the key
     * @param defaultValue the default value
     * @return the integer
     */
    fun getInteger(key: String, defaultValue: Int?): Int? {
        return preference.getInt(key, defaultValue!!)
    }

    /**
     * Sets object.
     *
     * @param key    the key
     * @param value the object
     */
    fun setObject(key: String, value: Any?) {
        preference.edit().putString(key, gson.toJson(value)).apply()
    }

    /**
     * Gets object.
     *
     * @param <T>   the type parameter
     * @param key   the key
     * @param clazz the clazz
     * @return the object
    </T> */
    fun <T> getObject(key: String, clazz: Class<T>?): T? {
        val value = preference.getString(key, null)
        return if (value != null && value != "") {
            try {
                gson.fromJson(value, clazz)
            } catch (e: Exception) {
                null
            }

        } else {
            null
        }
    }

    /**
     * Clear.
     */
    fun clear() {
        preference.edit().clear().apply()
    }
}