package com.tourconnex.android.framework.preference

import android.content.Context
import com.tourconnex.android.domain.trip.Trip
import com.tourconnex.android.domain.trip.TripTraveller

class TripManagePreference
/**
 * Instantiates a new User preference.
 *
 * @param context the context
 */
    (context: Context) {
    companion object {
        private const val TRIP_MANAGE_PREFS = "TRIP_MANAGE_PREFS"
        private const val ACTIVE_TRIP = "ACTIVE_TRIP_"
    }

    private val pref: SimplePreference
    private var activeTrip: TripTraveller? = null

    init {
        this.pref = SimplePreference(context, TRIP_MANAGE_PREFS)
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    fun setActiveTrip(trip: TripTraveller?, userId: String?) {
        this.activeTrip = trip
        pref.setObject(getTripByUserIdKey(userId), trip)
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    fun getActiveTrip(userId: String?): TripTraveller? {
        if (activeTrip == null) {
            activeTrip = pref.getObject(getTripByUserIdKey(userId), TripTraveller::class.java)
        }

        return activeTrip
    }

    private fun getTripByUserIdKey(userId: String?): String {
        val id = userId ?: ""
        val key = "$ACTIVE_TRIP$id"
        return key
    }

    /**
     * Clear.
     */
    fun clear() {
        activeTrip = null
        pref.clear()
    }

    fun clearCurrentUserActiveTrip() {
        activeTrip = null
    }

}