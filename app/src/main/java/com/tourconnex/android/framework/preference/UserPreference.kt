package com.tourconnex.android.framework.preference

import android.content.Context
import com.tourconnex.android.domain.users.User

/**
 * The interface User preference.
 *
 * @author Thanyatad Jumprom
 */
class UserPreference
/**
 * Instantiates a new User preference.
 *
 * @param context the context
 */
    (context: Context) {
    companion object {
        private const val USER_PREFER = "ZON_PREFS"
        private const val TOKEN = "token"
        private const val REFRESH_TOKEN = "refresh-token"
        private const val KEY_USER = "key-user"
        private const val KEY_ENABLE_LOCATION = "KEY_ENABLE_LOCATION"
        private const val KEY_ENABLE_NOTIFICATION = "KEY_ENABLE_NOTIFICATION"
    }

    private val pref: SimplePreference
    private var user: User? = null
    private var token: String? = null
    private var refreshToken: String? = null
    private var enableLocation: Boolean? = null
    private var enableNotification: Boolean? = null

    init {
        this.pref = SimplePreference(context, USER_PREFER)
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    fun setUser(user: User?) {
        this.user = user
        pref.setObject(KEY_USER, user)
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    fun getUser(): User? {
        if (user == null) {
            user = pref.getObject(KEY_USER, User::class.java)
        }
        return user
    }

    /**
     * Sets token.
     *
     * @param token the token
     */
    fun setToken(token: String?) {
        this.token = token
        pref.setString(TOKEN, token)
    }

    /**
     * Gets token.
     *
     * @return the token
     */
    fun getToken(): String? {
        if (token == null) {
            token = pref.getString(TOKEN, null)
        }

        return token
    }

    /**
     * Sets token.
     *
     * @param refresh the token
     */
    fun setRefreshToken(refreshToken: String?) {
        this.refreshToken = refreshToken
        pref.setString(REFRESH_TOKEN, refreshToken)
    }

    /**
     * Gets token.
     *
     * @return the token
     */
    fun getRefreshToken(): String? {
        if (refreshToken == null) {
            refreshToken = pref.getString(REFRESH_TOKEN, null)
        }
        return refreshToken
    }

    fun setEnableLocation(enable: Boolean) {
        enableLocation = enable
        pref.setBoolean(KEY_ENABLE_LOCATION, enable)
    }

    fun getEnableLocation(): Boolean {
        if (enableLocation == null) {
            enableLocation = pref.getBoolean(KEY_ENABLE_LOCATION, true) ?: true
        }

        return enableLocation ?: true
    }

    fun setEnableNotification(enable: Boolean) {
        enableNotification = enable
        pref.setBoolean(KEY_ENABLE_NOTIFICATION, enable)
    }

    fun getEnableNotification(): Boolean {
        if (enableNotification == null) {
            enableNotification = pref.getBoolean(KEY_ENABLE_NOTIFICATION, true) ?: true
        }

        return enableNotification ?: true
    }

    /**
     * Clear.
     */
    fun clear() {
        user = null
        token = null
        refreshToken = null
        enableLocation = null
        enableNotification = null
        pref.clear()
    }


}