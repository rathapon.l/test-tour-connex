package com.tourconnex.android.framework.services;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tourconnex.android.BuildConfig;
import com.tourconnex.android.R;
import com.tourconnex.android.domain.api.ApiClient;
import com.tourconnex.android.framework.http.RequestInterceptor;
import com.tourconnex.android.framework.http.TokenAuthenticator;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import javax.net.ssl.*;
import java.security.KeyStore;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * The type Services locator.
 *
 * @author Thanyatad Jumprom
 */
public class ServiceLocator {
    private static ServiceLocator instance;
    private Map<Class<?>, Object> locator = new HashMap<>();

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static ServiceLocator getInstance() {
        if (instance == null) {
            instance = new ServiceLocator();
        }
        return instance;
    }

    /**
     * Gets service of the specified type.
     *
     * @param <S>    Type of service
     * @param ofType type of service
     * @return service of the specified type
     */
    @SuppressWarnings("unchecked")
    public <S> S getService(Class<S> ofType) {
        return (S) locator.get(ofType);
    }

    /**
     * Init.
     *
     * @param context the context
     */
    void init(Context context) {
        locator.put(Gson.class, createGson());
        locator.put(OkHttpClient.class, createOkhttpClient(context));
        locator.put(ApiClient.class, createApiClient(createRetrofit(context)));
    }

    private ApiClient createApiClient(Retrofit retrofit) {
        return retrofit.create(ApiClient.class);
    }


    private Retrofit createRetrofit(Context context) {
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(context.getString(R.string.baseUrl));
        builder.client(getService(OkHttpClient.class));
        builder.addConverterFactory(GsonConverterFactory.create(getService(Gson.class)));
        return builder.build();
    }

    private Gson createGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.serializeSpecialFloatingPointValues();
        builder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        builder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        return builder.create();
    }

    private OkHttpClient createOkhttpClient(Context context) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.authenticator(new TokenAuthenticator(context));
        builder.readTimeout(2, TimeUnit.MINUTES);
        builder.writeTimeout(2, TimeUnit.MINUTES);
        builder.followRedirects(true);
        builder.followSslRedirects(true);
        builder.addInterceptor(new RequestInterceptor(getUserAgent(context)));

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);

            Log.d("User Agent", getUserAgent(context));
        }

        if (Build.VERSION.SDK_INT < 22) {
            try {
                /*SSLContext sc = SSLContext.getInstance("TLSv1.2");
                sc.init(null, null, null);
                builder.sslSocketFactory(new Tls12SocketFactory(sc.getSocketFactory()));
                ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS).tlsVersions(TlsVersion.TLS_1_2).build();
                List<ConnectionSpec> specs = new ArrayList<>();
                specs.add(cs);
                specs.add(ConnectionSpec.COMPATIBLE_TLS);
                specs.add(ConnectionSpec.CLEARTEXT);
                builder.connectionSpecs(specs);*/

                // Waiting for test in android lower api
                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                        TrustManagerFactory.getDefaultAlgorithm());
                trustManagerFactory.init((KeyStore) null);
                TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
                if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                    throw new IllegalStateException("Unexpected default trust managers:"
                            + Arrays.toString(trustManagers));
                }
                X509TrustManager trustManager = (X509TrustManager) trustManagers[0];
                SSLContext sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, new TrustManager[]{trustManager}, null);
                SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
                SSLContext sc = SSLContext.getInstance("TLSv1.2");
                sc.init(null, null, null);
                builder.sslSocketFactory(sslSocketFactory, trustManager);
                ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS).tlsVersions(TlsVersion.TLS_1_2).build();
                List<ConnectionSpec> specs = new ArrayList<>();
                specs.add(cs);
                specs.add(ConnectionSpec.COMPATIBLE_TLS);
                specs.add(ConnectionSpec.CLEARTEXT);
                builder.connectionSpecs(specs);
            } catch (Exception exc) {
                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc);
            }
        }

        return builder.build();
    }

    private String getUserAgent(Context context) {

        return "(" +
                Build.MODEL +
                "; " +
                "Android" +
                "; " +
                Build.VERSION_CODES.class.getFields()[Build.VERSION.SDK_INT].getName() +
                "; " +
                BuildConfig.VERSION_NAME +
                "; " +
                BuildConfig.VERSION_CODE +
                "; " +
                Build.VERSION.SDK_INT +
                ")";
    }


}
