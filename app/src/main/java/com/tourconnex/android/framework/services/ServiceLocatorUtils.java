package com.tourconnex.android.framework.services;

import android.content.Context;

public class ServiceLocatorUtils {

    /**
     * Init.
     *
     * @param context the context
     */
    public static void init(Context context) {
        ServiceLocator.getInstance().init(context);
    }

}