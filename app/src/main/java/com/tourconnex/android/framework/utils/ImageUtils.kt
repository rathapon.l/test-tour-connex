package com.tourconnex.android.framework.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import java.io.*
import java.util.*
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter

object ImageUtils {

    private const val MAX_SIZE = 1980

    fun genQRFromFreeText(text: String?, width: Int, height: Int): Bitmap? {
        val writer = QRCodeWriter()
        try {
            val bitMatrix = writer.encode(text ?: "", BarcodeFormat.QR_CODE, width, height)
            val width = bitMatrix.width
            val height = bitMatrix.height
            val bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
            for (x in 0 until width) {
                for (y in 0 until height) {
                    bmp.setPixel(x, y, if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE)
                }
            }

            return bmp
        } catch (e: WriterException) {
            e.printStackTrace()

            return null
        }
    }

    fun createRotatedAndResizedImage(file: File): File? {
        val byteArray = getStreamByteFromImage(file)
        val tempFile = File.createTempFile(
            UUID.randomUUID().toString().substring(5),
            file.name.substringAfterLast('.', "")
        )
        try {
            val rawBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
            val bitmap = getResizedBitmap(rawBitmap, MAX_SIZE, MAX_SIZE)
            val stream: OutputStream = FileOutputStream(tempFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
            return tempFile
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    private fun getResizedBitmap(bm: Bitmap, newWidth: Int, newHeight: Int): Bitmap {
        var width = bm.width
        var height = bm.height
        when {
            width > height -> {
                // landscape
                val ratio = width.toFloat() / newWidth
                width = newWidth
                height = (height / ratio).toInt()
            }
            height > width -> {
                // portrait
                val ratio = height.toFloat() / newHeight
                height = newHeight
                width = (width / ratio).toInt()
            }
            else -> {
                // square
                height = newHeight
                width = newWidth
            }
        }
        val resizedBitmap = Bitmap.createScaledBitmap(bm, width, height, true)
        bm.recycle()
        return resizedBitmap
    }

    private fun getStreamByteFromImage(imageFile: File): ByteArray {
        var photoBitmap = BitmapFactory.decodeFile(imageFile.path)
        val stream = ByteArrayOutputStream()
        val imageRotation = getImageRotation(imageFile)
        if (imageRotation != 0)
            photoBitmap = getBitmapRotatedByDegree(photoBitmap, imageRotation)
        photoBitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream)
        return stream.toByteArray()
    }

    private fun getImageRotation(imageFile: File): Int {
        var exif: androidx.exifinterface.media.ExifInterface? = null
        var exifRotation = 0
        try {
            exif = androidx.exifinterface.media.ExifInterface(imageFile.path)
            exifRotation = exif.getAttributeInt(
                androidx.exifinterface.media.ExifInterface.TAG_ORIENTATION,
                androidx.exifinterface.media.ExifInterface.ORIENTATION_NORMAL
            )
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return if (exif == null)
            0
        else
            exifToDegrees(exifRotation)
    }

    private fun exifToDegrees(rotation: Int): Int {
        if (rotation == androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_90)
            return 90
        else if (rotation == androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_180)
            return 180
        else if (rotation == androidx.exifinterface.media.ExifInterface.ORIENTATION_ROTATE_270)
            return 270

        return 0
    }

    private fun getBitmapRotatedByDegree(bitmap: Bitmap, rotationDegree: Int): Bitmap {
        val matrix = Matrix()
        matrix.preRotate(rotationDegree.toFloat())
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }
}
