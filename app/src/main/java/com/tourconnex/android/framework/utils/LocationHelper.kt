package com.tourconnex.android.framework.utils

import android.Manifest
import android.content.Context
import android.location.Location
import android.location.LocationListener
//import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import com.tourconnex.android.framework.utils.adapter.BaseLocationHelperAdapter
import pub.devrel.easypermissions.EasyPermissions
import com.yayandroid.locationmanager.constants.ProviderType
import com.yayandroid.locationmanager.configuration.DefaultProviderConfiguration
import com.yayandroid.locationmanager.configuration.GooglePlayServicesConfiguration
import android.Manifest.permission
import com.tourconnex.android.R
import com.tourconnex.android.framework.utils.adapter.ActivityLocationHelperAdapter
import com.tourconnex.android.framework.utils.adapter.FragmentLocationHelperAdapter
import com.yayandroid.locationmanager.LocationManager
import com.yayandroid.locationmanager.configuration.PermissionConfiguration
import com.yayandroid.locationmanager.configuration.LocationConfiguration



class LocationHelper {

    private var adapter: BaseLocationHelperAdapter? = null

    private var locationManager: LocationManager? = null
    private var completionBlock: ((Location?, Boolean) -> Unit)? = null
    private var requestCode = -1

    constructor(adapter: BaseLocationHelperAdapter?) {
        this.adapter = adapter
    }

    fun requestLocation(completionBlock: ((Location?, Boolean) -> Unit)?, requestCode: Int) {

        if (adapter?.getContext() == null) {
            return
        }

        this.completionBlock = completionBlock
        this.requestCode = requestCode

        val perms = arrayOf(permission.ACCESS_FINE_LOCATION, permission.ACCESS_COARSE_LOCATION)
        if (!EasyPermissions.hasPermissions(adapter?.getContext()!!, *perms)) {
            adapter?.requestPermission(perms, requestCode)
        } else {
            fetchLocation()
        }
    }

    private fun fetchLocation() {
        val activityAdapter = adapter as? ActivityLocationHelperAdapter
        val fragmentAdapter = adapter as? FragmentLocationHelperAdapter
        var context: Context? = null
        if (activityAdapter != null) {
            context = activityAdapter.activity?.applicationContext
        } else if (fragmentAdapter != null) {
            context = fragmentAdapter?.fragment.activity?.applicationContext
        }

        if (context == null) {
            return
        }

        val awesomeConfiguration = LocationConfiguration.Builder()
            .keepTracking(false)
            .askForPermission(
                PermissionConfiguration.Builder()
                    .rationaleMessage(context.getString(R.string.location_please_allow_location_permission))
                    .requiredPermissions(arrayOf(permission.ACCESS_FINE_LOCATION, permission.ACCESS_COARSE_LOCATION))
                    .build()
            )
            .useGooglePlayServices(
                GooglePlayServicesConfiguration.Builder()
                    .fallbackToDefault(true)
                    .askForGooglePlayServices(true)
                    .askForSettingsApi(true)
                    .failOnConnectionSuspended(true)
                    .failOnSettingsApiSuspended(false)
                    .ignoreLastKnowLocation(false)
                    .setWaitPeriod((20 * 1000).toLong())
                    .build()
            )
            .useDefaultProviders(
                DefaultProviderConfiguration.Builder()
                    .requiredTimeInterval((5 * 60 * 1000).toLong())
                    .requiredDistanceInterval(0)
                    .acceptableAccuracy(5.0f)
                    .acceptableTimePeriod((5 * 60 * 1000).toLong())
                    .gpsMessage(context.getString(R.string.location_please_turn_on_gps))
                    .setWaitPeriod(ProviderType.GPS, (20 * 1000).toLong())
                    .setWaitPeriod(ProviderType.NETWORK, (20 * 1000).toLong())
                    .build()
            )
            .build()

        locationManager = LocationManager.Builder(context!!)
            .activity(activityAdapter?.activity) // Only required to ask permission and/or GoogleApi - SettingsApi
            .fragment(fragmentAdapter?.fragment) // Only required to ask permission and/or GoogleApi - SettingsApi
            .configuration(awesomeConfiguration)
            .notify(object : com.yayandroid.locationmanager.listener.LocationListener {
                override fun onLocationChanged(location: Location?) {
                    completionBlock?.let { completionBlock ->
                        location?.let {
                            completionBlock(it, true)
                        } ?: kotlin.run {
                            completionBlock(null, false)
                        }
                    }
                    locationManager?.cancel()
                }

                override fun onLocationFailed(type: Int) {

                }

                override fun onPermissionGranted(alreadyHadPermission: Boolean) {

                }

                override fun onProcessTypeChanged(processType: Int) {

                }

                override fun onProviderDisabled(provider: String?) {

                }

                override fun onProviderEnabled(provider: String?) {

                }

                override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

                }
            })
            .build()

        locationManager?.get()
    }

    private val easyPermissionListener = object : EasyPermissions.PermissionCallbacks {
        override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {

        }

        override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
            if (requestCode == this@LocationHelper.requestCode) {
                fetchLocation()
            }
        }

        override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<out String>,
            grantResults: IntArray
        ) {

        }
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, easyPermissionListener)
    }

    fun cancel() {
        locationManager?.cancel()
        completionBlock = null
    }
}