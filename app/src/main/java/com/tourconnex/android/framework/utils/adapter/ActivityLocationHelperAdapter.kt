package com.tourconnex.android.framework.utils.adapter

import android.app.Activity
import android.content.Context
import androidx.core.app.ActivityCompat

data class ActivityLocationHelperAdapter(val activity: Activity?): BaseLocationHelperAdapter {
    override fun requestPermission(perms: Array<String>, requestCode: Int) {
        if (activity == null) {
            return
        }

        ActivityCompat.requestPermissions(activity, perms, requestCode)
    }

    override fun getContext(): Context? {
        return activity
    }
}