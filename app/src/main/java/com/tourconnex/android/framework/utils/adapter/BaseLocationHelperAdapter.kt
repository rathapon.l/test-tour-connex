package com.tourconnex.android.framework.utils.adapter

import android.content.Context

interface BaseLocationHelperAdapter {
    fun requestPermission(perms: Array<String>, requestCode: Int)
    fun getContext(): Context?
}