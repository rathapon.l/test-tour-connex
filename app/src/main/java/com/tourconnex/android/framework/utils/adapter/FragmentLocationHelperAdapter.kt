package com.tourconnex.android.framework.utils.adapter

import android.content.Context
import androidx.fragment.app.Fragment

data class FragmentLocationHelperAdapter(val fragment: Fragment): BaseLocationHelperAdapter {
    override fun requestPermission(perms: Array<String>, requestCode: Int) {
        fragment.requestPermissions(perms, requestCode)
    }

    override fun getContext(): Context? {
        return fragment.context
    }
}