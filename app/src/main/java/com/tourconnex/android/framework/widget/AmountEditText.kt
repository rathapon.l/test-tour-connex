package com.tourconnex.android.framework.widget

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import java.util.*

// research from
// https://stackoverflow.com/questions/28757931/how-to-format-the-input-of-edittext-when-typing-with-thousands-separators-in/28758064
// https://stackoverflow.com/questions/5107901/better-way-to-format-currency-input-edittext
// https://stackoverflow.com/questions/3821539/decimal-separator-comma-with-numberdecimal-inputtype-in-edittext
// https://stackoverflow.com/questions/12338445/how-to-automatically-add-thousand-separators-as-number-is-input-in-edittext
// http://android-designing.blogspot.com/2017/01/format-edittext-number-with-comma.html

interface AmountTextChangeListener {
    fun beforeTextChanged(s: CharSequence?, start: Int,
                          count: Int, after: Int)
    fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
    fun afterTextChanged(s: Editable?)
}

class AmountEditText: AppCompatEditText {

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init()
    }

    private fun init() {
        this.addTextChangedListener(onPriceChangeListener)
    }

    private var textChangeListener: AmountTextChangeListener? = null
    var ignoreTextChange = false

    fun setOnAmountTextChangeListener(textChangeListener: AmountTextChangeListener?) {
        this.textChangeListener = textChangeListener
    }

    fun getNoCommaString(): String? {
        return trimCommaOfString(this.text.toString())
    }

    private val onPriceChangeListener = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {

            val editText = this@AmountEditText
            try {
                editText.removeTextChangedListener(this)
                val value = editText.text.toString()
                val inilen = editText.text?.length ?: 0
                val cp = editText.selectionStart

                if (!value.isNullOrEmpty()) {

                    if (value.startsWith(".")) {
                        editText.setText("0.")
                    }
//                    if (value.startsWith("0") && !value.startsWith("0.")) {
//                        editText.setText("")
//                    }

                    val str = text.toString().replace(",".toRegex(), "")
                    if (value != "") {

                        editText.setText(getDecimalFormattedString(str))
                        val endlen = editText.text?.length ?: 0
                        val sel = cp + (endlen - inilen)
                        if (sel > 0 && sel <=  editText.text?.length ?: 0) {
                            editText.setSelection(sel)
                        } else {
                            editText.setSelection(editText.text.toString().length - 1)
                        }
                        //editText.setSelection(editText.text.toString().length)
                    }
                }
                editText.addTextChangedListener(this)
                if (ignoreTextChange) {
                    return
                }

                this@AmountEditText.textChangeListener?.afterTextChanged(p0)
                return
            } catch (ex: Exception) {
                ex.printStackTrace()
                editText.addTextChangedListener(this)
            }
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            if (ignoreTextChange) {
                return
            }

            this@AmountEditText.textChangeListener?.beforeTextChanged(p0,p1,p2,p3)
        }

        override fun onTextChanged(str: CharSequence?, p1: Int, p2: Int, p3: Int) {
            if (ignoreTextChange) {
                return
            }

            this@AmountEditText.textChangeListener?.onTextChanged(str,p1,p2,p3)
        }
    }

    private fun getDecimalFormattedString(value: String): String {
        if (value.isNullOrEmpty()) {
            return ""
        }

        val lst = StringTokenizer(value, ".")
        var str1 = value
        var str2 = ""
        if (lst.countTokens() > 1) {
            str1 = lst.nextToken()
            str2 = lst.nextToken()
        }
        var str3 = ""
        var i = 0
        var j = -1 + str1.length
        if (str1[-1 + str1.length] == '.') {
            j--
            str3 = "."
        }
        var k = j
        while (true) {
            if (k < 0) {
                if (str2.length > 0)
                    str3 = "$str3.$str2"
                return str3
            }
            if (i == 3) {
                str3 = ",$str3"
                i = 0
            }
            str3 = str1[k] + str3
            i++
            k--
        }
    }

    private fun trimCommaOfString(string: String): String {
        //        String returnString;
        return if (string.contains(",")) {
            string.replace(",", "")
        } else {
            string
        }

    }
}