package com.tourconnex.android.guest

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.users.form.ResetPasswordForm
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.framework.utils.StringUtils
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.view_toolbar.*
import retrofit2.Call
import retrofit2.Response

class ForgotPasswordActivity : AbstractActivity() {

    companion object {
        fun create(context: Context): Intent {
            return Intent(context, ForgotPasswordActivity::class.java)
        }
    }

    private var callResetPassword: Call<SimpleResponse>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.forgot_password_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        submitButton.setOnClickListener { performForgotPassword() }
    }

    private fun isValidToForgotPassword(): Boolean {
        var message: String? = null
        var valid = true

        if (emailEditText.text.isNullOrEmpty()) {
            message = getString(R.string.login_error_empty_username)
        } else if (!StringUtils.isEmailValid(emailEditText.text.toString())) {
            message = getString(R.string.login_error_invalid_email)
        }

        message?.let {
            valid = false
            showMessageDialog(it)
        }

        return valid
    }

    private fun performForgotPassword() {
        if (isValidToForgotPassword()) {
            val form = ResetPasswordForm(emailEditText.text.toString())
            callResetPassword?.cancel()
            callResetPassword = getApiClient().resetPassword(form)
            callResetPassword?.enqueue(object: MainThreadCallback<SimpleResponse>(this, simpleProgressBar) {
                override fun onSuccess(result: SimpleResponse?) {
                    if (result != null) {
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                }

                override fun onError(response: Response<SimpleResponse>?, error: Error?) {
                    showErrorDialog(error)
                }
            })
        }
    }

    override fun onDestroy() {
        callResetPassword?.cancel()
        super.onDestroy()
    }
}
