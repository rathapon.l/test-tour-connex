package com.tourconnex.android.guest

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.os.ConfigurationCompat
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import kotlinx.android.synthetic.main.activity_language_setting.*
import java.util.*

class LanguageSettingActivity : AbstractActivity() {

    companion object {
        fun create(context: Context): Intent {
            return Intent(context, LanguageSettingActivity::class.java)
        }
    }

    private lateinit var selectedLocale: Locale

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language_setting)

        val locales = ConfigurationCompat.getLocales(resources.configuration)
        if (locales.size() > 0) {
            selectedLocale = locales[0]
        } else {
            selectedLocale = Locale.getDefault()
        }

        if (selectedLocale.language != "th") {
            selectedLocale = Locale.ENGLISH
        }

        initUI()

        segmented.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.thButton -> {
                    selectedLocale = Locale("th")
                }

                R.id.enButton -> {
                    selectedLocale = Locale.ENGLISH
                }
            }
        }

        confirmButton.setOnClickListener { didSelectLanguage() }
    }

    private fun initUI() {
        when (selectedLocale) {
            Locale("th") -> {
                segmented.check((R.id.thButton))
            }
            Locale.ENGLISH -> {
                segmented.check((R.id.enButton))
            }
        }
    }

    private fun didSelectLanguage() {
        updateLocale(selectedLocale)
        setResult(Activity.RESULT_OK)
        finish()
    }
}
