package com.tourconnex.android.guest

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.LoginResponse
import com.tourconnex.android.domain.users.form.LoginWithEmailForm
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.framework.utils.StringUtils
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Response
import com.tourconnex.android.domain.common.Error


class LoginActivity: AbstractActivity() {

    companion object {

        private const val REQUEST_RESET_PASSWORD = 10752

        fun create(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

    private var callLogin: Call<LoginResponse>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        signupButton.setOnClickListener { loginWithEmail() }
        forgotPasswordTextView.setOnClickListener { startForgotPassword() }
    }

    private fun loginWithEmail() {
        if (!isValidToLoginWithEmail()) {
            return
        }

        val email = usernameEditText.text.toString().trim()
        val password = passwordEditText.text.toString().trim()
        val form = LoginWithEmailForm(email, password)

        callLogin?.cancel()
        callLogin = getApiClient().loginWithEmail(form)
        callLogin?.enqueue(object: MainThreadCallback<LoginResponse>(this, simpleProgressBar) {
            override fun onSuccess(result: LoginResponse?) {
                result?.let {
                    didReceiveLoginResponse(it)
                }
            }

            override fun onError(response: Response<LoginResponse>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    private fun isValidToLoginWithEmail(): Boolean {
        var message: String? = null
        var valid = true

        if (usernameEditText.text.isNullOrEmpty()) {
            message = getString(R.string.login_error_empty_username)
        } else if (!StringUtils.isEmailValid(usernameEditText.text.toString())) {
            message = getString(R.string.login_error_invalid_email)
        } else if (passwordEditText.text.isNullOrEmpty()) {
            message = getString(R.string.login_error_empty_password)
        }


        message?.let {
            valid = false
            showMessageDialog(it)
        }

        return valid
    }

    private fun didReceiveLoginResponse(loginResponse: LoginResponse) {
        loginResponse?.token?.let {
            TourConnex.getInstance().setRefreshToken(it.refreshToken)
            TourConnex.getInstance().setToken(it.accessToken)
        }

        TourConnex.getInstance().setUser(loginResponse.user)
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun startForgotPassword() {
        startActivityForResult(ForgotPasswordActivity.create(this), REQUEST_RESET_PASSWORD)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_RESET_PASSWORD -> {
                if (resultCode == Activity.RESULT_OK) {
                    showMessageDialog(getString(R.string.forgot_password_successfully))
                }
            }
        }
    }

    override fun onDestroy() {
        callLogin?.cancel()
        super.onDestroy()
    }
}
