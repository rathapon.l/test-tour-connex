package com.tourconnex.android.guest

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.LoginResponse
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.framework.utils.ImageUtils
import com.tourconnex.android.framework.utils.StringUtils
import kotlinx.android.synthetic.main.activity_register.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Response
import java.io.File
import com.tourconnex.android.domain.common.Error

class RegisterActivity : AbstractActivity() {

    companion object {
        private const val REQUEST_GALLERY = 112
        private const val REQUEST_CAMERA = 113
        private const val RC_CAMERA_AND_EXTERNAL_STORAGE = 164

        private const val UNKNOWN_GENDER = -1
        private const val MALE_GENDER = 1
        private const val FEMALE_GENDER = 2

        private const val MAX_PASSWORD_LENGTH = 6

        fun create(context: Context): Intent {
            return Intent(context, RegisterActivity::class.java)
        }
    }

    private var file: File? = null
    private var gender = UNKNOWN_GENDER
    private var callRegister: Call<LoginResponse>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        genderGroup.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.maleButton -> gender = MALE_GENDER
                R.id.femaleButton -> gender = FEMALE_GENDER
            }
        }

        profileImageView.setOnClickListener { openAddPhotoOptions() }
        signupButton.setOnClickListener { performRegister() }
    }

    private fun openAddPhotoOptions() {
        val builder = AlertDialog.Builder(this)
        builder.setItems(R.array.picture_selection_option) { dialog, which ->
            when (which) {
                0 -> openCamera()
                1 -> openGallery()
            }
            dialog.dismiss()
        }
        builder.show()
    }

    private fun openGallery() {
        EasyImage.openGallery(this, REQUEST_GALLERY)
    }

    @AfterPermissionGranted(RC_CAMERA_AND_EXTERNAL_STORAGE)
    private fun openCamera() {
        val perms = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            EasyImage.openCameraForImage(this, REQUEST_CAMERA)
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.register_camera_permission),
                RC_CAMERA_AND_EXTERNAL_STORAGE, *perms)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, onImagePickedCallback)
    }

    private val onImagePickedCallback = object : DefaultCallback() {
        override fun onImagesPicked(files: MutableList<File>, p1: EasyImage.ImageSource?, type: Int) {
            val f = files.firstOrNull()
            f?.let {
                this@RegisterActivity.file = ImageUtils.createRotatedAndResizedImage(it)
                updateUI()
            }
        }
    }

    private fun updateUI() {
        file?.let {
            Glide.with(this).load(file).apply(RequestOptions.centerCropTransform()).into(profileImageView)
        } ?: kotlin.run {
            Glide.with(this).load(R.drawable.icon_placeholder).apply(RequestOptions.centerInsideTransform()).into(profileImageView)
        }
    }

    private fun isValidToRegister(): Boolean {
        var message: String? = null
        var valid = true

        if (firstNameEditText.text.isNullOrEmpty()) {
            message = getString(R.string.register_error_fill_first_name)
        } else if (lastNameEditText.text.isNullOrEmpty()) {
            message = getString(R.string.register_error_fill_last_name)
        } else if (usernameEditText.text.isNullOrEmpty()) {
            message = getString(R.string.login_error_empty_username)
        } else if (!StringUtils.isEmailValid(usernameEditText.text.toString().trim())) {
            message = getString(R.string.login_error_invalid_email)
        } else if (passwordEditText.text.length < MAX_PASSWORD_LENGTH) {
            message = getString(R.string.login_password_length, MAX_PASSWORD_LENGTH)
        } else if (passwordEditText.text.toString() != confirmPasswordEditText.text.toString()) {
            message = getString(R.string.login_password_mismatch)
        }

        message?.let {
            valid = false
            showMessageDialog(it)
        }

        return valid
    }

    private fun createRequestBody(file: File): MultipartBody.Part {
        val body = RequestBody.create(MediaType.parse("image/*"), file)
        return MultipartBody.Part.createFormData("image", file.name, body)
    }

    private fun performRegister() {
        if (!isValidToRegister()) {
            return
        }

        val firstNameBody = RequestBody.create(MediaType.parse("text/plain"), firstNameEditText.text.toString().trim())
        val lastNameBody = RequestBody.create(MediaType.parse("text/plain"), lastNameEditText.text.toString().trim())
        val usernameBody = RequestBody.create(MediaType.parse("text/plain"), usernameEditText.text.toString().trim())
        val passwordBody = RequestBody.create(MediaType.parse("text/plain"), passwordEditText.text.toString().trim())
        //val genderBody = RequestBody.create(MediaType.parse("text/plain"), gender.toString())

        var filePart: MultipartBody.Part? = null
        file?.let { notNullFile ->
            filePart = createRequestBody(notNullFile)
        }

        callRegister?.cancel()
        callRegister = getApiClient().register(firstNameBody,
            lastNameBody,
            usernameBody,
            passwordBody,
            filePart
        )

        callRegister?.enqueue(object: MainThreadCallback<LoginResponse>(this, simpleProgressBar) {
            override fun onSuccess(result: LoginResponse?) {
                result?.let {
                    didReceiveLoginResponse(it)
                }
            }

            override fun onError(response: Response<LoginResponse>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    private fun didReceiveLoginResponse(loginResponse: LoginResponse) {
        loginResponse?.token?.let {
            TourConnex.getInstance().setRefreshToken(it.refreshToken)
            TourConnex.getInstance().setToken(it.accessToken)
        }

        TourConnex.getInstance().setUser(loginResponse.user)
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onDestroy() {
        callRegister?.cancel()
        super.onDestroy()
    }
}
