package com.tourconnex.android.guest

import android.os.Bundle
import android.os.CountDownTimer
import com.tourconnex.android.MainActivity
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.users.User
import com.tourconnex.android.framework.http.MainThreadCallback
import retrofit2.Call
import retrofit2.Response
import com.tourconnex.android.domain.common.Error
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AbstractActivity() {

    private val timer: SplashScreenCountDownTimer = SplashScreenCountDownTimer()
    private var callMe: Call<User>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        versionTextView.text = TourConnex.getVersionName(this)
        timer.start()
        getMe()
    }

    private fun getMe() {
        callMe?.cancel()
        callMe = getApiClient().getMe()
        callMe?.enqueue(object : MainThreadCallback<User>(this) {
            override fun onSuccess(result: User?) {
                result?.let {
                    TourConnex.getInstance().setUser(it)
                    updateSuccess()
                } ?: kotlin.run {
                    updateFailed()
                }
            }

            override fun onError(response: Response<User>?, error: Error?) {
                updateFailed()
            }

            override fun onFailure(call: Call<User>?, t: Throwable?) {
                updateFailed()
            }
        })
    }

    private fun startMain() {
        startActivity(MainActivity.create(this))
        finishWithoutAnimation()
    }

    private fun startWelcome() {
        startActivity(WelcomeActivity.create(this))
        finishWithoutAnimation()
    }

    private fun updateSuccess() {
        if (timer.isFinish) {
            timer.cancel()
            startMain()
        } else {
            timer.success = true
        }
    }

    private fun updateFailed() {
        if (timer.isFinish) {
            timer.cancel()
            startWelcome()
        } else {
            timer.success = true
        }
    }

    private inner class SplashScreenCountDownTimer : CountDownTimer(1000, 1000) {
        var isFinish = false
        var success = false

        override fun onFinish() {
            isFinish = true
            if (success) {
                if (TourConnex.getInstance().getUser() != null) {
                    updateSuccess()
                } else {
                    updateFailed()
                }
            }
        }

        override fun onTick(millisUntilFinished: Long) {

        }
    }

    override fun onDestroy() {
        callMe?.cancel()
        super.onDestroy()
    }
}
