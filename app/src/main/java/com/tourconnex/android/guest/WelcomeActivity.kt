package com.tourconnex.android.guest

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.GoogleAuthUtil
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.linecorp.linesdk.LineApiResponseCode
import com.linecorp.linesdk.Scope
import com.linecorp.linesdk.auth.LineAuthenticationParams
import com.linecorp.linesdk.auth.LineLoginApi
import com.linecorp.linesdk.auth.LineLoginResult
import com.tourconnex.android.MainActivity
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.LoginResponse
import com.tourconnex.android.domain.users.form.LoginForm
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.guest.model.WelcomeItem
import kotlinx.android.synthetic.main.activity_welcome.*
import retrofit2.Call
import retrofit2.Response
import java.lang.ref.WeakReference
import java.util.*

class WelcomeActivity : AbstractActivity() {

    companion object {
        private const val REQUEST_SIGNUP = 10015
        private const val REQUEST_LOGIN = 10016

        private const val REQUEST_GOOGLE_LOGIN = 10001
        private const val REQUEST_LINE_LOGIN = 10002

        private const val LOGIN_TYPE_FACEBOOK = "facebook"
        private const val LOGIN_TYPE_LINE = "line"
        private const val LOGIN_TYPE_GOOGLE = "google"

        fun create(context: Context): Intent {
            return Intent(context, WelcomeActivity::class.java)
        }
    }

    private var viewPagerAdapter = WelcomePagerAdapter(this)
    private lateinit var callbackManager: CallbackManager
    private var callLogin: Call<LoginResponse>? = null
    private lateinit var mGoogleSignInClient: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        signupButton.setOnClickListener { startSignup() }
        loginButton.setOnClickListener { startLogin() }

        callbackManager = CallbackManager.Factory.create()
        setupGoogleSignInClient()
        LoginManager.getInstance().registerCallback(callbackManager, facebookListener)

        viewPager.adapter = viewPagerAdapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {

            }
        })

        loginWithFacebookButton.setOnClickListener { loginWithFacebook() }
        loginWithLineGoogle.setOnClickListener { loginWithGoogle() }
        loginWithLineButton.setOnClickListener { loginWithLine() }

        versionTextView.text = TourConnex.getVersionName(this)

        setupWelcomeItems()
        //startLanguasetting()
    }

    private fun loginWithFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"))
    }

    private fun loginWithGoogle() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, REQUEST_GOOGLE_LOGIN)
    }

    private fun loginWithLine() {
        val loginIntent = LineLoginApi.getLoginIntent(
            applicationContext,
            getString(R.string.line_chanel_id),
            LineAuthenticationParams.Builder()
                .scopes(Arrays.asList(Scope.PROFILE, Scope.OPENID_CONNECT, Scope.OC_EMAIL))
                .build()
        )
        startActivityForResult(loginIntent, REQUEST_LINE_LOGIN)
    }

    private fun setupGoogleSignInClient() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.google_login_server_client_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    private fun startLanguasetting() {
        startActivity(LanguageSettingActivity.create(this))
    }

    private fun startMain() {
        startActivity(MainActivity.create(this))
        finishWithoutAnimation()
    }

    private fun setupWelcomeItems() {
        val welcomeItems = ArrayList<WelcomeItem>()
        welcomeItems.add(WelcomeItem(getString(R.string.welcome_title1), getString(R.string.welcome_description1)))
        welcomeItems.add(WelcomeItem(getString(R.string.welcome_title2), getString(R.string.welcome_description2)))
        welcomeItems.add(WelcomeItem(getString(R.string.welcome_title3), getString(R.string.welcome_description3)))

        viewPagerAdapter.addItem(welcomeItems)
    }

    class WelcomePagerAdapter(private val mContext: Context) : PagerAdapter() {
        private var items = ArrayList<WelcomeItem>()

        override fun instantiateItem(collection: ViewGroup, position: Int): Any {
            val inflater = LayoutInflater.from(mContext)
            val layout = inflater.inflate(R.layout.view_list_welcom_pager, collection, false) as ViewGroup
            val titleTextView = layout.findViewById<TextView>(R.id.titleTextView)
            val descriptionTextView = layout.findViewById<TextView>(R.id.descriptionTextView)
            val item = items[position]
            titleTextView.text = item.title
            descriptionTextView.text = item.description


            collection.addView(layout)
            return layout
        }

        override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
            collection.removeView(view as View)
        }

        override fun getCount(): Int {
            return items.count()
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object`
        }

        fun addItem(entities: ArrayList<WelcomeItem>?) {
            entities?.let {
                items = ArrayList<WelcomeItem>()
                items.addAll(it)
                notifyDataSetChanged()
            }
        }
    }

    private fun startLogin() {
        startActivityForResult(LoginActivity.create(this), REQUEST_LOGIN)
    }

    private fun startSignup() {
        startActivityForResult(RegisterActivity.create(this), REQUEST_SIGNUP)
    }

    private fun didReceiveLineToken(token: String) {
        loginWithToken(token, LOGIN_TYPE_LINE)
    }

    private fun didReceiveGoogleToken(token: String) {
        loginWithToken(token, LOGIN_TYPE_GOOGLE)
    }

    private fun didReceiveFacebookToken(token: String) {
        loginWithToken(token, LOGIN_TYPE_FACEBOOK)
    }

    private fun loginWithToken(token: String, type: String) {
        val form = LoginForm(token, type)
        callLogin?.cancel()
        callLogin = getApiClient().loginWithFacebook(form)
        callLogin?.enqueue(object : MainThreadCallback<LoginResponse>(this, simpleProgressBar) {
            override fun onSuccess(result: LoginResponse?) {
                result?.let {
                    didReceiveLoginResponse(it)
                }
            }

            override fun onError(response: Response<LoginResponse>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    private fun didReceiveLoginResponse(loginResponse: LoginResponse) {
        loginResponse?.token?.let {
            TourConnex.getInstance().setRefreshToken(it.refreshToken)
            TourConnex.getInstance().setToken(it.accessToken)
        }

        TourConnex.getInstance().setUser(loginResponse.user)
        startMain()
    }

    private val facebookListener = (object : FacebookCallback<LoginResult> {
        override fun onSuccess(result: LoginResult?) {
            result?.accessToken?.let {
                didReceiveFacebookToken(it.token)
            }
        }

        override fun onCancel() {

        }

        override fun onError(error: FacebookException?) {
            showMessageDialog(error?.localizedMessage ?: "error")
        }
    })

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            account?.idToken?.let {
                didReceiveGoogleToken(it)
//                GetGoogleAccessTokenTask(WeakReference(this), it) { accessToken ->
//                    didReceiveGoogleToken(accessToken ?: "")
//                }.execute()
            }


        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.d("error", "signInResult:failed code=" + e.statusCode)
        }

    }

    private fun handleLineLoginResult(result: LineLoginResult) {
        when (result.responseCode) {
            LineApiResponseCode.SUCCESS -> {
                val token = result.lineCredential?.accessToken?.tokenString
                token?.let {
                    didReceiveLineToken(it)
                }
            }

            LineApiResponseCode.CANCEL -> {

            }
            else -> {

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        callbackManager.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_SIGNUP -> {
                if (resultCode == Activity.RESULT_OK) {
                    startMain()
                }
            }

            REQUEST_LOGIN -> {
                if (resultCode == Activity.RESULT_OK) {
                    startMain()
                }
            }
            REQUEST_GOOGLE_LOGIN -> {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(task)
            }
            REQUEST_LINE_LOGIN -> {
                val result = LineLoginApi.getLoginResultFromIntent(data)
                handleLineLoginResult(result)
            }
        }
    }

    override fun onDestroy() {
        callLogin?.cancel()
        LoginManager.getInstance().unregisterCallback(callbackManager)
        super.onDestroy()
    }

    private data class GetGoogleAccessTokenTask(val context: WeakReference<Context>, val account: GoogleSignInAccount, val completion: ((String?)->(Unit))): AsyncTask<Void, Void, String?>() {
        override fun doInBackground(vararg params: Void?): String? {
            var token: String? = null
            try {
                val scopes = "oauth2:${Scopes.PROFILE} ${Scopes.EMAIL}"
                val accessToken = GoogleAuthUtil.getToken(context.get(), account.account, scopes)
                token = accessToken
            } catch (ex: Exception) {
            }

            return token
        }

        override fun onPostExecute(result: String?) {
            completion(result)
        }
    }
}
