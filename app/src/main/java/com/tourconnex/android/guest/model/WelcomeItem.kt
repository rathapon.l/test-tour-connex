package com.tourconnex.android.guest.model

data class WelcomeItem(val title: String?, val description: String?)