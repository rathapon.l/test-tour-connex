package com.tourconnex.android.hotel

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.bumptech.glide.Glide
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.Hotel
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.map.HotelLocationActivity
import kotlinx.android.synthetic.main.activity_hotel.*
import kotlinx.android.synthetic.main.view_toolbar.*
import retrofit2.Call
import retrofit2.Response

class HotelActivity : AbstractActivity() {

    companion object {

        private const val HOTEL_EXTRA = "HOTEL_EXTRA"

        fun create(context: Context, hotel: Hotel?): Intent {
            return Intent(context, HotelActivity::class.java).apply {
                putExtra(HOTEL_EXTRA, hotel)
            }
        }
    }

    private var hotel: Hotel? = null
    private var callHotel: Call<Hotel>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel)

        hotel = intent?.getParcelableExtra(HOTEL_EXTRA)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = hotel?.name
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        swipeRefreshLayout.setOnRefreshListener { fetchData() }
        hotelAddressView.setOnClickListener { openMapIfNeeded() }

        updateUI()
        fetchData()
    }

    private fun openMapIfNeeded() {
        startActivity(HotelLocationActivity.create(this, hotel))
    }

    private fun fetchData() {
        callHotel?.cancel()
        callHotel = getApiClient().getHotel(hotel?.id)
        callHotel?.enqueue(object: MainThreadCallback<Hotel>(this, simpleProgressBar) {
            override fun onSuccess(result: Hotel?) {
                swipeRefreshLayout.isRefreshing = false
                this@HotelActivity.hotel = hotel
            }

            override fun onError(response: Response<Hotel>?, error: Error?) {
                swipeRefreshLayout.isRefreshing = false
                //showErrorDialog(error)
            }
        })
    }

    private fun updateUI() {
        Glide.with(this).load(hotel?.coverPhoto?.largeImageUrl).into(hotelCoverImageView)
        hotelNameTextView.text = hotel?.name
        hotelDetailsTextView.text = hotel?.description
        hotelAddressTextView.text = hotel?.address
        hotelPhoneNumberTextView.text = hotel?.phoneNumber
    }

    override fun onDestroy() {
        callHotel?.cancel()
        super.onDestroy()
    }
}
