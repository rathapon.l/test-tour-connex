package com.tourconnex.android.images

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.tourconnex.android.R
import kotlinx.android.synthetic.main.activity_image_preview.*
import org.apache.commons.io.FilenameUtils
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.io.*
import java.net.URL


class ImagePreviewActivity : AppCompatActivity() {

    companion object {
        private const val RC_CAMERA_AND_EXTERNAL_STORAGE = 164
        private const val EXTRA_IMAGE_URL = "EXTRA_IMAGE_URL"

        fun create(context: Context, imageUrl: String?): Intent {
            return Intent(context, ImagePreviewActivity::class.java).apply {
                putExtra(EXTRA_IMAGE_URL, imageUrl)
            }
        }
    }

    private var imageUrl: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_preview)
        imageUrl = intent?.getStringExtra(EXTRA_IMAGE_URL)
        dismissButton.setOnClickListener {
            finish()
        }
        imageUrl?.let {
            Glide.with(this).load(it).apply(RequestOptions.centerInsideTransform()).into(photoView)
        }
        downloadButton.setOnClickListener { downloadFile() }
    }

    @AfterPermissionGranted(RC_CAMERA_AND_EXTERNAL_STORAGE)
    private fun downloadFile() {
        val perms = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            Glide.with(this).asFile()
                .load(imageUrl)
                .into(object : CustomTarget<File>() {
                    override fun onLoadCleared(placeholder: Drawable?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onResourceReady(resource: File, transition: Transition<in File>?) {
                        storeImage(resource)
                    }

                })
        } else {
            EasyPermissions.requestPermissions(
                this,
                getString(R.string.register_camera_permission),
                RC_CAMERA_AND_EXTERNAL_STORAGE,
                *perms
            )
        }
    }

    private fun storeImage(image: File) {
        val pictureFile = getOutputMediaFile() ?: return
        try {
            val output = FileOutputStream(pictureFile)
            val input = FileInputStream(image)
            val inputChannel = input.channel
            val outputChannel = output.channel
            inputChannel.transferTo(0, inputChannel.size(), outputChannel)
            output.close()
            input.close()

            val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
            val contentUri = Uri.fromFile(pictureFile)
            mediaScanIntent.data = contentUri
            sendBroadcast(mediaScanIntent)
            Toast.makeText(this, "Image Downloaded", Toast.LENGTH_SHORT).show()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun getOutputMediaFile(): File? {
        val mediaStorageDir =
            File(Environment.getExternalStorageDirectory().toString() + File.separator + "TourConnex")
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs())
                return null
        }
        val url = URL(imageUrl)
        val mediaFile: File
        mediaFile = File(mediaStorageDir.path + File.separator + FilenameUtils.getName(url.path))
        return mediaFile
    }

}
