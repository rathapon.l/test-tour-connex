package com.tourconnex.android.images

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.conversation.Message
import kotlinx.android.synthetic.main.activity_image_viewpager.*
import org.apache.commons.io.FilenameUtils
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.io.*
import java.net.URL

class ImageViewPagerActivity : AbstractActivity() {
    companion object {
        private const val EXTRA_CONVERSATION = "extra-conversation"
        private const val EXTRA_MESSAGE = "extra-message"
        private const val RC_CAMERA_AND_EXTERNAL_STORAGE = 164

        fun create(context: Context, conversationId: String, message: Message): Intent {
            return Intent(context, ImageViewPagerActivity::class.java).apply {
                putExtra(EXTRA_CONVERSATION, conversationId)
                putExtra(EXTRA_MESSAGE, message)
            }
        }
    }

    private lateinit var adapter: PhotoViewLoaderAdapter
    private lateinit var message: Message
    private lateinit var conversationId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        intent?.getStringExtra(EXTRA_CONVERSATION)?.let {
            conversationId = it
        }
        intent?.getParcelableExtra<Message>(EXTRA_MESSAGE)?.let {
            message = it
        }
        setContentView(R.layout.activity_image_viewpager)
        setSupportActionBar(actionBarView)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        adapter = PhotoViewLoaderAdapter(conversationId, message, viewPager) {
            it?.let {
                downloadFile(it)
            }
        }
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                if (position == 1 && adapter.hasPrevious()) {
                    adapter.loadPrevious()
                } else if (position == adapter.count - 2 && adapter.hasNext()) {
                    adapter.loadNext()
                }
            }
        })
        adapter.preload()
    }

    @AfterPermissionGranted(RC_CAMERA_AND_EXTERNAL_STORAGE)
    private fun downloadFile(imageUrl: String) {
        val perms = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            Glide.with(this).asFile()
                .load(imageUrl)
                .into(object : CustomTarget<File>() {
                    override fun onLoadCleared(placeholder: Drawable?) {

                    }

                    override fun onResourceReady(resource: File, transition: Transition<in File>?) {
                        storeImage(resource, imageUrl)
                    }

                })
        } else {
            EasyPermissions.requestPermissions(
                this,
                getString(R.string.register_camera_permission),
                RC_CAMERA_AND_EXTERNAL_STORAGE,
                *perms
            )
        }
    }

    private fun storeImage(image: File, imageUrl: String) {
        val pictureFile = getOutputMediaFile(imageUrl) ?: return
        try {
            val output = FileOutputStream(pictureFile)
            val input = FileInputStream(image)
            val inputChannel = input.channel
            val outputChannel = output.channel
            inputChannel.transferTo(0, inputChannel.size(), outputChannel)
            output.close()
            input.close()

            val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
            val contentUri = Uri.fromFile(pictureFile)
            mediaScanIntent.data = contentUri
            sendBroadcast(mediaScanIntent)
            Toast.makeText(this, "Image Downloaded", Toast.LENGTH_SHORT).show()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun getOutputMediaFile(imageUrl: String): File? {
        val mediaStorageDir =
            File(Environment.getExternalStorageDirectory().toString() + File.separator + "TourConnex")
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs())
                return null
        }
        val url = URL(imageUrl)
        val mediaFile: File
        mediaFile = File(mediaStorageDir.path + File.separator + FilenameUtils.getName(url.path))
        return mediaFile
    }


    override fun onDestroy() {
        adapter.destroy()
        super.onDestroy()
    }

}