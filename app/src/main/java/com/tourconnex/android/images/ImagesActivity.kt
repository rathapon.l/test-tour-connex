package com.tourconnex.android.images

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.trip.Image
import com.tourconnex.android.framework.widget.GridSpacingItemDecoration
import com.tourconnex.android.trip.adapter.TripDayImageAdapter
import kotlinx.android.synthetic.main.activity_images.*
import kotlinx.android.synthetic.main.view_toolbar.*

class ImagesActivity : AbstractActivity() {

    companion object {

        private const val EXTRA_IMAGES = "EXTRA_IMAGES"

        fun create(context: Context, images: ArrayList<Image>?): Intent {
            return Intent(context, ImagesActivity::class.java).apply {
                putParcelableArrayListExtra(EXTRA_IMAGES, images)
            }
        }
    }

    private val adapter = TripDayImageAdapter { image, _ ->
        previewImage(image)
    }


    private var images: ArrayList<Image>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_images)

        images = intent?.getParcelableArrayListExtra(EXTRA_IMAGES)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.images_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val spanCount =  3
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.spaceNormal)
        val includeEdge = true
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(this, spanCount)
        recyclerView.addItemDecoration(GridSpacingItemDecoration(spanCount, spacingInPixels, includeEdge))

        images?.let {
            adapter.addAll(it)
        }
    }

    private fun previewImage(image: Image?) {
        startActivity(ImagePreviewActivity.create(this, image?.largeImageUrl))
    }
}
