package com.tourconnex.android.images

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.novoda.viewpageradapter.ViewPagerAdapter
import com.tourconnex.android.R
import com.tourconnex.android.domain.api.ApiClient
import com.tourconnex.android.domain.conversation.Message
import com.tourconnex.android.domain.conversation.PageMessage
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.framework.services.ServiceLocator
import kotlinx.android.synthetic.main.activity_image_view.view.*
import retrofit2.Call

class PhotoViewLoaderAdapter(
    private val conversationId: String,
    message: Message,
    private val viewPager: ViewPager,
    private val onDownloadClick: (String?) -> Unit
) :
    ViewPagerAdapter<FrameLayout>() {
    private var callNext: Call<PageMessage>? = null
    private var callPrevious: Call<PageMessage>? = null
    private val items = ArrayList<Message>()
    private var hasNext = false
    private var hasPrevious = false
    private var nextLoading = false
    private var previousLoading = false

    init {
        items.add(message)
    }

    fun preload() {
        loadNext()
        loadPrevious()
    }

    fun destroy() {
        callNext?.cancel()
        callPrevious?.cancel()
    }

    fun loadNext() {
        if (!nextLoading) {
            nextLoading = true
            callNext?.cancel()
            callNext =
                getApiClient().getConversationMessagesByType(
                    conversationId,
                    2,
                    items.last().id,
                    true
                )
            callNext?.enqueue(object : MainThreadCallback<PageMessage>(null) {
                override fun onSuccess(result: PageMessage?) {
                    nextLoading = false
                    result?.entities?.let {
                        items.addAll(it)
                        hasNext = it.size >= 20
                        notifyDataSetChanged()
                    } ?: run {
                        hasNext = false
                    }
                }
            })
        }
    }

    fun loadPrevious() {
        if (!previousLoading) {
            previousLoading = true
            callPrevious?.cancel()
            callPrevious =
                getApiClient().getConversationMessagesByType(
                    conversationId,
                    2,
                    items.first().id,
                    false
                )
            callPrevious?.enqueue(object : MainThreadCallback<PageMessage>(null) {
                override fun onSuccess(result: PageMessage?) {
                    previousLoading = false
                    result?.entities?.let {
                        val position = viewPager.currentItem + it.size
                        items.addAll(0, it.reversed())
                        hasPrevious = it.size >= 20
                        notifyDataSetChanged()
                        viewPager.setCurrentItem(position, false)
                    } ?: run {
                        hasPrevious = false
                    }
                }
            })
        }
    }

    fun hasNext(): Boolean {
        return hasNext
    }

    fun hasPrevious(): Boolean {
        return hasPrevious
    }

    override fun createView(container: ViewGroup?, position: Int): FrameLayout {
        val view = LayoutInflater.from(container?.context)
            .inflate(R.layout.activity_image_view, container, false) as FrameLayout
        view.downloadButton.setOnClickListener {
            onDownloadClick(it.tag as String?)
        }
        return view
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun bindView(view: FrameLayout?, position: Int) {
        view?.downloadButton?.tag = items[position].images?.firstOrNull()?.largeImageUrl
        view?.context?.let {
            Glide.with(it)
                .load(items[position].images?.firstOrNull()?.largeImageUrl)
                .placeholder(R.drawable.ic_place_holder)
                .apply(RequestOptions.centerInsideTransform())
                .into(view.photoView)
        }
    }

    private fun getApiClient(): ApiClient {
        return ServiceLocator.getInstance().getService(ApiClient::class.java)
    }

}

