package com.tourconnex.android.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.conversation.ConversationActivity
import com.tourconnex.android.conversation.OnConversationActivityResult
import com.tourconnex.android.domain.fragment.AbstractFragment
import com.tourconnex.android.domain.api.ApiClient
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.conversation.*
import com.tourconnex.android.domain.conversation.form.ConversationItem
import com.tourconnex.android.domain.trip.TripTraveller
import com.tourconnex.android.domain.users.User
import com.tourconnex.android.domain.users.UserInterface
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.framework.manager.ConversationManager
import com.tourconnex.android.framework.services.ServiceLocator
import kotlinx.android.synthetic.main.fragment_chats.*
import kotlinx.android.synthetic.main.view_list_conversation.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Call
import retrofit2.Response
import android.content.DialogInterface
import android.widget.EditText
import android.widget.LinearLayout
import com.tourconnex.android.domain.announcement.Announcement
import com.tourconnex.android.domain.announcement.PageAnnouncement
import com.tourconnex.android.domain.announcement.form.CreateAnnouncementForm
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.users.Traveller
import kotlinx.android.synthetic.main.view_list_announcement.view.*


class ChatsFragment : AbstractFragment() {

    companion object {
        private const val REQUEST_CONVERSATION = 23445
    }

    private var activeTrip: TripTraveller? = null

    private val groupAdapter = ConversationAdapter { conversation ->
        didSelectConversation(conversation)
    }

    private val memberAdapter = ConversationAdapter { conversation ->
        didSelectConversation(conversation)
    }

    private val announcementAdapter = AnnouncementAdapter { announcement ->
        announcement?.let { removeAnnounceConfirmation(it) }
    }

    private var callConversation: Call<PageConversation>? = null

    private var getAnnouncement: Call<PageAnnouncement>? = null
    private var callCreateAnnouncement: Call<Announcement>? = null
    private var callRemoveAnnouncement: Call<SimpleResponse>? = null

    private val visibleThreshold = 5
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private var loading: Boolean = false

    private var nextPageId: String? = null

    private var associateUsers: ArrayList<Traveller>? = null
    private var isVisibleToUser = false
    private var isNeedToRefresh = false

    private var announcementMenu: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_chats, container, false)
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.common_announcement, menu)

        announcementMenu = menu.findItem(R.id.actionAnnouncement)

        updateMenuItemUI()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        refreshingLayout.setOnRefreshListener {
            fetchConversationIfNeeded()
        }

        groupRecyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        groupRecyclerView.adapter = groupAdapter

        memberRecyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        memberRecyclerView.adapter = memberAdapter

        announcementRecyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        announcementRecyclerView.adapter = announcementAdapter

        groupContainerView.visibility = View.GONE
        membersContainerView.visibility = View.GONE
        announcementContainerView.visibility = View.GONE
    }

    override fun onStart() {
        super.onStart()
        if (isNeedToRefresh) {
            isNeedToRefresh = false
            fetchConversationIfNeeded()
        } else {
            isNeedToRefresh = true
        }
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun getMessage(s: ConversationRetriveEvent) {
        fetchConversationIfNeeded()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        this.isVisibleToUser = isVisibleToUser
        fetchConversationIfNeeded()
    }

    private fun updateMenuItemUI() {
        val activeTrip = TourConnex.getInstance().getActiveTrip()
        announcementMenu?.isVisible = (activeTrip?.isTourLeader() == true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionAnnouncement -> {
                activity?.let { showCreateAnnouncementDialog(it) }
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun showCreateAnnouncementDialog(context: Context) {
        val alert = AlertDialog.Builder(context)
            val editText = EditText(context)
        alert.setTitle(getString(R.string.chat_announcement_title))
        editText.hint = getString(R.string.chat_announcement_placeholder)

        val layout = LinearLayout(context)
        layout.setPadding(15,15,15,0)

        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        layout.addView(editText,lp)

        alert.setView(layout)

        alert.setPositiveButton(
            getString(R.string.common_ok)
        ) { dialog, whichButton ->
            val text = editText.text.toString()
            performCreateAnnouncement(text)
            dialog.dismiss()
        }

        alert.setNegativeButton(getString(R.string.common_cancel), object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, whichButton: Int) {
                dialog.dismiss()
            }
        })

        alert.show()
    }

    private fun removeAnnounceConfirmation(announcement: Announcement) {
        val builder = AlertDialog.Builder(activity!!)
        builder.setMessage(R.string.chat_announcement_remove_confirmation)
        builder.setPositiveButton(R.string.common_ok) { dialog, _ ->
            dialog.dismiss()
            performRemoveAnnouncement(announcement)
        }
        builder.setNegativeButton(R.string.common_cancel) { dialog, _ -> dialog.cancel() }
        builder.show()
    }

    private fun performRemoveAnnouncement(announcement: Announcement) {
        callRemoveAnnouncement?.cancel()
        callRemoveAnnouncement = getApiClient().deleteAnnouncement(activeTrip?.id, announcement.id)
        callRemoveAnnouncement?.enqueue(object: MainThreadCallback<SimpleResponse>(activity) {
            override fun onSuccess(result: SimpleResponse?) {
                if (result != null) {
                    didRemoveAnnouncement(announcement)
                }
            }

            override fun onError(response: Response<SimpleResponse>?, error: Error?) {
                showErrorDialog(error)
            }
        })

    }

    private fun didRemoveAnnouncement(announcement: Announcement) {
        val tempAnnouncements = announcementAdapter.items.filter { it.id != announcement.id }
        val entities = ArrayList<Announcement>()
        entities.addAll(tempAnnouncements)
        announcementAdapter.addAll(entities)
    }

    private fun performCreateAnnouncement(message: String?) {
        val activeTrip = TourConnex.getInstance().getActiveTrip()
        if (activeTrip == null) {
            return
        }

        val form = CreateAnnouncementForm(message)
        callCreateAnnouncement?.cancel()
        callCreateAnnouncement = getApiClient().createAnnouncement(activeTrip.id, form)
        callCreateAnnouncement?.enqueue(object: MainThreadCallback<Announcement>(activity){
            override fun onSuccess(result: Announcement?) {
                result?.let {
                    announcementContainerView.visibility = View.VISIBLE
                    announcementAdapter.items.add(0, it)
                    announcementAdapter.notifyItemChanged(0)
                }
            }

            override fun onError(response: Response<Announcement>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    private fun fetchConversationIfNeeded() {
        if (isVisibleToUser) {
            checkActiveTripIfNeeded()
            noAvailableView.visibility = if (activeTrip?.checkedIn == true || TourConnex.getInstance().getUser()?.isGuide == true) View.GONE else View.VISIBLE
            if (activeTrip != null) {
                fetchConversationAssociateItemAndConversationRoom(null)
                fetchAnnouncements()
            } else {
                groupContainerView.visibility = View.GONE
                membersContainerView.visibility = View.GONE
                announcementContainerView.visibility = View.GONE
            }
        }
    }

    private fun checkActiveTripIfNeeded() {
        val tourConnex = TourConnex.getInstance()
        activeTrip = tourConnex.getActiveTrip()
    }

    private fun fetchAnnouncements() {
        getAnnouncement?.cancel()
        getAnnouncement = getApiClient().getAnnouncements(activeTrip?.id)
        getAnnouncement?.enqueue(object: MainThreadCallback<PageAnnouncement>(activity) {
            override fun onSuccess(result: PageAnnouncement?) {
                result?.entities?.let {
                    announcementContainerView.visibility = if (it.count() > 0) View.VISIBLE else View.GONE
                    val entities = ArrayList<Announcement>()
                    entities.addAll(it)
                    announcementAdapter.addAll(entities)
                } ?: Runnable {
                    announcementContainerView.visibility = View.GONE
                }
            }

            override fun onError(response: Response<PageAnnouncement>?, error: Error?) {

            }
        })
    }

    private fun fetchConversationAssociateItemAndConversationRoom(nextPageId: String?) {
        if (activity == null) {
            return
        }

        if (nextPageId == null) {
            associateUsers = null
            activeTrip?.trip?.id?.let { ConversationManager.getInstance().getConversationAssociateItems(activity!!, it) { conversationAssociateItem, error ->
                if (conversationAssociateItem != null) {
                    handleAssociateConversation(conversationAssociateItem, nextPageId)
                } else if (error != null) {
                    showErrorDialog(error)
                }
            }}
        } else {
            handleAssociateConversation(null, nextPageId)
        }
    }

    private fun handleAssociateConversation(conversationAssociateItem: ConversationAssociteItem?, nextPageId: String?) {
        associateUsers = ArrayList()
        conversationAssociateItem?.guides?.let { associateUsers?.addAll(it) }
        conversationAssociateItem?.travellers?.let { associateUsers?.addAll(it) }

        fetchConversations(nextPageId)
    }

    private fun fetchConversations(nextPageId: String?) {
        callConversation?.cancel()
        callConversation = getApiClient().getConversations(activeTrip?.trip?.id)
        callConversation?.enqueue(object : MainThreadCallback<PageConversation>(activity) {
            override fun onSuccess(result: PageConversation?) {
                refreshingLayout.isRefreshing = false
                val entities = ArrayList<ConversationItem>()
                result?.entities?.let {
                    if (nextPageId == null) {
                        entities.addAll(it.map {
                            val item = ConversationItem()
                            item.conversiontion = it
                            item.displayname = getDisplayName(it)
                            item.displayImageUrl = getDisplayImageUrl(it)

                            item
                        })
                    } else {
                        entities.addAll(groupAdapter.items)
                        entities.addAll(it.map {
                            val item = ConversationItem()
                            item.conversiontion = it
                            item.displayname = getDisplayName(it)
                            item.displayImageUrl = getDisplayImageUrl(it)

                            item
                        })
                    }
//
                    val members = entities.filter { it.conversiontion?.getConversationType() == Conversation.CONVERSATION_TYPE_ONE_ON_ONE }
                    val groups = entities.filter { it.conversiontion?.getConversationType() == Conversation.CONVERSATION_TYPE_GROUP }
                    groupContainerView.visibility = View.GONE
                    if (groups.count() > 0) {
                        groupContainerView.visibility = View.VISIBLE
                        val list = ArrayList<ConversationItem>()
                        list.addAll(groups)
                        groupAdapter.addAll(list)
                    }

                    membersContainerView.visibility = View.GONE
                    if (members.count() > 0) {
                        membersContainerView.visibility = View.VISIBLE
                        val list = ArrayList<ConversationItem>()
                        list.addAll(members)
                        memberAdapter.addAll(list)
                    }


                    this@ChatsFragment.nextPageId = result?.pageInformation?.nextPageId
                    loading = false
                }
            }

            override fun onError(response: Response<PageConversation>?, error: Error?) {
                refreshingLayout.isRefreshing = false
                loading = false
            }

            override fun onFailure(call: Call<PageConversation>?, t: Throwable?) {
                refreshingLayout.isRefreshing = false
                loading = false
            }

            private fun getDisplayName(conversation: Conversation): String? {
                when (conversation.getConversationType()) {
                    Conversation.CONVERSATION_TYPE_GROUP -> {
                        return activeTrip?.trip?.title
                    }

                    Conversation.CONVERSATION_TYPE_ONE_ON_ONE -> {
                        val opponentUser = getOpponentUser(conversation)
                        return opponentUser?.user?.getFullName()
                    }
                }

                return null
            }

            private fun getDisplayImageUrl(conversation: Conversation): String? {
                when (conversation.getConversationType()) {
                    Conversation.CONVERSATION_TYPE_GROUP -> {
                        return activeTrip?.trip?.coverPhoto?.thumbnailImageUrl
                    }

                    Conversation.CONVERSATION_TYPE_ONE_ON_ONE -> {
                        val opponentUser = getOpponentUser(conversation)
                        return opponentUser?.user?.profilePicture?.thumbnailImageUrl
                    }
                }

                return null
            }


            private fun getOpponentUser(conversation: Conversation): UserInterface? {
                val user = TourConnex.getInstance().getUser()
                val opponentUserId = conversation.userIds?.filter { it != user?.id }?.firstOrNull()
                val opponentUser = associateUsers?.filter { it.user?.id == opponentUserId }?.firstOrNull()

                return opponentUser
            }
        })
    }

    private fun didSelectConversation(conversation: ConversationItem?) {
        val list = ArrayList<Traveller>()
        associateUsers?.mapNotNull { it }?.let {
            list.addAll(it)
        }

        activity?.let { startActivityForResult(ConversationActivity.create(it,
            conversation?.conversiontion?.id, conversation?.displayname, list),
            REQUEST_CONVERSATION) }
    }

    private fun getLatestMessageFromMessage(message: Message?): String? {
        var latestMessage = message?.message
        when (message?.type?.toInt()) {
            MessageType.IMAGE -> {
                latestMessage = this.getString(R.string.message_imageg_file)
            }
            MessageType.VOICE -> {
                latestMessage = this.getString(R.string.message_audio_file)
            }
            MessageType.VIDEO -> {
                latestMessage = this.getString(R.string.message_video_file)
            }
        }

        return latestMessage
    }

    private fun handleReadMessage(message: Message?, conversation: Conversation?) {
        when (conversation?.getConversationType()) {
            Conversation.CONVERSATION_TYPE_GROUP -> {
                val index = groupAdapter.items.indexOfFirst { it.conversiontion?.id == conversation.id }
                if (index != -1) {
                    groupAdapter.items[index].latestMessage = getLatestMessageFromMessage(message)
                    groupAdapter.items[index].numberOfUnreadMessage = 0
                    groupAdapter.notifyItemChanged(index)
                }
            }

            Conversation.CONVERSATION_TYPE_ONE_ON_ONE -> {
                val index = memberAdapter.items.indexOfFirst { it.conversiontion?.id == conversation.id }
                if (index != -1) {
                    memberAdapter.items[index].latestMessage = getLatestMessageFromMessage(message)
                    memberAdapter.items[index].numberOfUnreadMessage = 0
                    memberAdapter.notifyItemChanged(index)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            requestCode -> {
                ConversationActivity.onActivityResult(resultCode, data, object: OnConversationActivityResult{
                    override fun onReadMessage(message: Message?, conversation: Conversation?) {
                        isNeedToRefresh = false
                        handleReadMessage(message, conversation)
                    }
                })
            }
        }
    }

    override fun onDestroy() {
        callConversation?.cancel()
        callCreateAnnouncement?.cancel()
        getAnnouncement?.cancel()
        callRemoveAnnouncement?.cancel()
        super.onDestroy()
    }

    private class AnnouncementAdapter(val deleteCompletionBlock: (Announcement?) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<AnnouncementViewHolder>() {
        val items = java.util.ArrayList<Announcement>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnnouncementViewHolder {
            val viewHolder = AnnouncementViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list_announcement, parent, false), deleteCompletionBlock)

            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: AnnouncementViewHolder, position: Int) {
            holder.bindData(items[position])
        }

        fun addAll(entities: java.util.ArrayList<Announcement>?) {
            entities?.let {
                items.clear()
                items.addAll(it)
            }
            notifyDataSetChanged()
        }
    }

    private class AnnouncementViewHolder(itemView: View, val deleteCompletionBlock: (Announcement?) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        private var announcement: Announcement? = null

        init {

        }

        fun bindData(item: Announcement) {
            announcement = item

            itemView.messageTextView.text = item.message

            itemView.removeImageView.visibility = if (TourConnex.getInstance().getActiveTrip()?.isTourLeader() == true) View.VISIBLE else View.GONE
            itemView.removeImageView.setOnClickListener {
                deleteCompletionBlock(item)
            }
        }

    }

    private class ConversationAdapter(val completionBlock: (ConversationItem?) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<ConversationViewHolder>() {
        val items = ArrayList<ConversationItem>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConversationViewHolder {
            val viewHolder = ConversationViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list_conversation, parent, false), completionBlock)

            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: ConversationViewHolder, position: Int) {
            holder.bindData(items[position])
        }

        fun addAll(entities: ArrayList<ConversationItem>?) {
            entities?.let {
                items.clear()
                items.addAll(it)
            }
            notifyDataSetChanged()
        }
    }

    private class ConversationViewHolder(itemView: View, completionBlock: ((ConversationItem?) -> Unit)) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        private var conversation: ConversationItem? = null
        private var callMessages: Call<PageMessage>? = null

        init {
            itemView.setOnClickListener { _ ->
                conversation?.let {
                    completionBlock(it)
                }
            }
        }

        fun bindData(item: ConversationItem) {
            conversation = item
            itemView.conversationNameTextView.text = item.displayname
            Glide.with(itemView.context).load(item.displayImageUrl).into(itemView.conversationRoomProfileImageView)
            itemView.numberOfUnreadTextContainerView.visibility = if (item.numberOfUnreadMessage == 0) View.INVISIBLE else View.VISIBLE
            itemView.latestReplyTextView.text = item.latestMessage
            if (!item.isFetchtedLatestMessage) {
                itemView.latestReplyTextView.text = null
                getLatestMessage(item.conversiontion?.id)
            }

        }

        fun getLatestMessage(conversationId: String?) {
            fetchMessagesWithNoHandle(itemView.context, conversationId) { pageMassage, error ->
                conversation?.isFetchtedLatestMessage = true
                pageMassage?.entities?.let {
                    if (conversationId == conversation?.conversiontion?.id) {
                        val message = it.firstOrNull()
                        conversation?.latestMessage = getLatestMessageFromMessage(message)
                        val latestSeenMessageId = TourConnex.getInstance().getLatestSeenMessageId(conversationId)
                        var unreadMessageCount = it.indexOfFirst { it.id == latestSeenMessageId}

                        itemView.numberOfUnreadTextContainerView.visibility = View.INVISIBLE
                        latestSeenMessageId?.let {
                            if (unreadMessageCount > 0) {
                                itemView.numberOfUnreadTextContainerView.visibility = View.VISIBLE
                                itemView.numberOfUnreadTextView.text = unreadMessageCount.toString()
                            }
                        } ?: run {
                            unreadMessageCount = it.count()
                            if (unreadMessageCount > 0) {
                                itemView.numberOfUnreadTextContainerView.visibility = View.VISIBLE
                                itemView.numberOfUnreadTextView.text = unreadMessageCount.toString()
                            }
                        }

                        conversation?.numberOfUnreadMessage = unreadMessageCount
                        itemView.latestReplyTextView.text = conversation?.latestMessage
                    }
                }
            }
        }

        private fun getLatestMessageFromMessage(message: Message?): String? {
            var latestMessage = message?.message
            when (message?.type?.toInt()) {
                MessageType.IMAGE -> {
                    latestMessage = itemView.context.getString(R.string.message_imageg_file)
                }
                MessageType.VOICE -> {
                    latestMessage = itemView.context.getString(R.string.message_audio_file)
                }
                MessageType.VIDEO -> {
                    latestMessage = itemView.context.getString(R.string.message_video_file)
                }
            }

            return latestMessage
        }

        private fun fetchMessagesWithNoHandle(context: Context, conversationId: String?, completionBlock: ((PageMessage?, Error?) -> Unit)) {
            callMessages = getApiClient().getConversationMessages(conversationId,size = 100)
            callMessages?.enqueue(object : MainThreadCallback<PageMessage>(context) {
                override fun onSuccess(result: PageMessage?) {
                    completionBlock(result, null)
                }

                override fun onError(response: Response<PageMessage>?, error: Error?) {
                    completionBlock(null, error)
                }
            })
        }

        private fun getApiClient(): ApiClient {
            return ServiceLocator.getInstance().getService(ApiClient::class.java)
        }
    }
}


