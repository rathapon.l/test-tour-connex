package com.tourconnex.android

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.location.Location
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.trip.SOS
import com.tourconnex.android.domain.trip.TripTraveller
import com.tourconnex.android.domain.trip.form.SendSOSForm
import com.tourconnex.android.domain.users.PageGuide
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.framework.utils.LocationHelper
import com.tourconnex.android.framework.utils.adapter.ActivityLocationHelperAdapter
import com.tourconnex.android.main.MyTripFragment
import com.tourconnex.android.main.ChatsFragment
import com.tourconnex.android.main.MoreFragment
import com.tourconnex.android.map.TourMapFragment
import com.tourconnex.android.sos.GuideSOSActivity
import com.tourconnex.android.sos.UserSOSActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.view_toolbar_main.*
import retrofit2.Call
import retrofit2.Response

class MainActivity : AbstractActivity() {

    companion object {
        private const val REQUEST_LOCATION  = 12337
        private const val REQUESR_SOS       = 12338

        fun create(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    private var locationHelper: LocationHelper? = null
    private var callSOS: Call<SOS>? = null
    private var callMySOS: Call<SOS>? = null
    private var callTrip: Call<TripTraveller>? = null
    private var sosMenu: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            handleIntent(intent)
        }

        setSupportActionBar(actionBarView)

        supportActionBar?.title = getString(R.string.home_menu_trip)

        val adapter = HomeFragmentAdapter(supportFragmentManager)
        viewPager.offscreenPageLimit = adapter.count
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(onViewPagerChangeListener)

        val colorList = ColorStateList(
            arrayOf(
                intArrayOf(-android.R.attr.state_selected),  // Disabled
                intArrayOf(android.R.attr.state_selected)    // Enabled
            ),
            intArrayOf(
                ContextCompat.getColor(this, R.color.textGrey),     // The color for the Disabled state
                ContextCompat.getColor(this, R.color.colorNavyLight)        // The color for the Enabled state
            )
        )

        bottomNavigation.itemIconTintList = null
        bottomNavigation.itemTextColor = colorList
        bottomNavigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        val helperAdapter = ActivityLocationHelperAdapter(this)
        locationHelper = LocationHelper(helperAdapter)
        sosMenu = bottomNavigation.menu.findItem(R.id.item_sos)
        sosMenu?.isVisible = false

        fetchMySOSIfNeeded()
    }

    override fun onStart() {
        super.onStart()

        fetchTripIfNeeded()
    }

    fun gotoTabId(id: Int) {
        bottomNavigation.selectedItemId = id
    }

    private fun fetchMySOSIfNeeded() {
        val activeTrip = TourConnex.getInstance().getActiveTrip()
        if (activeTrip == null) {
            return
        }

        callMySOS?.cancel()
        callMySOS = getApiClient().getMySOS(activeTrip?.trip?.id)
        callMySOS?.enqueue(object: MainThreadCallback<SOS>(this) {
            override fun onSuccess(result: SOS?) {
                if (result != null) {
                    openUserSOSActivity(result)
                }
            }
        })
    }

    private fun fetchTripIfNeeded() {
        val activeTrip = TourConnex.getInstance().getActiveTrip()
        if (activeTrip == null) {
            return
        }

        callTrip?.cancel()
        callTrip = getApiClient().getTrip(activeTrip.id ?: "")
        callTrip?.enqueue(object: MainThreadCallback<TripTraveller>(this) {
            override fun onSuccess(result: TripTraveller?) {
                updateSOSButton(result)
            }

            override fun onError(response: Response<TripTraveller>?, error: Error?) {

            }
        })
    }

    private fun updateSOSButton(trip: TripTraveller?) {
        val isGuide = TourConnex.getInstance().getUser()?.isGuide
        if (isGuide == true) {
            sosMenu?.isVisible = true
        } else {
            sosMenu?.isVisible = (trip?.trip?.sos == true && trip?.checkedIn == true)
        }
    }

    private fun sendSOSConfirmation() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(getString(R.string.home_sos_confirmation))
        builder.setPositiveButton(R.string.common_ok) { dialog, _ ->
            requestLocation()
            dialog.dismiss()
        }

        builder.setNegativeButton(R.string.common_cancel) { dialog, _ ->
            dialog.dismiss()
        }

        builder.show()
    }

    private fun requestLocation() {
        locationHelper?.requestLocation({ location, b ->
            location?.let {
                performSOS(it)
            }
        }, REQUEST_LOCATION)
    }

    private fun openUserSOSActivity(sos: SOS?) {
        startActivityForResult(UserSOSActivity.create(this, sos), REQUESR_SOS)
    }

    private fun performSOS(location: Location) {
        val tourConnex = TourConnex.getInstance()
        val activeTrip = tourConnex.getActiveTrip()

        val form = SendSOSForm(location.latitude, location.longitude)

        callSOS?.cancel()
        callSOS = getApiClient().sendSOS(activeTrip?.trip?.id, form)
        callSOS?.enqueue(object : MainThreadCallback<SOS>(this, simpleProgressBar) {
            override fun onSuccess(result: SOS?) {
                openUserSOSActivity(result)
            }

            override fun onError(response: Response<SOS>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    private val onNavigationItemSelectedListener = object : BottomNavigationView.OnNavigationItemSelectedListener {
        override fun onNavigationItemSelected(item: MenuItem): Boolean {
            var allow = false
            when (item.itemId) {
                R.id.item_trip -> {
                    viewPager.currentItem = 0
                    allow = true
                }
                R.id.item_chat -> {
                    viewPager.currentItem = 1
                    allow = true
                }
                R.id.item_sos -> {
                    val isGuide = TourConnex.getInstance().getUser()?.isGuide
                    if (isGuide == true) {
                        openSOSList()
                    } else {
                        sendSOSConfirmation()
                    }
                }
                R.id.item_map -> {
                    viewPager.currentItem = 3
                    allow = true
                }
                R.id.item_more -> {
                    viewPager.currentItem = 4
                    allow = true
                }
            }

            return allow
        }
    }

    private val onViewPagerChangeListener = object : ViewPager.SimpleOnPageChangeListener() {
        override fun onPageSelected(position: Int) {
            when (position) {
                0 -> {
                    supportActionBar?.title = getString(R.string.home_menu_trip)
                    fetchTripIfNeeded()
                }
                1 -> {
                    supportActionBar?.title = getString(R.string.home_menu_chats)
                }
                3 -> {
                    supportActionBar?.title = getString(R.string.home_menu_map)
                }
                4 -> {
                    supportActionBar?.title = getString(R.string.home_menu_more)
                }
            }
        }
    }

    private class HomeFragmentAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> MyTripFragment()
                1 -> ChatsFragment()
                2 -> Fragment()
                3 -> TourMapFragment()
                4 -> MoreFragment()
                else -> throw IllegalArgumentException("Fragment size wrong.")
            }
        }

        override fun getCount(): Int {
            return 5
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationHelper?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onDestroy() {
        callSOS?.cancel()
        locationHelper?.cancel()
        callMySOS?.cancel()
        super.onDestroy()
    }
}
