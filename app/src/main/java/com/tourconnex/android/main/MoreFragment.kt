package com.tourconnex.android.main

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.fragment.AbstractFragment
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.guest.WelcomeActivity
import kotlinx.android.synthetic.main.fragment_more.*
import android.view.ViewGroup
import android.widget.CompoundButton
import com.tourconnex.android.MainActivity
import com.tourconnex.android.framework.manager.ExchangeRateManager
import com.tourconnex.android.guest.LanguageSettingActivity
import com.tourconnex.android.more.CurrencyConverterActivity
import com.tourconnex.android.trip.MyTripsActivity
import com.tourconnex.android.user.EditProfileActivity

class MoreFragment : AbstractFragment() {

    companion object {
        private const val REQUEST_EDIT_PROFILE          = 1230
        private const val REQUEST_CHANGE_LANGUAGE       = 1231
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_more, container, false)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        //actionBarView.title = getString(R.string.home_menu_more)

        logoutView.setOnClickListener {
            context?.let {
                showLogoutConfirmation(it)
            }
        }

        converterView.setOnClickListener { openCurrencyConverter() }
        myTripsView.setOnClickListener { openMyTripsActivity() }
        languagesView.setOnClickListener { openLanguageSettingActivity() }
        locationEnableSwitch.setOnCheckedChangeListener(enableLocationListener)
        notificationEnableSwitch.setOnCheckedChangeListener(enableNotificationListener)

        // editTextView.visibility = if (TourConnex.getInstance().getUser()?.isGuide == true) View.VISIBLE else View.GONE
        editTextView.setOnClickListener { openEditGuide() }
        feedbackView.setOnClickListener { sendFeedback() }

        updateUI()
    }

    private fun updateUI() {
        val user = TourConnex.getInstance().getUser()
        val firstName = user?.firstName ?: ""
        val lastName = user?.lastName ?: ""

        Glide.with(this).load(user?.profilePicture?.largeImageUrl).into(coverImageView)
        Glide.with(this).load(user?.profilePicture?.largeImageUrl).into(profileImageView)

        usernameTextView.text = "$firstName $lastName"

        versionTextView.text = TourConnex.getVersionName(activity)

        setEnableLocation(TourConnex.getInstance().getEnableLocation())
        setEnableNotitifation(TourConnex.getInstance().getEnableNotification())
    }

    private fun setEnableLocation(enable: Boolean) {
        locationEnableSwitch.setOnCheckedChangeListener(null)
        locationEnableSwitch.isChecked = enable
        locationEnableSwitch.setOnCheckedChangeListener(enableLocationListener)
    }

    private fun setEnableNotitifation(enable: Boolean) {
        notificationEnableSwitch.setOnCheckedChangeListener(null)
        notificationEnableSwitch.isChecked = enable
        notificationEnableSwitch.setOnCheckedChangeListener(enableNotificationListener)
    }

    private fun showLogoutConfirmation(context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(R.string.more_logout_message)
        builder.setPositiveButton(R.string.common_ok) { dialog, _ ->
            dialog.dismiss()
            logout()
        }
        builder.setNegativeButton(R.string.common_cancel) { dialog, _ -> dialog.cancel() }
        builder.show()
    }

    private fun sendFeedback() {
        val emailIntent = Intent(Intent.ACTION_SENDTO)
        val email = getString(R.string.feedback_email)
        emailIntent.data = Uri.parse("mailto:$email")
        startActivity(Intent.createChooser(emailIntent, getString(R.string.more_feedback)))
    }

    private fun openMyTripsActivity() {
        activity?.let {
            startActivity(MyTripsActivity.create(it))
        }
    }

    private fun openLanguageSettingActivity() {
        activity?.let { startActivityForResult(LanguageSettingActivity.create(it), REQUEST_CHANGE_LANGUAGE) }
    }

    private fun openEditGuide() {
        activity?.let { startActivityForResult(EditProfileActivity.create(it), REQUEST_EDIT_PROFILE) }
    }

    private fun openCurrencyConverter() {
        context?.let {
            ExchangeRateManager.getInstance().getExchagngeRates(it) { exchangeRates, lastUpdate, powerBy, error ->
                if (exchangeRates != null) {
                    startActivity(CurrencyConverterActivity.create(it, exchangeRates, lastUpdate, powerBy))
                } else {
                    showErrorDialog(error)
                }
            }
        }
    }

    private var enableLocationListener = object : CompoundButton.OnCheckedChangeListener {
        override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
            TourConnex.getInstance().setEnableLocation(isChecked)
        }
    }

    private var enableNotificationListener = object : CompoundButton.OnCheckedChangeListener {
        override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
            TourConnex.getInstance().setEnableNotification(isChecked)
        }
    }

    private fun logout() {
        TourConnex.getInstance().logout()
        val abstractActivity = activity as? AbstractActivity
        abstractActivity?.let {
            startActivity(WelcomeActivity.create(it))
            it.finishWithoutAnimation()
        }
    }

    private fun didChangeLanguage() {
        val abstractActivity = activity as? AbstractActivity
        abstractActivity?.let {
            startActivity(MainActivity.create(it))
            it.finishWithoutAnimation()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_EDIT_PROFILE -> {
                if (resultCode == Activity.RESULT_OK) {
                    //showMessageDialog(getString(R.string.profile_edit_success))
                    updateUI()
                }
            }

            REQUEST_CHANGE_LANGUAGE -> {
                if (resultCode == Activity.RESULT_OK) {
                    didChangeLanguage()
                }
            }
        }
    }
}
