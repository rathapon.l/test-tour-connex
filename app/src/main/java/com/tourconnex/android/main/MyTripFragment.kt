package com.tourconnex.android.main
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.*
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.fragment.AbstractFragment
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.common.form.UpdateUserTripLocationForm
import com.tourconnex.android.domain.trip.AssignGuideEvent
import com.tourconnex.android.domain.trip.AssignTravellerEvent
import com.tourconnex.android.domain.trip.TripTraveller
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.framework.utils.ImageUtils
import com.tourconnex.android.framework.utils.LocationHelper
import com.tourconnex.android.framework.utils.adapter.FragmentLocationHelperAdapter
import com.tourconnex.android.trip.*
import com.tourconnex.android.user.AddTripActivity
import com.tourconnex.android.user.MyQRCodeActivity
import com.tourconnex.android.user.OnAddTripActivityResult
import com.tourconnex.android.user.OnMyQRCodeActivityResult
import kotlinx.android.synthetic.main.fragment_my_trip.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Call
import retrofit2.Response

class MyTripFragment : AbstractFragment() {

    companion object {
        private const val REQUEST_TRIPS = 10035
        private const val REQUEST_LOCATION = 12336


        private const val REQUEST_MY_QRCODE = 13446
        private const val REQUEST_ADD_TRIP = 13448
    }

    private var activeTrip: TripTraveller? = null
    private var updateLocationCall: Call<SimpleResponse>? = null
    private var locationHelper: LocationHelper? = null
    private var callTrip: Call<TripTraveller>? = null

    private var myQRCodeMenu: MenuItem? = null

    private var checkedIn = false
    private var gettingCheckinTrip = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_my_trip, container, false)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        qrCodeImageView.setImageBitmap(ImageUtils.genQRFromFreeText(TourConnex.getInstance().getUser()?.id, 400, 400))

        val adapter = FragmentLocationHelperAdapter(this)
        locationHelper = LocationHelper(adapter)

        myTripsButton.setOnClickListener { openMyTrips() }
        addTripButton.setOnClickListener { openAddTrip() }
    }

    override fun onResume() {
        super.onResume()

        if (!gettingCheckinTrip) {
            checkUserRole()
            updateTripIfNeeded()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun didReciveAssignTravellerEvent(s: AssignTravellerEvent) {
        gettingCheckinTrip = true
        getTripById(s.contentId)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun didReciveAssignGuideEvent(s: AssignGuideEvent) {
        checkUserRole()
    }

    override fun onStart() {
        EventBus.getDefault().register(this)
        super.onStart()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    private fun checkUserRole() {
        val user = TourConnex.getInstance().getUser()
        if (user?.isGuide == true) {
            handleGuide()
        } else {
            handleTraveller()
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        if (isVisibleToUser) {
            updateTripLocationIfNeeded()
        }
    }

    private fun updateTripLocationIfNeeded() {
        if (activeTrip == null || !TourConnex.getInstance().getEnableLocation()) {
            return
        }

        requestLocation()
    }

    private fun requestLocation() {
        locationHelper?.cancel()
        locationHelper?.requestLocation( { location, b ->
            location?.let {
                updateLocationWithLocation(it)
            }
        }, REQUEST_LOCATION)
    }

    private fun updateLocationWithLocation(location: Location) {
        val form = UpdateUserTripLocationForm(location.latitude, location.longitude)
        updateLocationCall?.cancel()
        updateLocationCall = getApiClient().updateUserTripLocation(activeTrip?.id, form)
        updateLocationCall?.enqueue(object : MainThreadCallback<SimpleResponse>(activity) {
            override fun onSuccess(result: SimpleResponse?) {

            }

            override fun onError(response: Response<SimpleResponse>?, error: Error?) {

            }
        })
    }

    private fun hideAllNoActiveTripView() {
        qrCodeContainerView.visibility = View.GONE
        addTripContainerView.visibility = View.GONE
        tripContainerView.visibility = View.GONE
    }

    private fun getCurrentActiveTour(): TripTraveller? {
        val tourConnex = TourConnex.getInstance()
        return tourConnex.getActiveTrip()
    }

    private fun updateTripIfNeeded() {
        if(activeTrip == null) {
            return
        }

        if (activeTrip?.isPreview == true) {
            updatePreviewTrip()
        } else {
            updateActiveTrip()
        }
    }

    private fun updateActiveTrip() {
        callTrip?.cancel()
        callTrip = getApiClient().getTrip(activeTrip?.id ?: "")
        callTrip?.enqueue(object: MainThreadCallback<TripTraveller>(activity) {
            override fun onSuccess(result: TripTraveller?) {
                this@MyTripFragment.activeTrip = result
                checkedIn = result?.checkedIn ?: false
                TourConnex.getInstance().setActiveTrip(result)
                updateMenuItemUI()
            }

            override fun onError(response: Response<TripTraveller>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    private fun updatePreviewTrip() {
        callTrip?.cancel()
        callTrip = getApiClient().getTripPreview(activeTrip?.id ?: "")
        callTrip?.enqueue(object: MainThreadCallback<TripTraveller>(activity) {
            override fun onSuccess(result: TripTraveller?) {
                this@MyTripFragment.activeTrip = result
                checkedIn = result?.checkedIn ?: false
                TourConnex.getInstance().setActiveTrip(result)
                updateMenuItemUI()
            }

            override fun onError(response: Response<TripTraveller>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    private fun getTripById(tripId: String?) {
        callTrip?.cancel()
        callTrip = getApiClient().getTrip(tripId ?: "")
        callTrip?.enqueue(object: MainThreadCallback<TripTraveller>(activity) {
            override fun onSuccess(result: TripTraveller?) {
                activeTrip = null
                TourConnex.getInstance().setActiveTrip(result)
                checkUserRole()
                gettingCheckinTrip = false
            }

            override fun onError(response: Response<TripTraveller>?, error: Error?) {
                showErrorDialog(error)
                gettingCheckinTrip = false
            }
        })
    }

    private fun handleTraveller() {
        hideAllNoActiveTripView()
        val currentActiveTour = getCurrentActiveTour()
        if (currentActiveTour != null) {
            setTrip(currentActiveTour)
        } else {
            activeTrip = null
            //qrCodeContainerView.visibility = View.VISIBLE
            addTripContainerView.visibility = View.VISIBLE
            addTripButton.visibility = View.VISIBLE
            updateMenuItemUI()
        }
    }

    private fun handleGuide() {
        hideAllNoActiveTripView()
        val currentActiveTour = getCurrentActiveTour()
        if (currentActiveTour != null) {
            setTrip(currentActiveTour)
        } else {
            activeTrip = null
            addTripContainerView.visibility = View.VISIBLE
            addTripButton.visibility = View.VISIBLE
            updateMenuItemUI()
        }
    }

    private fun setTrip(trip: TripTraveller?) {
        tripContainerView.visibility = if (trip == null) View.GONE else View.VISIBLE
        if (trip != activeTrip) {
            this.activeTrip = trip
            checkedIn = trip?.checkedIn ?: false
            updateTripUI()
            updateMenuItemUI()
        }
    }

    private fun updateMenuItemUI() {
        //myQRCodeMenu?.isVisible = (TourConnex.getInstance().getUser()?.isGuide == false)
    }

    private fun openAddTrip() {
        activity?.let { startActivityForResult(AddTripActivity.create(it), REQUEST_ADD_TRIP) }
    }

    private fun updateTripUI() {

        removeFragmentIfNeeded(ActiveTripFragment.FRAGMENT_TAG)
        val tripFragment = ActiveTripFragment.create(activeTrip)
        tripFragment.onTripFragmentInactiveTripCompletionBlock = {
            activeTrip = null
            checkUserRole()
        }

        addFragment(R.id.tripContainerView, tripFragment, ActiveTripFragment.FRAGMENT_TAG)
        updateTripLocationIfNeeded()
    }

    private fun openMyTrips() {
        activity?.let { startActivityForResult(MyTripsActivity.create(it), REQUEST_TRIPS) }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.common_my_trips, menu)

        myQRCodeMenu = menu.findItem(R.id.actionQrCode)

        updateMenuItemUI()

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionQrCode -> {
                openMyQrCode()
                return true
            }
            R.id.actionMyTrips -> {
                openMyTrips()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun openMyQrCode() {
        val user = TourConnex.getInstance().getUser()
        activity?.let { startActivityForResult(MyQRCodeActivity.create(it, user), REQUEST_MY_QRCODE) }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationHelper?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_MY_QRCODE -> {
                MyQRCodeActivity.onActivityResult(resultCode, data, object : OnMyQRCodeActivityResult {
                    override fun onAddedTripTraveller(tripTraveller: TripTraveller?) {
                        TourConnex.getInstance().setActiveTrip(tripTraveller)
                        checkUserRole()
                    }

                    override fun onCheckedIn(travellerId: String?) {
                        gettingCheckinTrip = true
                        getTripById(travellerId)
                    }

                    override fun onCancel() {

                    }
                })
            }

            REQUEST_ADD_TRIP -> {
                AddTripActivity.onActivityResult(resultCode, data, object: OnAddTripActivityResult {
                    override fun onAddedTrip(traveller: TripTraveller?) {
                        TourConnex.getInstance().setActiveTrip(traveller)
                        checkUserRole()
                    }

                    override fun onCancel() {

                    }
                })
            }
        }
    }

    override fun onDestroy() {
        updateLocationCall?.cancel()
        locationHelper?.cancel()
        callTrip?.cancel()
        super.onDestroy()
    }
}
