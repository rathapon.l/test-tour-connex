package com.tourconnex.android.map

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.tourconnex.android.R
import com.tourconnex.android.domain.common.Hotel

class HotelLocationActivity : LocationActivity() {

    companion object {
        private const val EXTRA_HOTEL = "EXTRA_HOTEL"
        fun create(context: Context, hotel: Hotel?): Intent {

            return Intent(context, TravellerLocationActivity::class.java).apply {
                putExtra(EXTRA_HOTEL, hotel)
            }
        }
    }

    private var hotel: Hotel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        hotel = intent?.getParcelableExtra(EXTRA_HOTEL)
        location = hotel?.location
        super.onCreate(savedInstanceState)

        supportActionBar?.title = hotel?.name

    }

    override fun addMarker(latLng: LatLng, title: String?, any: Any) {
        val markerOptions = MarkerOptions()
        markerOptions.position(latLng)
        markerOptions.title(hotel?.address)

        val marker = googleMap?.addMarker(markerOptions)
        marker?.tag = any
    }

}
