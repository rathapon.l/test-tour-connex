package com.tourconnex.android.map

import android.os.Bundle
import android.util.Log
import androidx.core.content.res.ResourcesCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Location
import com.tourconnex.android.domain.users.Traveller
import com.tourconnex.android.framework.utils.LocationHelper
import com.tourconnex.android.framework.utils.adapter.ActivityLocationHelperAdapter
import kotlinx.android.synthetic.main.view_toolbar.*

open class LocationActivity : AbstractActivity(), OnMapReadyCallback {

    private var mapFragment: SupportMapFragment? = null

    protected var googleMap: GoogleMap? = null
    private var locationHelper: LocationHelper? = null

    protected var traveller: Traveller? = null
    protected var location: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.location_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mapFragment = supportFragmentManager.findFragmentById(R.id.mapView) as? SupportMapFragment
        mapFragment?.getMapAsync(this)

        val adapter = ActivityLocationHelperAdapter(this)
        locationHelper = LocationHelper(adapter)



    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
        val latLng = LatLng(location?.latitude ?: 0.0, location?.longitude  ?: 0.0)

        val zoomLevel = 16.0f //This goes up to 21
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel))

        try {
            // Request location updates
            googleMap?.isMyLocationEnabled = true
        } catch(ex: SecurityException) {
            Log.d("myTag", "Security Exception, no location available")
        }

        traveller?.let {
            addMakerTravellerToMap(it, latLng)
        }
    }

    private fun addMakerTravellerToMap(traveller: Traveller, latLng: LatLng) {
        val lat = traveller.location?.latitude ?: 0.0
        val lng = traveller.location?.longitude ?: 0.0
        addMarker(LatLng(lat, lng), traveller.user?.getFullName(), traveller)
    }

    open fun addMarker(latLng: LatLng, title: String?, any: Any) {

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationHelper?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onDestroy() {
        locationHelper?.cancel()
        super.onDestroy()
    }
}
