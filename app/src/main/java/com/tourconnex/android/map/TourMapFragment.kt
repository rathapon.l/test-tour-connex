package com.tourconnex.android.map


import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.tourconnex.android.R
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Marker
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.fragment.AbstractFragment
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.trip.TripTraveller
import com.tourconnex.android.domain.users.Guide
import com.tourconnex.android.domain.users.PageGuide
import com.tourconnex.android.domain.users.PageTraveller
import com.tourconnex.android.domain.users.Traveller
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.framework.utils.LocationHelper
import retrofit2.Call
import retrofit2.Response
import android.graphics.Bitmap
import com.google.android.gms.maps.model.BitmapDescriptor
import android.graphics.drawable.BitmapDrawable
import android.os.Handler
import androidx.core.content.res.ResourcesCompat
import com.tourconnex.android.framework.utils.adapter.FragmentLocationHelperAdapter
import com.tourconnex.android.user.GuideProfileActivity
import com.tourconnex.android.user.TravellerProfileActivity
import kotlinx.android.synthetic.main.fragment_tour_map.*


class TourMapFragment : AbstractFragment(), OnMapReadyCallback {

    companion object {
        private const val TYPE_TRAVELLER = 0
        private const val TYPE_GUIDE = 1

        private const val REQUEST_LOCATION = 12331
    }

    private var mapFragment: SupportMapFragment? = null

    private var googleMap: GoogleMap? = null
    private var currentLocation: Location? = null
    private var locationHelper: LocationHelper? = null

    private var activeTrip: TripTraveller? = null

    private var callTripGuide: Call<PageGuide>? = null
    private var callTravellers: Call<PageTraveller>? = null

    private var markers = ArrayList<Marker>()

    var h = Handler()
    var delay: Long = 10 * 1000 //1 second=1000 milisecond, 10*1000=10seconds
    var runnable: Runnable? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tour_map, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mapFragment = childFragmentManager.findFragmentById(R.id.mapView) as? SupportMapFragment
        mapFragment?.getMapAsync(this)

        val adapter = FragmentLocationHelperAdapter(this)
        locationHelper = LocationHelper(adapter)

        updateActiveTripIfNeeded()
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)

        if (isVisibleToUser) {
            updateActiveTripIfNeeded()
            requestLocation()
            startAutoLoad()
        } else {
            if (runnable != null) {
                h.removeCallbacks(runnable)
            }
        }
    }

    private fun updateActiveTripIfNeeded() {
        val tourConnex = TourConnex.getInstance()
        activeTrip = tourConnex.getActiveTrip()

        noAvailableView.visibility = if (activeTrip?.checkedIn == true || TourConnex.getInstance().getUser()?.isGuide == true) View.GONE else View.VISIBLE
    }

    private fun requestLocation() {
        locationHelper?.cancel()
        locationHelper?.requestLocation({ location, isUserAllow ->
            if (isUserAllow) {
                this.currentLocation = location
                reachCurrentLocation()
            } else {

            }
        }, REQUEST_LOCATION)
    }

    private fun reachCurrentLocation() {
        val latLng = LatLng(currentLocation?.latitude ?: 0.0, currentLocation?.longitude  ?: 0.0)

        val zoomLevel = 16.0f //This goes up to 21
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel))

        try {
            // Request location updates
            googleMap?.isMyLocationEnabled = true
        } catch(ex: SecurityException) {
            Log.d("myTag", "Security Exception, no location available")
        }

        fetchAllGuidesAndTrveller()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
        googleMap?.setOnInfoWindowClickListener { didSelectMarker(it) }
        reachCurrentLocation()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        locationHelper?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun fetchAllGuidesAndTrveller() {
        googleMap?.clear()
        markers.clear()

        fetchGuides()
        if (TourConnex.getInstance().getUser()?.isGuide == true) {
            fetchTravellers()
        }
    }

    private fun startAutoLoad() {
        if (googleMap == null || activeTrip == null) {
            return
        }

        runnable = Runnable {
            fetchAllGuidesAndTrveller()
            startAutoLoad()
        }

        runnable?.let {
            h.postDelayed(it, delay)
        }
    }

    private fun fetchGuides() {
        callTripGuide?.cancel()
        callTripGuide = getApiClient().getTripGuides(activeTrip?.trip?.id ?: "")

        callTripGuide?.enqueue(object: MainThreadCallback<PageGuide>(activity) {
            override fun onSuccess(result: PageGuide?) {
                result?.entities?.let { guides ->
                    val filteredGuides = guides.filter { it.location != null }
                    filteredGuides.forEach { addMarkerGuideToMap(it) }
                }
            }

            override fun onError(response: Response<PageGuide>?, error: Error?) {
            }
        })
    }

    private fun fetchTravellers() {
        callTravellers?.cancel()
        callTravellers = getApiClient().getTripTravellers(activeTrip?.trip?.id ?: "")
        callTravellers?.enqueue(object : MainThreadCallback<PageTraveller>(activity) {
            override fun onSuccess(result: PageTraveller?) {
                result?.entities?.let { travellers ->
                    val filteredTravellers = travellers.filter { it.location != null }
                    filteredTravellers.forEach { addMakerTravellerToMap(it) }
                }
            }

            override fun onError(response: Response<PageTraveller>?, error: Error?) {

            }
        })
    }

    private fun addMarkerGuideToMap(guide: Traveller) {
        val lat = guide.location?.latitude ?: 0.0
        val lng = guide.location?.longitude ?: 0.0
        addMarker(LatLng(lat, lng), guide.user?.getFullName(), guide, TYPE_GUIDE)
    }

    private fun addMakerTravellerToMap(traveller: Traveller) {
        val lat = traveller.location?.latitude ?: 0.0
        val lng = traveller.location?.longitude ?: 0.0
        addMarker(LatLng(lat, lng), traveller.user?.getFullName(), traveller, TYPE_TRAVELLER)
    }

    private fun addMarker(latLng: LatLng, title: String?, any: Any, type: Int) {
        val markerOptions = MarkerOptions()
        markerOptions.position(latLng)
        markerOptions.title(title)

        markerOptions.icon( getMarkerIconFromResource( if (type == TYPE_TRAVELLER) R.drawable.icon_user_pin else R.drawable.icon_guide_pin))

        val marker = googleMap?.addMarker(markerOptions)
        marker?.tag = any
    }

    private fun getMarkerIconFromResource(resource :Int): BitmapDescriptor {
        val drawable = ResourcesCompat.getDrawable(resources, resource, null) as? BitmapDrawable

        val bitmap = drawable?.bitmap

        val widthInPixels = resources.getDimensionPixelSize(R.dimen.pinWidth)
        val heightInPixels = resources.getDimensionPixelSize(R.dimen.pinHeight)
        return BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(bitmap, widthInPixels, heightInPixels, false))
    }

    private fun didSelectMarker(marker: Marker) {
        val tag = marker.tag
        if (tag is Traveller) {
            val traveller = tag as? Traveller
            traveller?.let {
                didSelectTraveller(it)
            }
        } else if (tag is Guide) {
            val guide = tag as? Traveller
            guide?.let {
                didSelectGuide(it)
            }
        }
    }

    private fun didSelectTraveller(traveller: Traveller?) {
        activity?.let { startActivity(TravellerProfileActivity.create(it, traveller, traveller?.id, activeTrip?.trip?.id, null)) }

    }

    private fun didSelectGuide(user: Traveller) {
        activity?.let { startActivity(GuideProfileActivity.create(it, user, activeTrip?.trip?.id)) }
    }

    override fun onDestroy() {
        locationHelper?.cancel()
        super.onDestroy()
    }

}
