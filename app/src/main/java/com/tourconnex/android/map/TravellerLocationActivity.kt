package com.tourconnex.android.map

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import androidx.core.content.res.ResourcesCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Location
import com.tourconnex.android.domain.users.Traveller
import com.tourconnex.android.framework.utils.LocationHelper
import com.tourconnex.android.framework.utils.adapter.ActivityLocationHelperAdapter
import kotlinx.android.synthetic.main.view_toolbar.*

class TravellerLocationActivity : LocationActivity() {
    companion object {
        private const val EXTRA_TRAVELLER = "EXTRA_TRAVELLER"
        private const val EXTRA_LOCATION = "EXTRA_LOCATION"
        fun create(context: Context, traveller: Traveller?, location: Location?): Intent {

            return Intent(context, TravellerLocationActivity::class.java).apply {
                putExtra(EXTRA_TRAVELLER, traveller)
                putExtra(EXTRA_LOCATION, location)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        traveller = intent?.getParcelableExtra(EXTRA_TRAVELLER)
        location = intent?.getParcelableExtra(EXTRA_LOCATION)
        super.onCreate(savedInstanceState)
    }


    override fun addMarker(latLng: LatLng, title: String?, any: Any) {
        val markerOptions = MarkerOptions()
        markerOptions.position(latLng)
        markerOptions.title(title)

        markerOptions.icon( getMarkerIconFromResource(R.drawable.icon_user_pin))

        val marker = googleMap?.addMarker(markerOptions)
        marker?.tag = any
    }

    private fun getMarkerIconFromResource(resource :Int): BitmapDescriptor {
        val drawable = ResourcesCompat.getDrawable(resources, resource, null) as? BitmapDrawable

        val bitmap = drawable?.bitmap

        val widthInPixels = resources.getDimensionPixelSize(R.dimen.pinWidth)
        val heightInPixels = resources.getDimensionPixelSize(R.dimen.pinHeight)
        return BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(bitmap, widthInPixels, heightInPixels, false))
    }

}
