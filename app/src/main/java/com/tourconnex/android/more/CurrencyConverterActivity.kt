package com.tourconnex.android.more

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.TextView
import com.tourconnex.android.R
import com.google.gson.Gson
import com.tourconnex.android.domain.common.Currency
import com.google.gson.reflect.TypeToken
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.ExchangeRate
import com.tourconnex.android.framework.widget.AmountEditText
import com.tourconnex.android.framework.widget.AmountTextChangeListener
import kotlinx.android.synthetic.main.activity_currency_converter.*
import kotlinx.android.synthetic.main.view_toolbar.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CurrencyConverterActivity : AbstractActivity() {

    companion object {

        private const val EXTRA_EXCHANGE_RATES = "EXTRA_EXCHANGE_RATES"
        private const val EXTRA_LAST_UPDATE = "EXTRA_LAST_UPDATE"
        private const val EXTRA_POWER_BY = "EXTRA_POWER_BY"
        private const val FORMAT = "#.##"

        fun create(context: Context, exchangeRate: ArrayList<ExchangeRate>?, lastUpdate: Date?, powerBy: String?): Intent {
            return Intent(context, CurrencyConverterActivity::class.java).apply {
                putParcelableArrayListExtra(EXTRA_EXCHANGE_RATES, exchangeRate)
                putExtra(EXTRA_LAST_UPDATE, lastUpdate)
                putExtra(EXTRA_POWER_BY, powerBy)
            }
        }
    }
    private var currencies: List<Currency>? = null
    private var exchangeRates: ArrayList<ExchangeRate>? = null
    private var baseCurrency: Currency? = null
    private var resultCurrency: Currency? = null
    private var lastUpdate: Date? = null
    private var powerBy: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency_converter)

        exchangeRates = intent?.getParcelableArrayListExtra(EXTRA_EXCHANGE_RATES)
        lastUpdate = intent?.getSerializableExtra(EXTRA_LAST_UPDATE) as? Date
        powerBy = intent?.getStringExtra(EXTRA_POWER_BY)


        val tourConnex = TourConnex.getInstance()
        baseCurrency = tourConnex.getBaseCurrency()
        resultCurrency = tourConnex.getResultCurrency()

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.more_converter)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        preparingCurrencies()

        switchCurrencyTop.setOnClickListener { switchCurrency() }

        currencies?.let {
            val baseCurrencyDataAdapter = ArrayAdapter<Currency>(this, android.R.layout.simple_spinner_item, it)
            baseCurrencyDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            baseCurrencySpinner.adapter = baseCurrencyDataAdapter

            val resultCurrencyDataAdapter = ArrayAdapter<Currency>(this, android.R.layout.simple_spinner_item, it)
            resultCurrencyDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            resultCurrencySpinner.adapter = resultCurrencyDataAdapter
        }

        var date = "-"
        lastUpdate?.let {
            val format = SimpleDateFormat("d MMM yyyy hh:mm:ss")
            date = format.format(it)
        }
        powerByTextView.text = getString(R.string.converter_power_by, powerBy ?: "")
        val lastUpdateText =  getString(R.string.converter_update_at, date)
        lastUpdateTextView.text = lastUpdateText

        updateUI()

        baseCurrencySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (currencies == null) {
                    return
                }

                baseCurrency = currencies!![position]
                tourConnex.setBaseCurrency(currencies!![position])
                updateExchangeSymbolUI()
                calculateExchangeRates()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        resultCurrencySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (currencies == null) {
                    return
                }

                resultCurrency = currencies!![position]
                tourConnex.setResultCurrency(currencies!![position])
                updateExchangeSymbolUI()
                calculateExchangeRates()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        baseValueEditText.setText("")
        baseValueEditText.addTextChangedListener(textChangeListener)
    }

    private fun updateUI() {
        val selectedBaseCurrencyIndex = currencies?.indexOfFirst { it.code == baseCurrency?.code } ?: -1
        if (selectedBaseCurrencyIndex != -1) {
            baseCurrencySpinner.setSelection(selectedBaseCurrencyIndex)
        }

        val selectedResultCurrencyIndex = currencies?.indexOfFirst { it.code == resultCurrency?.code } ?: -1
        if (selectedResultCurrencyIndex != -1) {
            resultCurrencySpinner.setSelection(selectedResultCurrencyIndex)
        }

        updateExchangeSymbolUI()
    }

    private fun updateExchangeSymbolUI() {
        shortResultValueCurrencyName.text = "${resultCurrency?.code ?: ""}(${resultCurrency?.symbol ?: ""})"
        shortBaseValueCurrencyName.text = "${baseCurrency?.code ?: ""}(${baseCurrency?.symbol ?: ""})"
        updateLastRateUI()
    }

    private fun updateLastRateUI() {
        val baseValue = 1
        val exchangeRate = exchangeRates?.filter { it.base == baseCurrency?.code }?.firstOrNull()?.rates?.get(resultCurrency?.code) ?: 1.0
        val rateTitle = getString(R.string.converter_exchange_rate)
        val baseCode = "(${baseCurrency?.code ?: ""})"
        val resultCode = "(${resultCurrency?.code ?: ""})"
        val rateText = "$rateTitle $baseValue $baseCode = $exchangeRate $resultCode"
        rateTextView.text = rateText
    }

    private fun calculateExchangeRates() {
        val exchangeRate = exchangeRates?.filter { it.base == baseCurrency?.code }?.firstOrNull()?.rates?.get(resultCurrency?.code) ?: 1.0
//        val df = DecimalFormat(FORMAT)
//        df.roundingMode = RoundingMode.CEILING
//        if (isFromBase) {
//            val baseValue = baseValueEditText.getNoCommaString()?.toFloatOrNull() ?: 0.0F
//            val resultValue = baseValue * exchangeRate.toFloat()
//            resultValueEditText.ignoreTextChange = true
//            resultValueEditText.setText(df.format(resultValue))
//            resultValueEditText.ignoreTextChange = false
//        } else {
//            val resultValue = resultValueEditText.getNoCommaString()?.toFloatOrNull() ?: 0.0F
//            val baseValue = resultValue / exchangeRate.toFloat()
//            baseValueEditText.ignoreTextChange = true
//            baseValueEditText.setText(df.format(baseValue))
//            baseValueEditText.ignoreTextChange = false
//        }

        val baseValue = baseValueEditText.text.toString().toFloatOrNull() ?: 0.0F
        val resultValue = baseValue * exchangeRate.toFloat()
        resultValueTextView.text = getString(R.string.converter_result_format, resultValue)
    }

    private fun switchCurrency() {
        val tempBaseCurrency = baseCurrency
        val tempResultCurrency = resultCurrency

        baseCurrency = tempResultCurrency
        resultCurrency = tempBaseCurrency

        val tourConnex = TourConnex.getInstance()
        tourConnex.setBaseCurrency(tempResultCurrency!!)
        tourConnex.setResultCurrency(tempBaseCurrency!!)

//        val df = DecimalFormat(FORMAT)
//        df.roundingMode = RoundingMode.CEILING
//
//        baseValueEditText.ignoreTextChange = true
//        val resultValue = resultValueEditText.getNoCommaString()?.toFloatOrNull() ?: 0.0F
//        baseValueEditText.setText(df.format(resultValue))
//        baseValueEditText.ignoreTextChange = false

        updateUI()
        calculateExchangeRates()
    }

    private fun preparingCurrencies() {
        try {
            val inputStream = resources.openRawResource(R.raw.currency)
            val bytes = ByteArray(inputStream.available())
            inputStream.read(bytes)
            val json =  String(bytes)
            val gson = Gson()
            val empMapType = object : TypeToken<Map<String, Currency>>() {
            }.type
            val currencies: Map<String, Currency> = gson.fromJson(json, empMapType)
            this.currencies = currencies.values.toList().sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER, { it.name ?: "" }))
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private var textChangeListener = object: TextWatcher {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun afterTextChanged(s: Editable?) {
            calculateExchangeRates()
        }
    }
}
