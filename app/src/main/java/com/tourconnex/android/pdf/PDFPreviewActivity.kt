package com.tourconnex.android.pdf

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import kotlinx.android.synthetic.main.activity_pdfpreview.*
import kotlinx.android.synthetic.main.view_toolbar.*
import java.io.File

class PDFPreviewActivity : AbstractActivity() {

    companion object {
        private const val EXTRA_FILE_NAME = "EXTRA_FILE_NAME"
        private const val EXTRA_TRIP_NAME = "EXTRA_TRIP_NAME"
        fun create(context: Context, fileName: String?, tripName: String?): Intent {
            return Intent(context, PDFPreviewActivity::class.java).apply {
                putExtra(EXTRA_FILE_NAME, fileName)
                putExtra(EXTRA_TRIP_NAME, tripName)
            }
        }
    }

    private var fileName: String? = null
    private var tripName: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdfpreview)

        fileName = intent?.getStringExtra(EXTRA_FILE_NAME)
        tripName = intent?.getStringExtra(EXTRA_TRIP_NAME)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = tripName
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val file = File(fileName)
        pdfView.fromFile(file).load()
    }
}
