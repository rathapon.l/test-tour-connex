package com.tourconnex.android.pdf

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.FileItem
import kotlinx.android.synthetic.main.activity_pdf_viewer.*
import kotlinx.android.synthetic.main.view_toolbar.*



class PDFViewerActivity : AbstractActivity() {

    companion object {

        private const val TITLE_EXTRA = "TITLE_EXTRA"
        private const val FILE_EXTRA = "FILE_EXTRA"

        private const val MAX_LOAD = 5

        fun create(context: Context, title: String?, fileItem: FileItem): Intent {
            return Intent(context, PDFViewerActivity::class.java).apply {
                putExtra(TITLE_EXTRA, title)
                putExtra(FILE_EXTRA, fileItem)
            }
        }
    }

    lateinit var fileItem: FileItem
    private var title: String? = null
    private var loadCount = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf_viewer)

        fileItem = intent.getParcelableExtra(FILE_EXTRA)
        title = intent.getStringExtra(TITLE_EXTRA)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        webView.webViewClient = object: WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {

            }

            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                if (loadCount < MAX_LOAD) {
                    previewPDF()
                }
            }
        }

        previewPDF()
    }

    private fun previewPDF() {
        val fileURL = fileItem.fileUrl ?: ""

        webView.settings.javaScriptEnabled = true
        webView.loadUrl("https://docs.google.com/gview?embedded=true&url=$fileURL")
        loadCount += 1
    }
}
