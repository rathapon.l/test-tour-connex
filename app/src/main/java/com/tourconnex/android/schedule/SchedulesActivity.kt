package com.tourconnex.android.schedule

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.schedule.PageSchedule
import com.tourconnex.android.domain.schedule.Schedule
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.trip.CreateTripScheduleActivity
import kotlinx.android.synthetic.main.activity_schedules.*
import kotlinx.android.synthetic.main.view_list_schedule.view.*
import kotlinx.android.synthetic.main.view_toolbar.*
import retrofit2.Call
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class SchedulesActivity : AbstractActivity() {

    companion object {

        private const val REQUEST_SET_SCHEDULE = 1001

        private const val EXTRA_TRIP_ID = "EXTRA_TRIP_ID"

        fun create(context: Context, tripId: String?): Intent {
            return Intent(context, SchedulesActivity::class.java).apply {
                putExtra(EXTRA_TRIP_ID, tripId)
            }
        }
    }

    private var callSchedules: Call<PageSchedule>? = null
    private var callDeleteSchedule: Call<SimpleResponse>? = null

    private val visibleThreshold = 5
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private var loading: Boolean = false

    private var nextPageId: String? = null

    private var tripId: String? = null

    private val adapter = ScheduleAdapter { nullAbleSchedule ->
        nullAbleSchedule?.let { showRemoveScheduleConfirmation(it) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedules)

        tripId = intent?.getStringExtra(EXTRA_TRIP_ID)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.schedules_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        refreshingLayout.setOnRefreshListener {
            fetchData(null)
        }

        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recyclerView.adapter = adapter

        val linearLayoutManager = recyclerView
            .layoutManager as LinearLayoutManager

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                totalItemCount = linearLayoutManager.itemCount
                lastVisibleItem = linearLayoutManager
                    .findLastVisibleItemPosition()
                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold) && nextPageId != null) {
                    // End has been reached
                    // Do something
                    fetchData(nextPageId)
                    loading = true
                }
            }
        })

        fetchData(null)

    }

    private fun showRemoveScheduleConfirmation(schedule: Schedule) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(R.string.schedules_delete_confirmation_information)
        builder.setPositiveButton(R.string.common_ok) { dialog, _ ->
            dialog.dismiss()
            performDeleteSchedule(schedule)
        }
        builder.setNegativeButton(R.string.common_cancel) { dialog, _ -> dialog.cancel() }
        builder.show()

    }

    private fun performDeleteSchedule(schedule: Schedule) {
        callDeleteSchedule?.cancel()
        callDeleteSchedule = getApiClient().deleteSchedule(tripId, schedule?.id)
        callDeleteSchedule?.enqueue(object :  MainThreadCallback<SimpleResponse>(this, simpleProgressBar) {
            override fun onSuccess(result: SimpleResponse?) {
                if (result != null) {
                    didDeleteSchedule(schedule)
                }
            }

            override fun onError(response: Response<SimpleResponse>?, error: Error?) {
                showErrorDialog(error)
            }
        })

    }

    private fun didDeleteSchedule(schedule: Schedule) {
        val tempItems = adapter.items
        tempItems.addAll(tempItems.filter { it.id != schedule.id })

        adapter.addAll(tempItems)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.common_create, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun fetchData(nextPageId: String? = null) {
        callSchedules?.cancel()
        callSchedules = getApiClient().getSchedules(tripId)
        callSchedules?.enqueue(object : MainThreadCallback<PageSchedule>(this, simpleProgressBar) {
            override fun onSuccess(result: PageSchedule?) {
                refreshingLayout.isRefreshing = false
                var entities = ArrayList<Schedule>()
                result?.entities?.let { schedules ->

                    if (nextPageId == null) {
                        entities.addAll(schedules)
                    } else {
                        entities.addAll(adapter.items)
                        entities.addAll(schedules)
                    }
//                    allItems.addAll(entities)
                    adapter.addAll(entities)
                    this@SchedulesActivity.nextPageId = result?.pageInformation?.nextPageId
                    loading = false
                }
            }

            override fun onError(response: Response<PageSchedule>?, error: Error?) {
                refreshingLayout.isRefreshing = false
                loading = false
            }

        })
    }

    private fun createSchedule() {
        startActivityForResult(CreateTripScheduleActivity.create(this, tripId), REQUEST_SET_SCHEDULE)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.actionCreate) {
            createSchedule()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_SET_SCHEDULE -> {
                if (resultCode == Activity.RESULT_OK) {
                    //showMessageDialog(getString(R.string.create_trip_add_schedule_successfully))
                    fetchData(null)
                }
            }
        }
    }

    override fun onDestroy() {
        callSchedules?.cancel()
        callDeleteSchedule?.cancel()
        super.onDestroy()
    }

    private class ScheduleAdapter(val deleteCompletionBlock: (Schedule?) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<SchedultViewHolder>() {
        val items = java.util.ArrayList<Schedule>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchedultViewHolder {
            val viewHolder = SchedultViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list_schedule, parent, false), deleteCompletionBlock)

            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: SchedultViewHolder, position: Int) {
            holder.bindData(items[position])
        }

        fun addAll(entities: java.util.ArrayList<Schedule>?) {
            entities?.let {
                items.clear()
                items.addAll(it)
            }
            notifyDataSetChanged()
        }
    }

    private class SchedultViewHolder(itemView: View, val deleteCompletionBlock: (Schedule?) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        private var schedule: Schedule? = null

        init {

        }

        fun bindData(item: Schedule) {
            schedule = item

            itemView.messageTextView.text = item.message

            var dateText = "-"
            val format = SimpleDateFormat("d MMM yyyy hh:mm:ss")
            val date = getTime(item)
            dateText = format.format(date)

            itemView.scheduleTimeTextView.text = itemView.context.getString(R.string.schedules_time_prefix, dateText)
            itemView.removeImageView.visibility = if (TourConnex.getInstance().getActiveTrip()?.isTourLeader() == true) View.VISIBLE else View.GONE
            itemView.removeImageView.setOnClickListener {
                deleteCompletionBlock(item)
            }
        }

        private fun getTime(schedule: Schedule): Date {
            val date = schedule.time ?: Date()
            val dateString = toISO8601UTC(date)
            val convertedDate = fromISO8601UTC(dateString)
            return convertedDate ?: Date()
        }

        private fun toISO8601UTC(date: Date): String {
            val tz = TimeZone.getDefault()
            val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")
            df.timeZone = tz
            return df.format(date)
        }

        private fun fromISO8601UTC(dateStr: String): Date? {
            val tz = TimeZone.getTimeZone("GMT")
            val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")
            df.timeZone = tz

            try {
                return df.parse(dateStr)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return null
        }

    }
}
