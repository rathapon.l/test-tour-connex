package com.tourconnex.android.sos

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import kotlinx.android.synthetic.main.view_toolbar.*
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import kotlinx.android.synthetic.main.activity_guide_sos.*
import com.ogaclejapan.smarttablayout.utils.v4.Bundler
import com.tourconnex.android.domain.trip.SOS


class GuideSOSActivity : AbstractActivity() {

    companion object {
        fun create(context: Context): Intent {
            return Intent(context, GuideSOSActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guide_sos)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.sos_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val adapter = FragmentPagerItemAdapter(
            supportFragmentManager, FragmentPagerItems.with(this)
                .add(getString(R.string.sos_cancel), SOSFragment::class.java,
                    Bundler().putInt(SOSFragment.EXTRA_STATE, SOS.STATE_CANCEL).get())
                .add(getString(R.string.sos_waiting), SOSFragment::class.java,
                    Bundler().putInt(SOSFragment.EXTRA_STATE, SOS.STATE_WAITING).get())
                .add(getString(R.string.sos_done), SOSFragment::class.java,
                    Bundler().putInt(SOSFragment.EXTRA_STATE, SOS.STATE_DONE).get())
                .create()
        )

        viewPager.adapter = adapter
        viewPagerTab.setViewPager(viewPager)

        viewPager.currentItem = 1
    }
}
