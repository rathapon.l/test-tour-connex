package com.tourconnex.android.sos


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex

import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.fragment.AbstractFragment
import com.tourconnex.android.domain.sos.SOSItem
import com.tourconnex.android.domain.trip.PageSOS
import com.tourconnex.android.domain.trip.SOS
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.user.TravellerProfileActivity
import kotlinx.android.synthetic.main.fragment_so.*
import kotlinx.android.synthetic.main.view_list_sos.view.*
import retrofit2.Call
import retrofit2.Response
import java.util.ArrayList

class SOSFragment : AbstractFragment() {

    companion object {
        const val EXTRA_STATE = "EXTRA_STATE"

    }

    private var callSOS: Call<PageSOS>? = null
    private var callTakeActionSOS: Call<SimpleResponse>? = null

    private val visibleThreshold = 5
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private var loading: Boolean = false

    private var nextPageId: String? = null

    private var state = SOS.STATE_WAITING

    private val adapter = SOSAdapter({
        openTravellerActivity(it)
    }, {
        performTakeActionSOS(it)
    })

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_so, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        state = arguments?.getInt(EXTRA_STATE, SOS.STATE_WAITING) ?: SOS.STATE_WAITING

        refreshingLayout.setOnRefreshListener {
            fetchData(null)
        }

        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        recyclerView.adapter = adapter


        val linearLayoutManager = recyclerView
            .layoutManager as LinearLayoutManager

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                totalItemCount = linearLayoutManager.itemCount
                lastVisibleItem = linearLayoutManager
                    .findLastVisibleItemPosition()
                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold) && nextPageId != null) {
                    // End has been reached
                    // Do something
                    fetchData(nextPageId)
                    loading = true
                }
            }
        })

        fetchData(null)
    }

    private fun performTakeActionSOS(sos: SOSItem?) {
        if (sos?.state == SOS.STATE_DONE) {
            openTravellerActivity(sos?.sos)
        } else {
            callTakeActionSOS?.cancel()
            callTakeActionSOS = getApiClient().takeActionSos(sos?.sos?.tripId, sos?.sos?.id)
            callTakeActionSOS?.enqueue(object : MainThreadCallback<SimpleResponse>(activity) {
                override fun onSuccess(result: SimpleResponse?) {
                    if (result != null) {
                        openTravellerActivity(sos?.sos)
                        takeActionUser(sos)
                    }
                }

                override fun onError(response: Response<SimpleResponse>?, error: Error?) {
                    showErrorDialog(error)
                }
            })

        }
    }

    private fun takeActionUser(sos: SOSItem?) {
        val index = adapter.items.indexOfFirst { it.sos?.id == sos?.sos?.id }
        if (index != -1) {
            adapter.items[index].state = SOS.STATE_DONE
            adapter.items[index].actorUSer = TourConnex.getInstance().getUser()
            adapter.notifyItemChanged(index)
        }
    }

    private fun openTravellerActivity(sos: SOS?) {
        activity?.let { startActivity(TravellerProfileActivity.create(it, sos?.traveller, sos?.travellerId, sos?.tripId, sos?.location)) }
    }

    private fun fetchData(nextPageId: String?) {
        val tourConnex = TourConnex.getInstance()
        val activeTrip = tourConnex.getActiveTrip()

        callSOS?.cancel()
        callSOS = getApiClient().getSOS(activeTrip?.trip?.id, state)
        callSOS?.enqueue(object: MainThreadCallback<PageSOS>(activity) {
            override fun onSuccess(result: PageSOS?) {
                refreshingLayout.isRefreshing = false
                var entities = ArrayList<SOSItem>()
                result?.entities?.let {

                    if (nextPageId == null) {
                        val items = it.map { sos ->
                            SOSItem(sos, sos.state, sos.reactionUser)
                        }
                        entities.addAll(items)
                    } else {
                        val items = it.map { sos ->
                            SOSItem(sos, sos.state, sos.reactionUser)
                        }
                        entities.addAll(adapter.items)
                        entities.addAll(items)
                    }
//
                    adapter.addAll(entities)
                    this@SOSFragment.nextPageId = result?.pageInformation?.nextPageId
                    loading = false
                } ?: kotlin.run {
                    if (nextPageId == null) {
                        val entities = ArrayList<SOSItem>()
                        adapter.addAll(entities)
                    }
                }
            }

            override fun onError(response: Response<PageSOS>?, error: Error?) {
                refreshingLayout.isRefreshing = false
                loading = false
            }
        })
    }

    private class SOSAdapter(val completionBlock: (SOS?) -> Unit, val takeActionCompletionBlock: (SOSItem?) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<SOSViewHolder>() {
        val items = ArrayList<SOSItem>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SOSViewHolder {
            val viewHolder = SOSViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list_sos, parent, false), completionBlock, takeActionCompletionBlock)

            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: SOSViewHolder, position: Int) {
            holder.bindData(items[position])
        }

        fun addAll(entities: ArrayList<SOSItem>?) {
            entities?.let {
                items.clear()
                items.addAll(it)
            }
            notifyDataSetChanged()
        }
    }

    private class SOSViewHolder(itemView: View, val completionBlock: ((SOS?) -> Unit), val takeActionCompletionBlock: (SOSItem?) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        private var sosItem: SOSItem? = null

        init {

        }

        fun bindData(item: SOSItem) {
            sosItem = item

            itemView.setOnClickListener {}

            itemView.takeActionButton.setOnClickListener {
                takeActionCompletionBlock(item)
            }

            itemView.cancelTextView.visibility = View.GONE
            itemView.takenByTextView.visibility = View.GONE
            itemView.takeActionButton.visibility = View.GONE

            item.sos?.user?.let {
                itemView.usernameTextView.text = it.getFullName()
                Glide.with(itemView.context).load(it.profilePicture?.largeImageUrl).into(itemView.profileImageView)
            }

            when (item.state) {
                SOS.STATE_CANCEL -> {
                    itemView.cancelTextView.visibility = View.VISIBLE
                }
                SOS.STATE_WAITING -> {
                    itemView.takeActionButton.visibility = View.VISIBLE
                }
                SOS.STATE_DONE -> {
                    itemView.takenByTextView.visibility = View.VISIBLE
                    itemView.takenByTextView.text = itemView.context.getString(R.string.sos_take_action_by_prefix, item.actorUSer?.getFullName())
                    itemView.setOnClickListener { _ ->
                        sosItem?.let {
                            completionBlock(it.sos)
                        }
                    }
                }
            }
        }

    }


    override fun onDestroy() {
        callSOS?.cancel()
        callTakeActionSOS?.cancel()
        super.onDestroy()
    }
}
