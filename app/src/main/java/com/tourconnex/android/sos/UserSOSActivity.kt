package com.tourconnex.android.sos

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.conversation.ConversationRetriveEvent
import com.tourconnex.android.domain.sos.SOSTakeActionRetriveEvent
import com.tourconnex.android.domain.sos.form.CancelSOSForm
import com.tourconnex.android.domain.trip.SOS
import com.tourconnex.android.framework.http.MainThreadCallback
import kotlinx.android.synthetic.main.activity_user_sos.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Call
import retrofit2.Response

class UserSOSActivity : AbstractActivity() {

    companion object {

        private const val EXTRA_SOS_ID = "EXTRA_SOS_ID"

        fun create(context: Context, sos: SOS?): Intent {
            return Intent(context, UserSOSActivity::class.java).apply {
                putExtra(EXTRA_SOS_ID, sos)
            }
        }
    }

    private var sos: SOS? = null
    private var callSOS: Call<SimpleResponse>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_sos)

        sos = intent?.getParcelableExtra(EXTRA_SOS_ID)

        content.startRippleAnimation()
        cancelButton.setOnClickListener { performCancelSOS() }
    }

    override fun onBackPressed() {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun getConversationRetriveEvent(s: SOSTakeActionRetriveEvent) {
        showSOSHasBeenActedByGuideMessage(s.message?.notification?.body ?: getString(R.string.sos_have_been_acted_by_guide))
    }

    private fun showSOSHasBeenActedByGuideMessage(message: String?) {
        showMessageDialog(message) {
            finish()
        }
    }
    private fun performCancelSOS() {
        callSOS?.cancel()
        callSOS = getApiClient().cancelSOS(sos?.tripId, sos?.id)
        callSOS?.enqueue(object: MainThreadCallback<SimpleResponse>(this, simpleProgressBar) {
            override fun onSuccess(result: SimpleResponse?) {
                if (result != null) {
                    finish()
                }
            }

            override fun onError(response: Response<SimpleResponse>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    override fun onDestroy() {
        callSOS?.cancel()
        super.onDestroy()
    }
}
