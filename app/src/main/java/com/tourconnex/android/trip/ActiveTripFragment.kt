package com.tourconnex.android.trip

import android.os.Bundle
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.trip.TripTraveller
import com.tourconnex.android.framework.http.MainThreadCallback
import kotlinx.android.synthetic.main.fragment_trip.*
import retrofit2.Response

class ActiveTripFragment: TripFragment() {

    companion object {
        private const val EXTRA_TRIP = "EXTRA_TRIP"

        const val FRAGMENT_TAG = "TripFragment"

        fun create(trip: TripTraveller?): ActiveTripFragment {
            val tripFragment = ActiveTripFragment()
            val args = Bundle()
            args.putString(TripFragment.EXTRA_TRIP_ID, trip?.id)
            args.putParcelable(EXTRA_TRIP, trip)
            tripFragment.arguments = args

            return tripFragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        trip = arguments?.getParcelable(EXTRA_TRIP)
        super.onActivityCreated(savedInstanceState)

        updateUI()
    }

    override fun fetchData() {
        if (trip?.isPreview == true) {
            fetchPreviewTrip()
        } else {
            fetchTrip()
        }
    }

    private fun fetchPreviewTrip() {
        callTrip?.cancel()
        callTrip = getApiClient().getTripPreview(tripId)
        callTrip?.enqueue(object: MainThreadCallback<TripTraveller>(activity) {
            override fun onSuccess(result: TripTraveller?) {
                refreshingLayout.isRefreshing = false
                this@ActiveTripFragment.trip = result
                this@ActiveTripFragment.numberOfTraveller = result?.trip?.numberOfTravellers ?: 0
                fetchGuides()
                updateUI()
            }

            override fun onError(response: Response<TripTraveller>?, error: Error?) {
                refreshingLayout.isRefreshing = false
            }
        })
    }

    private fun fetchTrip() {
        callTrip?.cancel()
        callTrip = getApiClient().getTrip(tripId)
        callTrip?.enqueue(object: MainThreadCallback<TripTraveller>(activity) {
            override fun onSuccess(result: TripTraveller?) {
                refreshingLayout.isRefreshing = false
                this@ActiveTripFragment.trip = result
                this@ActiveTripFragment.numberOfTraveller = result?.trip?.numberOfTravellers ?: 0
                fetchGuides()
                updateUI()
            }

            override fun onError(response: Response<TripTraveller>?, error: Error?) {
                refreshingLayout.isRefreshing = false

            }
        })
    }
}