package com.tourconnex.android.trip

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.CaptureManager
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.traveller.TravellerCheckInForm
import com.tourconnex.android.domain.users.Traveller
import com.tourconnex.android.framework.http.MainThreadCallback
import kotlinx.android.synthetic.main.activity_add_travellers.*
import kotlinx.android.synthetic.main.view_toolbar.*
import retrofit2.Call
import retrofit2.Response

interface OnAddTravellerResult {
    fun onAssignUserSuccess()
    fun onCancel()
}

class AddTravellersActivity : AbstractActivity() {

    companion object {

        private const val EXTRA_TRIP_ID = "EXTRA_TRIP_ID"
        private const val EXTRA_TRAVELLER = "EXTRA_TRAVELLER"

        fun create(context: Context, tripId: String?, traveller: Traveller?): Intent {
            return Intent(context, AddTravellersActivity::class.java).apply {
                putExtra(EXTRA_TRIP_ID, tripId)
                putExtra(EXTRA_TRAVELLER, traveller)
            }
        }

        fun onActivityResult(resultCode: Int, data: Intent?, onAddTravellerResult: OnAddTravellerResult?) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    onAddTravellerResult?.onAssignUserSuccess()
                }

                Activity.RESULT_CANCELED -> {
                    onAddTravellerResult?.onCancel()
                }
            }
        }
    }

    private var tripId: String? = null
    private var traveller: Traveller? = null

    private var capture: CaptureManager? = null
    private var callAssignUser: Call<SimpleResponse>? = null

    private var scanning = false

    private var scannedQRCode = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_travellers)

        tripId = intent.getStringExtra(EXTRA_TRIP_ID)
        traveller = intent.getParcelableExtra(EXTRA_TRAVELLER)

        actionBarView.title = getString(R.string.traveller_add_traveller)
        setSupportActionBar(actionBarView)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        barcodeScannerView.setStatusText("")

        capture = CaptureManager(this, barcodeScannerView)
        capture?.initializeFromIntent(intent, savedInstanceState)
//        capture?.decode()

        barcodeScannerView.decodeContinuous(object: BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult?) {
                Log.d("tag", "found barcodeResult")
                handleQrCodeIfNeeded(result?.result?.text ?: "")

            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
                Log.d("tag", "found possibleResultPoints")
            }
        })

        barcodeScannerView.setTorchOff()
    }

    private fun handleQrCodeIfNeeded(qrCode: String) {
        if (qrCode.isEmpty() || isQRCodeExist(qrCode)) {
            return
        }

        performRedeemQRCode(qrCode)
    }

    private fun performRedeemQRCode(qrCode: String) {
        if (!scanning) {
            scanning = true

            val form = TravellerCheckInForm(qrCode)

            callAssignUser?.cancel()
            callAssignUser = getApiClient().travellerCheckIn(tripId, traveller?.id, form)
            callAssignUser?.enqueue(object : MainThreadCallback<SimpleResponse>(this, simpleProgressBar) {
                override fun onSuccess(result: SimpleResponse?) {
                    scanning = false
                    //scannedQRCode.add(qrCode)
                    setResult(Activity.RESULT_OK)
                    finish()
                }

                override fun onError(response: Response<SimpleResponse>?, error: Error?) {
                    scanning = false
                    scannedQRCode.add(qrCode)
                    showErrorDialog(error)
                }
            })
        }
    }

    private fun isQRCodeExist(qrCode: String): Boolean {
        return scannedQRCode.contains(qrCode)
    }

    override fun onResume() {
        super.onResume()
        capture?.onResume()
    }

    override fun onPause() {
        super.onPause()
        capture?.onPause()
    }

    override fun onDestroy() {
        //callRedeem?.cancel()
        super.onDestroy()
        capture?.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        capture?.onSaveInstanceState(outState)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event)
    }
}
