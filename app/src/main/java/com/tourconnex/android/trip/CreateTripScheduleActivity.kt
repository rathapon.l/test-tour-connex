package com.tourconnex.android.trip

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.schedule.form.CreateScheduleForm
import com.tourconnex.android.framework.http.MainThreadCallback
import kotlinx.android.synthetic.main.activity_create_trip_schedule.*
import kotlinx.android.synthetic.main.view_toolbar.*
import retrofit2.Call
import retrofit2.Response

class CreateTripScheduleActivity : AbstractActivity() {

    companion object {

        private const val EXTRA_TRIP_ID = "EXTRA_TRIP_ID"

        fun create(context: Context, tripId: String?): Intent {
            return Intent(context, CreateTripScheduleActivity::class.java).apply {
                putExtra(EXTRA_TRIP_ID, tripId)
            }
        }
    }

    private var callSchedule: Call<SimpleResponse>? = null
    private var tripId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_trip_schedule)

        tripId = intent?.getStringExtra(EXTRA_TRIP_ID)

        actionBarView.title = getString(R.string.create_trip_schedule)
        setSupportActionBar(actionBarView)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        submitButton.setOnClickListener { submitScheduleIfNeeded() }
    }

    private fun validateError(): String? {
        var message: String? = null
        val scheduleMessage = messageEditText.text.toString()
        val time = timeEditText.text.toString()
        if (scheduleMessage.isEmpty()) {
            message = getString(R.string.create_trip_please_fill_message)
        } else if (time.isEmpty()) {
            message = getString(R.string.create_trip_please_fill_time)
        } else if (time.toIntOrNull() == null) {
            message = getString(R.string.create_trip_please_time_mismatch)
        } else if (time.toInt() == 0) {
            message = getString(R.string.create_trip_please_time_must_more_than_zero)
        }

        return message
    }

    private fun submitScheduleIfNeeded() {
        validateError()?.let {
            showMessageDialog(it)

            return
        }

        val scheduleMessage = messageEditText.text.toString()
        val time = timeEditText.text.toString().toIntOrNull() ?: 0

        confirmationToSubmitSchedule(scheduleMessage, time)
    }

    private fun confirmationToSubmitSchedule(message: String, time: Int) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(getString(R.string.create_trip_add_schedule_information, message, time))
        builder.setPositiveButton(R.string.common_ok) { dialog, _ ->
            dialog.dismiss()
            performSchedule(message, time)
        }
        builder.setNegativeButton(R.string.common_cancel) { dialog, _ -> dialog.cancel() }
        builder.show()
    }

    private fun performSchedule(message: String, time: Int) {
        val form = CreateScheduleForm(message, time)
        callSchedule?.cancel()
        callSchedule = getApiClient().setSchedule(tripId, form)
        callSchedule?.enqueue(object: MainThreadCallback<SimpleResponse>(this, simpleProgressBar) {
            override fun onSuccess(result: SimpleResponse?) {
                if (result != null) {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }

            override fun onError(response: Response<SimpleResponse>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    override fun onDestroy() {
        callSchedule?.cancel()
        super.onDestroy()
    }
}
