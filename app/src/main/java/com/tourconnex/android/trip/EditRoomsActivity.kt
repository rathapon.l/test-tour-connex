package com.tourconnex.android.trip

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.Room
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.trip.form.EditRoomForm
import com.tourconnex.android.framework.http.MainThreadCallback
import kotlinx.android.synthetic.main.activity_edit_rooms.*
import kotlinx.android.synthetic.main.view_list_edit_room.view.*
import kotlinx.android.synthetic.main.view_toolbar.*
import retrofit2.Call
import retrofit2.Response

interface OnEditRoomsActivityResult {
    fun onEditedRooms(rooms: ArrayList<Room>?, dayId: Int?)
    fun onCancel()
}

class EditRoomsActivity : AbstractActivity() {

    companion object {
        private const val EXTRA_TRIP_ID = "EXTRA_TRIP_ID"
        private const val EXTRA_ROOMS = "EXTRA_ROOMS"
        private const val EXTRA_DAY_ID = "EXTRA_DAY_ID"

        private const val RESULT_DAY_ID = "RESULT_DAY_ID"
        private const val RESULT_ROOMS = "RESULT_ROOMS"

        fun create(context: Context, tripId: String?, rooms: ArrayList<Room>?, dayId: Int?): Intent {
            return Intent(context, EditRoomsActivity::class.java).apply {
                putExtra(EXTRA_TRIP_ID, tripId)
                putParcelableArrayListExtra(EXTRA_ROOMS, rooms)
                putExtra(EXTRA_DAY_ID, dayId)
            }
        }

        fun onActivityResult(resultCode: Int, data: Intent?, onEditRoomsActivityResult: OnEditRoomsActivityResult) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val rooms = data?.getParcelableArrayListExtra<Room>(RESULT_ROOMS)
                    val dayId = data?.getIntExtra(RESULT_DAY_ID, -1)
                    onEditRoomsActivityResult.onEditedRooms(rooms, dayId)
                }

                Activity.RESULT_CANCELED -> {
                    onEditRoomsActivityResult.onCancel()
                }
            }
        }
    }

    private var tripId: String? = null
    private var rooms: ArrayList<Room>? = null
    private var dayId: Int? = null
    private var callEditRoom: Call<SimpleResponse>? = null

    private val adapter = RoomAdapter()

    private var tempRoomNumbers: List<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_rooms)

        tripId = intent?.getStringExtra(EXTRA_TRIP_ID)
        dayId = intent?.getIntExtra(EXTRA_DAY_ID, 0)
        rooms = intent?.getParcelableArrayListExtra(EXTRA_ROOMS)
        tempRoomNumbers = rooms?.mapNotNull { it.roomNumber }

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.edit_room_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recyclerView.adapter = adapter

        rooms?.let {
            adapter.addAll(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.common_save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun save() {
        performEditRooms()
    }

    private fun editedRooms(): ArrayList<Room>? {
        if (tempRoomNumbers == null || tempRoomNumbers?.size != rooms?.size) {
            return null
        }

        val editedRooms = rooms
        var counter = 0
        editedRooms?.forEach {
            val tempRoomNumber = tempRoomNumbers!![counter]
            if (tempRoomNumber != it.roomNumber) {
                it.lestestRoomNumber = tempRoomNumber
            }

            counter++
        }

        return editedRooms
    }

    private fun performEditRooms() {
        val editedRooms = editedRooms()
        val form = EditRoomForm(editedRooms)
        callEditRoom?.cancel()
        callEditRoom = getApiClient().editRoom(tripId, dayId, form)
        callEditRoom?.enqueue(object: MainThreadCallback<SimpleResponse>(this, simpleProgressBar) {
            override fun onSuccess(result: SimpleResponse?) {
                didEditedRooms(editedRooms)
            }

            override fun onError(response: Response<SimpleResponse>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    private fun didEditedRooms(editedRooms: ArrayList<Room>?) {
        setResult(Activity.RESULT_OK, Intent().apply {
            putParcelableArrayListExtra(RESULT_ROOMS, editedRooms)
            putExtra(RESULT_DAY_ID, dayId)
        })

        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.actionSave -> {
                save()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        callEditRoom?.cancel()
        super.onDestroy()
    }

    private class RoomAdapter: RecyclerView.Adapter<RoomViewHolder>() {
        val items = ArrayList<Room>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomViewHolder {
            val viewHolder = RoomViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list_edit_room, parent, false))

            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: RoomViewHolder, position: Int) {
            holder.bindData(items[position])
        }

        fun addAll(entities: ArrayList<Room>?) {
            entities?.let {
                items.clear()
                items.addAll(it)
            }
            notifyDataSetChanged()
        }
    }

    private class RoomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var room: Room? = null

        init {

        }

        fun bindData(item: Room) {
            room = item
            itemView.roomNumberEditText.removeTextChangedListener(roomNumberTextWatcher)
            itemView.roomNumberEditText.setText(room?.roomNumber)
            itemView.roomNumberEditText.addTextChangedListener(roomNumberTextWatcher)

            itemView.remarkEditText.removeTextChangedListener(remarkNumberTextWatcher)
            itemView.remarkEditText.setText(room?.remark)
            itemView.remarkEditText.addTextChangedListener(remarkNumberTextWatcher)

            var travellerNames = item.travellers?.map {
                val title = it.title ?: ""
                val firstName = it.firstName ?: ""
                val lastName = it.lastName ?: ""

                "$title $firstName $lastName"
            }

            itemView.memberContainerView.visibility = if (item.travellers == null) View.GONE else View.VISIBLE
            travellerNames = travellerNames?.filter { !it.isNullOrEmpty() }

            itemView.memberTextView.text = travellerNames?.joinToString("\n")
        }

        private var roomNumberTextWatcher = object: TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                room?.roomNumber = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
        }

        private var remarkNumberTextWatcher = object: TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                room?.remark = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
        }
    }
}
