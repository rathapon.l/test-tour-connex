package com.tourconnex.android.trip

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.framework.utils.ImageUtils
import kotlinx.android.synthetic.main.activity_trip_qrcode.*
import kotlinx.android.synthetic.main.view_toolbar.*

class GuideTripQRCodeActivity : AbstractActivity() {

    companion object {
        private const val EXTRA_TRIP_ID = "EXTRA_TRIP_ID"
        private const val EXTRA_GUIDE_ID = "EXTRA_GUIDE_ID"
        fun create(context: Context, tripId: String?, guideId: String?): Intent {
            return Intent(context, GuideTripQRCodeActivity::class.java).apply {
                putExtra(EXTRA_TRIP_ID, tripId)
                putExtra(EXTRA_GUIDE_ID, guideId)
            }
        }
    }

    private var tripId: String? = null
    private var guideId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip_qrcode)

        tripId = intent.getStringExtra(EXTRA_TRIP_ID)
        guideId = intent.getStringExtra(EXTRA_GUIDE_ID)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.trip_qr_code_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        updateUI()
    }

    private fun updateUI() {
        val qrCode = guideId ?: ""
        val bmp = ImageUtils.genQRFromFreeText(qrCode, 400, 400)
        bmp?.let { bitmap ->
            qrCodeImageView.setImageBitmap(bitmap)
        }
    }
}
