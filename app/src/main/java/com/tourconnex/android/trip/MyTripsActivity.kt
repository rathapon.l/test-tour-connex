package com.tourconnex.android.trip

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.trip.Trip
import com.tourconnex.android.domain.trip.TripTravellerPage
import com.tourconnex.android.domain.trip.TripTraveller
import com.tourconnex.android.domain.users.User
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.user.EditMainInformationActivity
import com.tourconnex.android.user.OnEditMainInformationActivityResult
import kotlinx.android.synthetic.main.activity_my_trips.*
import kotlinx.android.synthetic.main.view_toolbar.*
import kotlinx.android.synthetic.main.view_trip.view.*
import retrofit2.Call
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class MyTripsActivity : AbstractActivity() {

    companion object {
        private const val REQUEST_EDIT_PROFILE = 12315

        fun create(context: Context): Intent {
            return Intent(context, MyTripsActivity::class.java)
        }
    }

    var callTrips: Call<TripTravellerPage>? = null

    private val adapter = OrderAdapter({ optionalOrder ->
        optionalOrder?.let {
            didSelectTrip(it)
        }
    }, { optionalOrder ->
        optionalOrder?.let {
            didActiveTrip(it)
        }
    }, { optionalOrder ->
        optionalOrder?.let {
            didInActiveTrip(it)
        }
    })

    private val visibleThreshold = 5
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private var loading: Boolean = false

    private var nextPageId: String? = null

    private var activeTrip: TripTraveller? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_trips)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.more_my_trips)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        refreshingLayout.setOnRefreshListener {
            fetchData(null)
        }

        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recyclerView.adapter = adapter


        val linearLayoutManager = recyclerView
            .layoutManager as LinearLayoutManager

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                totalItemCount = linearLayoutManager.itemCount
                lastVisibleItem = linearLayoutManager
                    .findLastVisibleItemPosition()
                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold) && nextPageId != null) {
                    // End has been reached
                    // Do something
                    fetchData(nextPageId)
                    loading = true
                }
            }
        })

        fetchData(null)
        //editUserInformationIfNeeded()
    }

    override fun onResume() {
        super.onResume()
        val tourConnex = TourConnex.getInstance()
        activeTrip = tourConnex.getActiveTrip()
        setActiveTrip(activeTrip)
    }

    private fun editUserInformationIfNeeded() {
        val user = TourConnex.getInstance().getUser()
        if (user?.citizenId == null || user?.passportNumber == null) {
            startActivityForResult(EditMainInformationActivity.create(this, user), REQUEST_EDIT_PROFILE)
        }
    }

    private fun fetchData(nextPageId: String? = null) {
        val user = TourConnex.getInstance().getUser()
        callTrips?.cancel()
        callTrips = getApiClient().getTrips(user?.citizenId, user?.passportNumber)
        callTrips?.enqueue(object : MainThreadCallback<TripTravellerPage>(this, simpleProgressBar) {
            override fun onSuccess(result: TripTravellerPage?) {
                refreshingLayout.isRefreshing = false
                var entities = ArrayList<TripTraveller>()
                result?.entities?.let {

                    if (nextPageId == null) {
                        entities.addAll(it)
                    } else {
                        entities.addAll(adapter.items)
                        entities.addAll(it)
                    }
//
                    adapter.addAll(entities)
                    this@MyTripsActivity.nextPageId = result?.pageInformation?.nextPageId
                    loading = false
                }
            }

            override fun onError(response: Response<TripTravellerPage>?, error: Error?) {
                //refreshingLayout.isRefreshing = false
                loading = false
            }

        })
    }

    private fun didSelectTrip(trip: TripTraveller) {
        startActivity(TripActivity.create(this, trip.id ?: "", trip.trip?.title ?: ""))
    }

    private fun didActiveTrip(trip: TripTraveller) {
        setActiveTrip(trip)
    }

    private fun didInActiveTrip(trip: TripTraveller) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(R.string.trip_in_active_confirmation)
        builder.setPositiveButton(R.string.common_ok) { dialog, _ ->
            dialog.dismiss()
            setActiveTrip(null)
        }
        builder.setNegativeButton(R.string.common_cancel) { dialog, _ -> dialog.cancel() }
        builder.show()
    }

    private fun setActiveTrip(activeTrip: TripTraveller?) {
        val tourConnex = TourConnex.getInstance()
        tourConnex.setActiveTrip(activeTrip)
        this.activeTrip = activeTrip
        adapter.setActiveTrip(activeTrip)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_EDIT_PROFILE -> {
                EditMainInformationActivity.onActivityResult(resultCode, data, object :
                    OnEditMainInformationActivityResult {
                    override fun onEditProfileSuccess(user: User) {
                        TourConnex.getInstance().setUser(user)
                        fetchData()
                    }

                    override fun onCancel() {
                        finish()
                    }
                })
            }
        }
    }

    override fun onDestroy() {
        callTrips?.cancel()
        super.onDestroy()
    }

    private class OrderAdapter(val completionBlock: (TripTraveller?) -> Unit, val activeTripBlock: (TripTraveller?) -> Unit, val inActiveTripBlock: (TripTraveller?) -> Unit) : RecyclerView.Adapter<TripViewHolder>() {
        val items = ArrayList<TripTraveller>()
        private var activeTrip: TripTraveller? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripViewHolder {
            val viewHolder = TripViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_trip, parent, false), completionBlock, activeTripBlock, inActiveTripBlock)

            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: TripViewHolder, position: Int) {
            holder.bindData(items[position], activeTrip)
        }

        fun addAll(entities: ArrayList<TripTraveller>?) {
            entities?.let {
                items.clear()
                items.addAll(it)
            }
            notifyDataSetChanged()
        }

        fun setActiveTrip(activeTrip: TripTraveller?) {
            this.activeTrip = activeTrip
            notifyDataSetChanged()
        }
    }

    private class TripViewHolder(itemView: View, completionBlock: ((TripTraveller?) -> Unit) , val activeTripBlock: (TripTraveller?) -> Unit, val inActiveTripBlock: (TripTraveller?) -> Unit) : RecyclerView.ViewHolder(itemView) {
        private var trip: TripTraveller? = null

        init {
            itemView.setOnClickListener { _ ->
                trip?.let {
                    completionBlock(it)
                }
            }

            itemView.activeButton.setOnClickListener {
                trip?.let {
                    activeTripBlock(it)
                }
            }
            itemView.inActiveButton.setOnClickListener {
                trip?.let {
                    inActiveTripBlock(it)
                }
            }
        }

        fun bindData(item: TripTraveller, activeTrip: TripTraveller?) {
            trip = item
            val tripInformation = item.trip
            itemView.titleTextView.text = tripInformation?.title

            itemView.numberOfMembersTextView.text = (tripInformation?.numberOfTravellers ?: 0).toString()
            itemView.locationTextView.text = tripInformation?.destination

            tripInformation?.description?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    itemView.tripDetailTextView.text = Html.fromHtml(it, Html.FROM_HTML_MODE_COMPACT)
                } else {
                    itemView.tripDetailTextView.text = Html.fromHtml(it)
                }
            }

            setDateText(itemView.startDateTextView, tripInformation?.startDateTime)
            setDateText(itemView.endDateTextView, tripInformation?.endDateTime)

            Glide.with(itemView.context).load(tripInformation?.coverPhoto?.largeImageUrl).into(itemView.tripImageView)

            val isActive = activeTrip?.id == item.id

            itemView.inActiveButton.visibility = if (isActive) View.VISIBLE else View.GONE
            itemView.activeButton.visibility = if (isActive) View.GONE else View.VISIBLE
        }

        private fun setDateText(textView: TextView?, date: Date?) {
            textView?.visibility = View.GONE
            date?.let {
                textView?.visibility = View.VISIBLE
                val format = SimpleDateFormat("dd MMMM yyyy")
                textView?.text = format.format(it)
            } ?: kotlin.run {

            }
        }
    }
}
