package com.tourconnex.android.trip

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tourconnex.android.R
import com.tourconnex.android.color.ColorSelectionActivity
import com.tourconnex.android.color.OnColorSelectionActivityResult
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.users.PageTraveller
import com.tourconnex.android.domain.users.Traveller
import com.tourconnex.android.framework.http.MainThreadCallback
import kotlinx.android.synthetic.main.activity_travellers.*
import retrofit2.Call
import retrofit2.Response
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.common.Type
import com.tourconnex.android.domain.traveller.TravellerItem
import com.tourconnex.android.domain.traveller.form.SetTravellerGroupColorForm
import com.tourconnex.android.user.OnTravellerProfileActivityResult
import com.tourconnex.android.user.TravellerProfileActivity
import kotlinx.android.synthetic.main.view_list_traveller.view.*
import kotlinx.android.synthetic.main.view_toolbar.*

interface OnTravellersActivityResult {
    fun onAssignUserSuccess()
    fun onRemoveTravellerSuccess()
    fun onCancel()
}

class TravellersActivity : AbstractActivity() {

    companion object {

        private const val ASSIGN_TYPE = 1
        private const val REMOVE_TYPE = 0

        private const val RESULT_TYPE_EXTRA = "RESULT_TYPE_EXTRA"
        private const val TRIP_ID_EXTRA = "TRIP_ID_EXTRA"

        private const val REQUEST_TRAVELLER = 12200
        private const val REQUEST_CHANGE_GROUP_COLOR = 12201

        fun create(context: Context, tripId: String): Intent {
            return Intent(context, TravellersActivity::class.java).apply {
                putExtra(TRIP_ID_EXTRA, tripId)
            }
        }

        fun onActivityResult(resultCode: Int, data: Intent?, onTravellersActivityResult: OnTravellersActivityResult?) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val resultType = data?.getIntExtra(RESULT_TYPE_EXTRA, ASSIGN_TYPE) ?: ASSIGN_TYPE
                    when (resultType) {
                        ASSIGN_TYPE -> {
                            onTravellersActivityResult?.onAssignUserSuccess()
                        }

                        REMOVE_TYPE -> {
                            onTravellersActivityResult?.onRemoveTravellerSuccess()
                        }
                    }

                }

                Activity.RESULT_CANCELED -> {
                    onTravellersActivityResult?.onCancel()
                }
            }
        }
    }

    var callTravellers: Call<PageTraveller>? = null
    var callSetGroupColorTraveller: Call<SimpleResponse>? = null

    private val adapter = TravellerAdapter({ optionalTraveller ->
        didSelectTraveller(optionalTraveller)
    }, {
        openSetTravellerGroupColor(it)
    })

    private var travellerToChangeColor: Traveller? = null

    lateinit var tripId: String
    private val allItems = ArrayList<TravellerItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_travellers)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.traveller_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        searchEditText.addTextChangedListener(onTravellerNameChangeListener)

        tripId = intent.getStringExtra(TRIP_ID_EXTRA)

        refreshingLayout.setOnRefreshListener {
            fetchData()
        }

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        fetchData()
    }

    private fun fetchData() {
        callTravellers?.cancel()
        callTravellers = getApiClient().getTripTravellers(tripId)
        callTravellers?.enqueue(object : MainThreadCallback<PageTraveller>(this, simpleProgressBar) {
            override fun onSuccess(result: PageTraveller?) {
                refreshingLayout.isRefreshing = false
                val entities = ArrayList<TravellerItem>()
                result?.entities?.let { travellers ->
                    entities.addAll(travellers.mapNotNull { TravellerItem(it, it.colorType?.name) })
                    entities.sortBy { "${it.traveller?.firstName ?: ""}${it.traveller?.lastName ?: ""}" }

                    allItems.clear()
                    allItems.addAll(entities)
                    adapter.addAll(entities)
                }
            }

            override fun onError(response: Response<PageTraveller>?, error: Error?) {
                refreshingLayout.isRefreshing = false
            }

        })
    }

    private fun searchByText(text: String) {
        var itemToDisplay = allItems.toList()

        if (text.isNotEmpty()) {
            itemToDisplay = allItems.filter {
                val firstName = it.traveller?.firstName ?: ""
                val lastName = it.traveller?.lastName ?: ""
                val name = "$firstName$lastName"
                name.toLowerCase().contains(text.toLowerCase())
            }
        }

        val itemToDisplayArrayList: ArrayList<TravellerItem> = ArrayList()
        itemToDisplayArrayList.addAll(itemToDisplay)
        adapter.addAll(itemToDisplayArrayList)
    }

    private val onTravellerNameChangeListener = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            searchByText(searchEditText.text.toString())
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(str: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }
    }

    private fun openSetTravellerGroupColor(traveller: Traveller?) {
        travellerToChangeColor = traveller
        startActivityForResult(ColorSelectionActivity.create(this), REQUEST_CHANGE_GROUP_COLOR)
    }

    private fun didChangeColor(color: Type?, travellerId: String?) {
        val index = adapter.items?.indexOfFirst { it.traveller?.id == travellerId }
        if (index != -1) {
            adapter.items[index].colorName = color?.name
            adapter.notifyItemChanged(index)
        }

        performColorForTraveller(color, travellerId)
    }

    private fun performColorForTraveller(color: Type?, travellerId: String?) {
        val form = SetTravellerGroupColorForm(color?.value ?: 1)
        callSetGroupColorTraveller?.cancel()
        callSetGroupColorTraveller = getApiClient().setTravellerGroupColor(tripId, travellerId, form)
        callSetGroupColorTraveller?.enqueue(object: MainThreadCallback<SimpleResponse>(this) {
            override fun onSuccess(result: SimpleResponse?) {

            }

            override fun onError(response: Response<SimpleResponse>?, error: Error?) {

            }
        })
    }

    private fun didSelectTraveller(traveller: Traveller?) {
        startActivityForResult(TravellerProfileActivity.create(this, traveller, tripId, traveller?.id, null), REQUEST_TRAVELLER)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_TRAVELLER -> {
                TravellerProfileActivity.onActivityResult(resultCode, data, object: OnTravellerProfileActivityResult {
                    override fun onSelectColor(colorType: Type?, travellerId: String?) {
                        didChangeColor(colorType, travellerId)
                    }

                    override fun onCancel() {

                    }
                })
            }
            REQUEST_CHANGE_GROUP_COLOR -> {
                ColorSelectionActivity.onActivityResult(resultCode, data, object: OnColorSelectionActivityResult{
                    override fun onCancel() {
                        travellerToChangeColor = null
                    }

                    override fun onSelectColorType(colorType: Type?) {
                        didChangeColor(colorType, travellerToChangeColor?.id)
                    }
                })
            }
        }
    }

    override fun onDestroy() {
        callTravellers?.cancel()
        callSetGroupColorTraveller?.cancel()
        super.onDestroy()
    }

    private class TravellerAdapter(val completionBlock: (Traveller?) -> Unit, val moreCompletionBlock: (Traveller?) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<TravellerViewHolder>() {
        val items = ArrayList<TravellerItem>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TravellerViewHolder {
            val viewHolder = TravellerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list_traveller, parent, false), completionBlock, moreCompletionBlock)

            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: TravellerViewHolder, position: Int) {
            holder.bindData(items[position])
        }

        fun addAll(entities: ArrayList<TravellerItem>?) {
            entities?.let {
                items.clear()
                items.addAll(it)
            }
            notifyDataSetChanged()
        }
    }

    private class TravellerViewHolder(itemView: View, completionBlock: ((Traveller?) -> Unit), moreCompletionBlock: (Traveller?) -> Unit) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        private var traveller: TravellerItem? = null

        init {
            itemView.setOnClickListener { _ ->
                traveller?.let {
                    completionBlock(it.traveller)
                }
            }

            itemView.moreImageView.setOnClickListener {
                traveller?.let {
                    moreCompletionBlock(it.traveller)
                }
            }
        }

        fun bindData(item: TravellerItem) {
            traveller = item

            val firstName = item.traveller?.firstName ?: ""
            val lastName = item.traveller?.lastName ?: ""

            itemView.usernameTextView.text = "$firstName $lastName"
            itemView.iconSelectedImageView.visibility = View.GONE
            itemView.iconUnselectedImageView.visibility = View.VISIBLE
            item.traveller?.user?.let {
                Glide.with(itemView.context).load(it?.profilePicture?.largeImageUrl).into(itemView.profileImageView)
                itemView.iconSelectedImageView.visibility = View.VISIBLE
                itemView.iconUnselectedImageView.visibility = View.GONE
            }

            item.colorName?.let {
                if (it.startsWith("#")) {
                    val color = Color.parseColor(it)
                    itemView.setBackgroundColor(color)
                }
            } ?: kotlin.run {
                val color = Color.parseColor("#ffffff")
                itemView.setBackgroundColor(color)
            }

        }

    }
}
