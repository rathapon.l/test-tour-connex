package com.tourconnex.android.trip

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.trip.Trip
import com.tourconnex.android.framework.http.MainThreadCallback
import kotlinx.android.synthetic.main.activity_trip.*
import kotlinx.android.synthetic.main.view_toolbar.*
import retrofit2.Call
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.trip.Day
import com.tourconnex.android.domain.trip.Program
import com.tourconnex.android.domain.users.Guide
import com.tourconnex.android.domain.users.PageGuide
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection
import kotlinx.android.synthetic.main.view_list_guide.view.*
import kotlinx.android.synthetic.main.view_list_trip_program.view.*
import kotlinx.android.synthetic.main.view_section_program.view.*
import kotlin.collections.ArrayList

class TripActivity : AbstractActivity() {

    companion object {

        const val EXTRA_TRIP_ID = "EXTRA_TRIP_ID"
        const val EXTRA_TRIP_TITLE = "EXTRA_TRIP_TITLE"

        fun create(context: Context, tripId: String, tripName: String): Intent {
            return Intent(context, TripActivity::class.java).apply {
                putExtra(EXTRA_TRIP_ID, tripId)
                putExtra(EXTRA_TRIP_TITLE, tripName)
            }
        }
    }

    private lateinit var tripId: String
    private lateinit var tripTitle: String

    override fun onCreate(savedInstanceState: Bundle?) {
        tripId = intent.getStringExtra(EXTRA_TRIP_ID)
        tripTitle = intent.getStringExtra(EXTRA_TRIP_TITLE)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = tripTitle
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            val tripFragment = TripFragment.create(tripId)
            addFragment(R.id.tripContainerView, tripFragment, TripFragment.FRAGMENT_TAG)
        }
    }
}
