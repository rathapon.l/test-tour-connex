package com.tourconnex.android.trip

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.CaptureManager
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.trip.form.CheckInForm
import com.tourconnex.android.framework.http.MainThreadCallback
import kotlinx.android.synthetic.main.activity_trip_checkin.*
import kotlinx.android.synthetic.main.view_toolbar.*
import retrofit2.Call
import retrofit2.Response

interface OnScanTripActivityResult {
    fun onCheckInSuccess()
    fun onCancel()
}

class TripCheckInActivity : AbstractActivity() {

    companion object {

        private const val EXTRA_TRAVELLER_ID = "EXTRA_TRAVELLER_ID"
        private const val EXTRA_TRIP_ID = "EXTRA_TRIP_ID"

        fun create(context: Context, travellerId: String, tripId: String?): Intent {
            return Intent(context, TripCheckInActivity::class.java).apply {
                putExtra(EXTRA_TRAVELLER_ID, travellerId)
                putExtra(EXTRA_TRIP_ID, tripId)
            }
        }

        fun onActivityResult(resultCode: Int, data: Intent?, onScanTripActivityResult: OnScanTripActivityResult?) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    onScanTripActivityResult?.onCheckInSuccess()
                }

                Activity.RESULT_CANCELED -> {
                    onScanTripActivityResult?.onCancel()
                }
            }
        }
    }

    private var travellerId: String? = null
    private var tripId: String? = null

    private var capture: CaptureManager? = null
    private var callCheckIn: Call<SimpleResponse>? = null

    private var scanning = false

    private var scannedQRCode = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip_checkin)

        travellerId = intent.getStringExtra(EXTRA_TRAVELLER_ID)
        tripId = intent.getStringExtra(EXTRA_TRIP_ID)

        actionBarView.title = getString(R.string.trip_check_in_title)
        setSupportActionBar(actionBarView)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        barcodeScannerView.setStatusText("")

        capture = CaptureManager(this, barcodeScannerView)
        capture?.initializeFromIntent(intent, savedInstanceState)
//        capture?.decode()

        barcodeScannerView.decodeContinuous(object: BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult?) {
                //Log.d("tag", "found barcodeResult")
                handleQrCodeIfNeeded(result?.result?.text ?: "")
            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
                //Log.d("tag", "found possibleResultPoints")
            }
        })

        barcodeScannerView.setTorchOff()
    }

    private fun handleQrCodeIfNeeded(qrCode: String) {
        if (qrCode.isEmpty() || isQRCodeExist(qrCode)) {
            return
        }

        performRedeemQRCode(qrCode)
    }

    private fun performRedeemQRCode(qrCode: String) {
        val guideId = qrCode

        if (!scanning) {
            scanning = true

            val form = CheckInForm(guideId, travellerId)

            callCheckIn?.cancel()
            callCheckIn = getApiClient().checkIn(tripId, form)
            callCheckIn?.enqueue(object : MainThreadCallback<SimpleResponse>(this, simpleProgressBar) {
                override fun onSuccess(result: SimpleResponse?) {
                    scanning = false
                    showMessageDialog(getString(R.string.trip_success)) {
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                    //scannedQRCode.add(qrCode)
                }

                override fun onError(response: Response<SimpleResponse>?, error: Error?) {
                    scanning = false
                    scannedQRCode.add(qrCode)
                    showErrorDialog(error)
                }
            })
        }
    }

    private fun isQRCodeExist(qrCode: String): Boolean {
        return scannedQRCode.contains(qrCode)
    }

    override fun onResume() {
        super.onResume()
        capture?.onResume()
    }

    override fun onPause() {
        super.onPause()
        capture?.onPause()
    }

    override fun onDestroy() {
        callCheckIn?.cancel()
        super.onDestroy()
        capture?.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        capture?.onSaveInstanceState(outState)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event)
    }
}
