package com.tourconnex.android.trip

import android.Manifest
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.text.Html
import android.text.Spannable
import android.text.Spanned
import android.text.style.StrikethroughSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.images.ImagePreviewActivity
import com.tourconnex.android.domain.fragment.AbstractFragment
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.Room
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.flight.FlightItem
import com.tourconnex.android.domain.trip.*
import com.tourconnex.android.domain.trip.form.ManageSOSForm
import com.tourconnex.android.domain.users.Guide
import com.tourconnex.android.domain.users.PageGuide
import com.tourconnex.android.domain.users.Traveller
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.framework.widget.GridSpacingItemDecoration
import com.tourconnex.android.hotel.HotelActivity
import com.tourconnex.android.images.ImagesActivity
import com.tourconnex.android.pdf.PDFPreviewActivity
import com.tourconnex.android.pdf.PDFViewerActivity
import com.tourconnex.android.schedule.SchedulesActivity
import com.tourconnex.android.trip.adapter.TripDayImageAdapter
import com.tourconnex.android.user.GuideProfileActivity
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection
import kotlinx.android.synthetic.main.fragment_trip.*
import kotlinx.android.synthetic.main.fragment_trip.endDateTextView
import kotlinx.android.synthetic.main.fragment_trip.locationTextView
import kotlinx.android.synthetic.main.fragment_trip.numberOfMembersTextView
import kotlinx.android.synthetic.main.fragment_trip.startDateTextView
import kotlinx.android.synthetic.main.fragment_trip.titleTextView
import kotlinx.android.synthetic.main.fragment_trip.tripDetailTextView
import kotlinx.android.synthetic.main.view_list_flight.view.*
import kotlinx.android.synthetic.main.view_list_guide.view.*
import kotlinx.android.synthetic.main.view_list_hotel.view.*
import kotlinx.android.synthetic.main.view_list_trip_program.view.*
import kotlinx.android.synthetic.main.view_section_program.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Response
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

open class TripFragment : AbstractFragment() {

    companion object {
        const val EXTRA_TRIP_ID = "EXTRA_TRIP_ID"

        private const val RC_EXTERNAL_STORAGE = 23000

        private const val REQUEST_TRAVELLERS = 14340
        private const val REQUEST_SURVEY = 14341
        private const val REQUEST_EDIT_ROOM = 1432

        const val FRAGMENT_TAG = "TripFragment"

        fun create(tripId: String?): TripFragment {
            val tripFragment = TripFragment()
            // Supply index input as an argument.
            val args = Bundle()
            args.putString(EXTRA_TRIP_ID, tripId)
            tripFragment.arguments = args

            return tripFragment
        }
    }

    lateinit var tripId: String

    protected var trip: TripTraveller? = null
    protected var numberOfTraveller = 0

    private var callManageSOS: Call<SimpleResponse>? = null
    protected var callTrip: Call<TripTraveller>? = null
    private var callTripGuide: Call<PageGuide>? = null
    private var callManageRating: Call<SimpleResponse>? = null


    private val programSectionAdapter = SectionedRecyclerViewAdapter()

    private var activeTrip: TripTraveller? = null

    var onTripFragmentInactiveTripCompletionBlock: ((TripTraveller?) -> Unit)? = null

    private val flightAdapter = FlightAdapter {

    }

    private val guideAdapter = GuideAdapter { user ->
        user?.let { didSelectGuide(it) }
    }

    private var tripImagesPagerAdapter: TripImagesPagerAdapter? =  null

    private val hotelAdapter = HotelAdapter {
        didSelectHotel(it)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trip, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tripId = arguments?.getString(EXTRA_TRIP_ID, "") ?: ""

        guidesRecyclerView.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        guidesRecyclerView.adapter = guideAdapter

        hotelRecyclerView.layoutManager = LinearLayoutManager(activity)
        hotelRecyclerView.adapter = hotelAdapter

        guideContainerView.visibility = View.GONE

        programsRecyclerView.layoutManager = LinearLayoutManager(activity)
        programsRecyclerView.adapter = programSectionAdapter

        flightRecyclerView.layoutManager = LinearLayoutManager(activity)
        flightRecyclerView.adapter = flightAdapter

        programsContainerView.visibility = View.GONE
        setScheduleView.visibility = View.GONE
        doRatingView.visibility = View.GONE

        refreshingLayout.setOnRefreshListener {
            fetchData()
        }

        activeView.setOnClickListener {
            setActiveTrip(trip)
        }

        inactiveView.setOnClickListener {
            activity?.let {
                val builder = AlertDialog.Builder(it)
                builder.setMessage(R.string.trip_in_active_confirmation)
                builder.setPositiveButton(R.string.common_ok) { dialog, _ ->
                    dialog.dismiss()
                    setActiveTrip(null)
                    onTripFragmentInactiveTripCompletionBlock?.let {
                        it(trip)
                    }
                }
                builder.setNegativeButton(R.string.common_cancel) { dialog, _ -> dialog.cancel() }
                builder.show()
            }
        }

        val config = PRDownloaderConfig.newBuilder()
                .setReadTimeout(30_000)
                .setConnectTimeout(30_000)
                .build()
        PRDownloader.initialize(activity, config)

        val tourConnex = TourConnex.getInstance()
        activeTrip = tourConnex.getActiveTrip()
        updateActiveButtonUI()

        activity?.let {
            tripImagesPagerAdapter = TripImagesPagerAdapter(it) { image ->
                previewImage(image?.largeImageUrl)
            }
            tripImagesPagerAdapter?.let { adapter ->
                pagerView.adapter = adapter
                indicator.setViewPager(pagerView)
                pagerView.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

                    override fun onPageScrollStateChanged(state: Int) {

                    }

                    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                    }

                    override fun onPageSelected(position: Int) {

                    }
                })
            }
        }


        insuranceView.setOnClickListener { openInsurance() }
        doRatingView.setOnClickListener { openTripSurver() }
        downloadView.setOnClickListener { downloadTripFile() }
        previewView.setOnClickListener { previewTripFile() }
        setScheduleView.setOnClickListener { openSchedules() }
        agencyView.setOnClickListener { openLink(trip?.agency?.link ?: "") }

        fetchData()
    }

    private fun handleTravellerSection() {
        val isGuide = TourConnex.getInstance().getUser()?.isGuide
        val numberOfTravellers = trip?.trip?.numberOfTravellers ?: 0
        travellersView.visibility = if (isGuide == true) View.VISIBLE else View.GONE
        travellersTextView.text = getString(R.string.trip_travellers_prefix, numberOfTravellers.toString())
        travellersView.setOnClickListener { openTravellers() }
    }

    protected fun updateUI() {
        val tripInformation = trip?.trip
        titleTextView.text = tripInformation?.title

        if (numberOfTraveller < 0) {
            numberOfTraveller = 0
        }

        numberOfMembersTextView.text = numberOfTraveller.toString()
        locationTextView.text = tripInformation?.destination

        tripInformation?.description?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tripDetailTextView.text = Html.fromHtml(it, Html.FROM_HTML_MODE_COMPACT)
            } else {
                tripDetailTextView.text = Html.fromHtml(it)
            }
        }

        setDateText(startDateTextView, tripInformation?.startDateTime)
        setDateText(endDateTextView, tripInformation?.endDateTime)

        trip?.trip?.coverPhoto?.let {
            val images = ArrayList<Image>()
            images.add(it)
            tripImagesPagerAdapter?.addItem(images)
        }



        tripInformation?.days?.let {
            updateProgramsUI(ArrayList(it))
        }

        handleTravellerSection()
        updateActiveButtonUI()
        updateInsuranceUI()
        updateHotelsUI()
        updateSOSUI()
        updateRating()
        updateFlightUI()
        updateTripFileUI()
        updateSetScheduleUI()
        updateAgencyUI()
    }

    override fun onStart() {
        EventBus.getDefault().register(this)
        super.onStart()
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    private fun setActiveTrip(activeTrip: TripTraveller?) {
        val tourConnex = TourConnex.getInstance()
        tourConnex.setActiveTrip(activeTrip)
        this.activeTrip = activeTrip
        updateActiveButtonUI()
    }

    private fun openGallery() {

    }

    private fun downloadTripFile() {
        if (activity == null) {
            return
        }

        val loading = createProgressDialog(activity!!, getString(R.string.common_downloading))
        loading.show()

        PRDownloader.download(trip?.trip?.tripFile?.fileUrl ?: "" ,
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath,
            "${trip?.trip?.tripFile?.id ?: ""}.pdf").build().start(object :OnDownloadListener {
            override fun onDownloadComplete() {
                loading.dismiss()
                updateTripFileUI()
            }

            override fun onError(error: com.downloader.Error?) {
                loading.dismiss()
                showMessageDialog(error.toString())
            }
        })
    }

    private fun previewTripFile() {
        val file = getTripFile()
        activity?.let { startActivity(PDFPreviewActivity.create(it, file?.absolutePath, trip?.trip?.title)) }
    }

    @AfterPermissionGranted(RC_EXTERNAL_STORAGE)
    private fun updateTripFileUI() {
        previewView.visibility = View.GONE
        downloadView.visibility = View.GONE
        val tripFile = trip?.trip?.tripFile
        if (activity == null || tripFile == null) {
            return
        }

        val perms = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(activity!!, *perms)) {

            val file = getTripFile()
            if (file?.exists() == true) {
                previewView.visibility = View.VISIBLE
            } else {
                downloadView.visibility = View.VISIBLE
            }
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.trip_external_storage),
                RC_EXTERNAL_STORAGE, *perms)
        }
    }

    private fun updateSetScheduleUI() {
        setScheduleView.visibility = if (TourConnex.getInstance().getActiveTrip()?.isTourLeader() == true) View.VISIBLE else View.GONE
    }

    private fun getTripFile(): File? {
        val tripFile = trip?.trip?.tripFile
        var file: File? = null

        if (tripFile != null) {
            val root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            file = File(root.absolutePath + "/${tripFile.id}.pdf")
        }

        return file
    }

    private fun updateActiveButtonUI() {
        val isActive = trip?.id == activeTrip?.id
        inactiveView.visibility = if (isActive) View.GONE else View.GONE
        activeView.visibility = if (isActive) View.GONE else View.VISIBLE
    }

    private fun updateInsuranceUI() {
        insuranceView.visibility = View.GONE
        trip?.trip?.insurance?.let {
            insuranceView.visibility = View.VISIBLE
            insuranceTitleTextView.text = it.name
            insuranceDescriptionTextView.text = it.description
        }
    }

    private fun updateProgramsUI(days: ArrayList<Day>) {
        programsContainerView.visibility = if (days.filter { it.programs?.size ?: 0 > 0 }.isNotEmpty()) View.VISIBLE else View.GONE
        programSectionAdapter.removeAllSections()
        for (day in days) {
            val title = day.title ?: ""
            if (!day.programs.isNullOrEmpty()) {
                val section = ProgramSection(ArrayList(day.programs), title, day.images) { image, isNormally ->
                    if (isNormally) {
                        previewImage(image?.largeImageUrl)
                    } else {
                        showMoreImage(day.images)
                    }
                }

                programSectionAdapter.addSection(section)
            }
        }
    }

    private fun previewImage(url: String?) {
        activity?.let { startActivity(ImagePreviewActivity.create(it, url)) }
    }

    private fun showMoreImage(images: List<Image>?) {
        images?.let {
            val imageArrayList = ArrayList<Image>()
            imageArrayList.addAll(it)

            activity?.let { activity ->
                startActivity(ImagesActivity.create(activity, imageArrayList))
            }
        }
    }

    private fun updateHotelsUI() {
        var hotelItems = trip?.trip?.days?.map {
            TripHotelItem(it,
                it.hotel,
                it.rooms?.filter { room ->
                    room.travellers?.filter { travellerRoom ->
                        travellerRoom.id == trip?.id
                    }?.count() ?: 0 > 0
                }?.firstOrNull())
        }

        hotelItems = hotelItems?.filter { it.hotel != null }

        hotelItems?.let {
            hotelAdapter.addAll(ArrayList(it))
        }
    }

    private fun updateSOSUI() {
        val isTourLeader = TourConnex.getInstance().getActiveTrip()?.isTourLeader()
        activeSOSContainerView.isVisible = isTourLeader == true
        sosSwitch.setOnCheckedChangeListener(null)
        sosSwitch.isChecked = trip?.trip?.sos == true
        sosSwitch.setOnCheckedChangeListener(checkSOSListener)
    }

    private fun updateRating() {
        val isTourLeader = TourConnex.getInstance().getActiveTrip()?.isTourLeader()
        activeRatingContainerView.visibility = if (isTourLeader == true) View.VISIBLE else View.GONE
        ratingSwitch.setOnCheckedChangeListener(null)
        ratingSwitch.isChecked = trip?.trip?.rating == true
        ratingSwitch.setOnCheckedChangeListener(checkRatingListener)

        // user
        doRatingView.visibility = if (isTourLeader == false && trip?.checkedIn == true && trip?.trip?.rating == true) View.VISIBLE else View.GONE
    }

    private fun updateAgencyUI() {
        val agency = trip?.agency
        agencyView.visibility = if (agency != null) View.VISIBLE else View.GONE
        agencyTextView?.visibility = if (agency?.name?.isNotEmpty() == true) View.VISIBLE else View.GONE
        agencyTextView.text = agency?.name
        agencyDescriptionTextView.visibility = if (agency?.description?.isNotEmpty() != null) View.VISIBLE else View.GONE
        agencyDescriptionTextView.text = agency?.description
    }

    private fun updateFlightUI() {
        val flightItems = getFlightItems()
        flightsContainerView.visibility = if (flightItems.size > 0) View.VISIBLE else View.GONE
        flightAdapter.addAll(flightItems)
    }

    private fun getFlightItems(): ArrayList<FlightItem> {
        val flightItems = ArrayList<FlightItem>()

        if (trip?.flight != null) {
            val departureItems = trip?.flight?.departFlights?.mapNotNull { FlightItem(it, true, activeTrip?.flightInfo) }
            departureItems?.let {
                flightItems.addAll(it)
            }
            val returnItems = trip?.flight?.returnFlights?.mapNotNull { FlightItem(it, false, activeTrip?.flightInfo) }
            returnItems?.let {
                flightItems.addAll(it)
            }

        } else if (trip?.trip?.tripFlight != null) {
            val departureItems = trip?.trip?.tripFlight?.firstOrNull()?.departFlights?.mapNotNull { FlightItem(it, true, activeTrip?.flightInfo) }
            departureItems?.let {
                flightItems.addAll(it)
            }
            val returnItems = trip?.trip?.tripFlight?.firstOrNull()?.returnFlights?.mapNotNull { FlightItem(it, false, activeTrip?.flightInfo) }
            returnItems?.let {
                flightItems.addAll(it)
            }
        }

        return flightItems
    }

    private var checkSOSListener = object : CompoundButton.OnCheckedChangeListener {
        override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
            performSOS()
        }
    }

    private var checkRatingListener = object : CompoundButton.OnCheckedChangeListener {
        override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
            performRating()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun didReciveAssignTravellerEvent(s: AssignTravellerEvent) {
        fetchData()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun didReciveAssignGuideEvent(s: AssignGuideEvent) {
        fetchData()
    }

    private fun performRating() {
        if (activity == null) {
            return
        }

        val form = ManageSOSForm(ratingSwitch.isChecked)

        callManageRating?.cancel()
        callManageRating = getApiClient().manageRating(trip?.trip?.id, form)
        callManageRating?.enqueue(object: MainThreadCallback<SimpleResponse>(activity!!) {
            override fun onSuccess(result: SimpleResponse?) {

            }

            override fun onError(response: Response<SimpleResponse>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    private fun performSOS() {
        if (activity == null) {
            return
        }

        val form = ManageSOSForm(sosSwitch.isChecked)
        callManageSOS?.cancel()
        callManageSOS = getApiClient().manageSOS(trip?.trip?.id, form)
        callManageSOS?.enqueue(object: MainThreadCallback<SimpleResponse>(activity!!) {
            override fun onSuccess(result: SimpleResponse?) {

            }

            override fun onError(response: Response<SimpleResponse>?, error: Error?) {

            }
        })
    }

    private fun setDateText(textView: TextView?, date: Date?) {
        textView?.visibility = View.GONE
        date?.let {
            textView?.visibility = View.VISIBLE
            val format = SimpleDateFormat("dd MMMM yyyy")
            textView?.text = format.format(it)
        } ?: kotlin.run {

        }
    }

    protected fun fetchGuides() {
        callTripGuide?.cancel()
        callTripGuide = getApiClient().getTripGuides(trip?.trip?.id ?: "")

        callTripGuide?.enqueue(object: MainThreadCallback<PageGuide>(activity) {
            override fun onSuccess(result: PageGuide?) {
                result?.entities?.let {
                    if (it.size > 0) {
                        guideContainerView.visibility = View.VISIBLE
                        val sortedList = it.sortedByDescending { traveller -> traveller.guideType == 1 }
                        guideAdapter.addAll(ArrayList(sortedList))
                    }
                }
            }

            override fun onError(response: Response<PageGuide>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    open fun fetchData() {
        callTrip?.cancel()
        callTrip = getApiClient().getTrip(tripId)
        callTrip?.enqueue(object: MainThreadCallback<TripTraveller>(activity) {
            override fun onSuccess(result: TripTraveller?) {
                refreshingLayout.isRefreshing = false
                this@TripFragment.trip = result
                this@TripFragment.numberOfTraveller = result?.trip?.numberOfTravellers ?: 0
                fetchGuides()
                updateUI()
            }

            override fun onError(response: Response<TripTraveller>?, error: Error?) {
                refreshingLayout.isRefreshing = false
                showErrorDialog(error)
            }
        })
    }

    private fun didSelectGuide(guide: Traveller) {
        activity?.let { startActivity(GuideProfileActivity.create(it, guide, trip?.trip?.id)) }
    }

    private fun didSelectHotel(tripHotelItem: TripHotelItem?) {
        val isTourLeader = TourConnex.getInstance().getActiveTrip()?.isTourLeader()
        if (isTourLeader == true) {
            activity?.let { openHotelOption(it, tripHotelItem) }
        } else {
            openHotel(tripHotelItem)
        }
    }

    private fun openLink(url: String) {
        if (activity == null) {
            return
        }

        val builder = CustomTabsIntent.Builder()
        builder.setToolbarColor(ContextCompat.getColor(activity!!, R.color.white))
        val customTabsIntent = builder.build()
        var urlToParse = url
        if (!url.startsWith("http://") || !url.startsWith("https://")) {
            urlToParse = "http://$url"
        }

        if (URLUtil.isValidUrl(urlToParse)) {
            customTabsIntent.launchUrl(activity!!, Uri.parse(urlToParse))
        }
    }

    private fun openHotel(tripHotelItem: TripHotelItem?) {
        activity?.let { startActivity(HotelActivity.create(it, tripHotelItem?.hotel)) }
    }

    private fun openEditRoom(tripHotelItem: TripHotelItem?) {
        val rooms = tripHotelItem?.day?.rooms ?: listOf()
        val arrayListOfRooms = ArrayList<Room>()
        arrayListOfRooms.addAll(rooms)
        activity?.let {startActivityForResult(EditRoomsActivity.create(it, trip?.trip?.id, arrayListOfRooms, tripHotelItem?.day?.id),REQUEST_EDIT_ROOM)}
    }

    private fun openHotelOption(context: Context, tripHotelItem: TripHotelItem?) {
        val builder = AlertDialog.Builder(context)
        builder.setItems(R.array.hotel_selection_option) { dialog, which ->
            when (which) {
                0 -> openHotel(tripHotelItem)
                1 -> openEditRoom(tripHotelItem)
            }
            dialog.dismiss()
        }
        builder.show()
    }

    private fun openInsurance() {
        val insurance =  trip?.trip?.insurance
        insurance?.file?.let { file ->
            activity?.let { startActivity(PDFViewerActivity.create(it, insurance.name, file)) }

        }
    }

    private fun openTravellers() {
        trip?.trip?.id?.let {
            activity?.let { activity ->
                startActivityForResult(TravellersActivity.create(activity, it), REQUEST_TRAVELLERS)
            }
        }
    }

    private fun openTripSurver() {
        activity?.let { startActivityForResult(TripSurveyActivity.create(it, activeTrip?.trip?.id, activeTrip?.id), REQUEST_SURVEY) }
    }

    private fun openSchedules() {
        activity?.let { startActivity(SchedulesActivity.create(it, trip?.trip?.id)) }
    }

    private fun updateRooms(rooms: ArrayList<Room>?, dayId: Int?) {
        if (trip?.trip?.days == null) {
            return
        }

        val index = trip?.trip?.days?.indexOfFirst { it.id == dayId } ?: -1
        if (index != -1) {
            trip?.trip?.days!![index].rooms = rooms
            updateHotelsUI()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_TRAVELLERS -> {
                TravellersActivity.onActivityResult(resultCode, data, object: OnTravellersActivityResult {
                    override fun onAssignUserSuccess() {
                        numberOfTraveller += 1
                        updateUI()
                    }

                    override fun onRemoveTravellerSuccess() {
                        numberOfTraveller -= 1
                        updateUI()
                    }

                    override fun onCancel() {

                    }
                })
            }

            REQUEST_SURVEY -> {
                TripSurveyActivity.onActivityResult(resultCode, data, object : OnTripSurveyActivityResult {
                    override fun onSendFeedbackSuccess() {
                        showMessageDialog(getString(R.string.trip_survey_success_message))
                    }

                    override fun onCancel() {

                    }
                })
            }

            REQUEST_EDIT_ROOM -> {
                EditRoomsActivity.onActivityResult(resultCode, data, object: OnEditRoomsActivityResult{
                    override fun onEditedRooms(rooms: ArrayList<Room>?, dayId: Int?) {
                        updateRooms(rooms, dayId)
                    }

                    override fun onCancel() {

                    }
                })
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onDestroy() {
        callTrip?.cancel()
        callManageSOS?.cancel()
        callManageRating?.cancel()
        callTripGuide?.cancel()
        super.onDestroy()
    }

    private class GuideAdapter(val completionBlock: (Traveller?) -> Unit) : RecyclerView.Adapter<GuideViewHolder>() {
        val items = ArrayList<Traveller>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GuideViewHolder {
            val viewHolder = GuideViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list_guide, parent, false), completionBlock)

            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: GuideViewHolder, position: Int) {
            holder.bindData(items[position])
        }

        fun addAll(entities: ArrayList<Traveller>?) {
            entities?.let {
                items.clear()
                items.addAll(it)
            }
            notifyDataSetChanged()
        }
    }

    private class GuideViewHolder(itemView: View, completionBlock: ((Traveller?) -> Unit)) : RecyclerView.ViewHolder(itemView) {
        private var user: Traveller? = null

        init {
            itemView.setOnClickListener { _ ->
                user?.let {
                    completionBlock(it)
                }
            }
        }

        fun bindData(item: Traveller) {
            user = item
            Glide.with(itemView.context).load(item.user?.profilePicture?.thumbnailImageUrl).into(itemView.profileImageView)
            itemView.usernameTextView.text = item.user?.firstName

            itemView.tourLeaderView.visibility = if (item.guideType == 1) View.VISIBLE else View.INVISIBLE
        }
    }

    private class ProgramSection(val items: ArrayList<Program>, val title: String, val images: List<Image>?, val completionBlock: (Image?, Boolean) -> (Unit)): StatelessSection(

        SectionParameters.builder().itemResourceId(R.layout.view_list_trip_program).headerResourceId(R.layout.view_section_program).build()) {

        override fun getContentItemsTotal(): Int {
            return items.size
        }

        override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
            val itemViewHolder = holder as? ProgramViewHolder
            itemViewHolder?.bindData(items[position], position)
        }

        override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder?) {
            val sectionViewHolder = holder as? SectionViewHolder
            sectionViewHolder?.bindData(title, images)
        }

        override fun getItemViewHolder(view: View?): RecyclerView.ViewHolder {
            return ProgramViewHolder(view!!)
        }

        override fun getHeaderViewHolder(view: View?): RecyclerView.ViewHolder {
            return SectionViewHolder(view!!, completionBlock)
        }
    }

    class SectionViewHolder(itemView: View, completionBlock: (Image?, Boolean) -> (Unit)): RecyclerView.ViewHolder(itemView) {

        private val adapter = TripDayImageAdapter(completionBlock)

        private val maxNumberOfImages = 5

        init {
            val spanCount =  maxNumberOfImages
            val spacingInPixels = itemView.context.resources.getDimensionPixelSize(R.dimen.spaceNormal)
            val includeEdge = true
            itemView.dayImagesRecyclerView.adapter = adapter
            itemView.dayImagesRecyclerView.layoutManager = GridLayoutManager(itemView.context, maxNumberOfImages)
            itemView.dayImagesRecyclerView.addItemDecoration(GridSpacingItemDecoration(spanCount, spacingInPixels, includeEdge))
        }

        fun bindData(title: String, images: List<Image>?) {
            itemView.sectionTitleTextView.text = title

            images?.take(maxNumberOfImages)?.let {
                adapter.maxNumberOfPhoto = maxNumberOfImages
                adapter.diffOfPhotoNumberDiffOfPhotoNumber = (images?.size ?: 0) - maxNumberOfImages
                adapter.addAll(it)
            }
        }
    }

    class ProgramViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        fun bindData(program: Program, position: Int) {
            val seq = position + 1
            itemView.segTextView.text = seq.toString()
            itemView.timeTextView.text = program.title

            itemView.htmlTextView.setHtml(program.description ?: "")
        }
    }

    private class HotelAdapter(val completionBlock: (TripHotelItem?) -> Unit) : RecyclerView.Adapter<HotelViewHolder>() {
        val items = ArrayList<TripHotelItem>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HotelViewHolder {
            val viewHolder = HotelViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list_hotel, parent, false), completionBlock)

            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: HotelViewHolder, position: Int) {
            holder.bindData(items[position])
        }

        fun addAll(entities: ArrayList<TripHotelItem>?) {
            entities?.let {
                items.clear()
                items.addAll(it)
            }
            notifyDataSetChanged()
        }
    }

    private class HotelViewHolder(itemView: View, completionBlock: ((TripHotelItem?) -> Unit)) : RecyclerView.ViewHolder(itemView) {
        private var hotelItem: TripHotelItem? = null

        init {
            itemView.setOnClickListener { _ ->
                hotelItem?.let {
                    completionBlock(it)
                }
            }
        }

        fun bindData(item: TripHotelItem) {
            hotelItem = item
            val hotel = item.hotel
            Glide.with(itemView.context).load(item.hotel?.coverPhoto?.thumbnailImageUrl).into(itemView.hotelImageView)

            itemView.hotelLatestRoomTextView.visibility = View.GONE

            itemView.hotelNametextView.text = hotel?.name
            itemView.hotelRemarkTextView.text = item.room?.remark

            val lestestRoomNumber = item.room?.lestestRoomNumber ?: ""

            if (lestestRoomNumber.isNotEmpty()) {
                itemView.hotelLatestRoomTextView.visibility = View.VISIBLE

                itemView.hotelLatestRoomTextView.setText(lestestRoomNumber, TextView.BufferType.SPANNABLE)
                val spannable = itemView.hotelLatestRoomTextView.text as? Spannable
                spannable?.setSpan(StrikethroughSpan(), 0, lestestRoomNumber.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            }

            itemView.roomContainerView.visibility = if (item.room == null) View.GONE else View.VISIBLE

            itemView.roomNumberTextView.text = item.room?.roomNumber
            var travellerNames = item.room?.travellers?.map {
                val title = it.title ?: ""
                val firstName = it.firstName ?: ""
                val lastName = it.lastName ?: ""

                "$title $firstName $lastName"
            }

            travellerNames = travellerNames?.filter { !it.isNullOrEmpty() }

            itemView.travellersTextView.text = travellerNames?.joinToString("\n")
        }
    }

    private class FlightAdapter(val completionBlock: (FlightItem?) -> Unit) : RecyclerView.Adapter<FlightViewHolder>() {
        val items = ArrayList<FlightItem>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightViewHolder {
            val viewHolder = FlightViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list_flight, parent, false), completionBlock)

            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: FlightViewHolder, position: Int) {
            holder.bindData(items[position])
        }

        fun addAll(entities: ArrayList<FlightItem>?) {
            entities?.let {
                items.clear()
                items.addAll(it)
            }
            notifyDataSetChanged()
        }
    }

    private class FlightViewHolder(itemView: View, completionBlock: ((FlightItem?) -> Unit)) : RecyclerView.ViewHolder(itemView) {
        private var flightItem: FlightItem? = null

        init {
            itemView.setOnClickListener { _ ->
                flightItem?.let {
                    completionBlock(it)
                }
            }
        }

        fun bindData(item: FlightItem) {
            flightItem = item
            itemView.seatContainerView.visibility = View.GONE
            itemView.returnView.visibility = if (item.isDepartureFight) View.GONE else View.VISIBLE
            itemView.departureView.visibility = if (item.isDepartureFight) View.VISIBLE else View.GONE

            itemView.airlineTextView.text = item.flight?.airline
            itemView.flightDateTextView.text = item.flight?.date
            itemView.flightTimeTextView.text = item.flight?.time
            itemView.destinationTextView.text = "${item.flight?.flightNo ?: ""} ${item.flight?.goTo ?: ""}"

            if (item.isDepartureFight) {
                item.flightInfo?.departSeat?.let {
                    itemView.seatContainerView.visibility = View.VISIBLE
                    itemView.seatTextView.text = it
                }
            } else {
                item.flightInfo?.returnSeat?.let {
                    itemView.seatContainerView.visibility = View.VISIBLE
                    itemView.seatTextView.text = it
                }
            }
        }
    }

    class TripImagesPagerAdapter(private val mContext: Context, completionBlock: ((Image?) -> Unit)?) : PagerAdapter() {
        private var completionBlock: ((image: Image?) -> Unit)? = null
        private var images = java.util.ArrayList<Image>()

        init {
            this.completionBlock = completionBlock
        }

        override fun instantiateItem(collection: ViewGroup, position: Int): Any {
            val inflater = LayoutInflater.from(mContext)
            val layout = inflater.inflate(R.layout.view_list_trip_image, collection, false) as ViewGroup
            val imageView = layout.findViewById<ImageView>(R.id.imageView)
            val item = images[position]
            Glide.with(mContext).load(item.thumbnailImageUrl).into(imageView)

            layout.setOnClickListener {
                completionBlock?.let { completionBlock ->
                    completionBlock(item)
                }
            }

            collection.addView(layout)
            return layout
        }

        override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
            collection.removeView(view as View)
        }

        override fun getCount(): Int {
            return images.count()
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object`
        }

        fun addItem(entities: java.util.ArrayList<Image>?) {
            images.clear()
            entities?.let {
                images = java.util.ArrayList<Image>()
                images.addAll(it)
                notifyDataSetChanged()
            }
        }
    }
}
