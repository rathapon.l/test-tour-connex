package com.tourconnex.android.trip

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.common.SimpleResponse
import com.tourconnex.android.domain.trip.FeedbackItem
import com.tourconnex.android.domain.trip.PageFeedback
import com.tourconnex.android.domain.trip.form.FeedbackForm
import com.tourconnex.android.domain.trip.form.SendFeedbackForm
import com.tourconnex.android.framework.http.MainThreadCallback
import kotlinx.android.synthetic.main.activity_trip_survey.*
import kotlinx.android.synthetic.main.view_list_feedback.view.*
import kotlinx.android.synthetic.main.view_toolbar.*
import retrofit2.Call
import retrofit2.Response

interface OnTripSurveyActivityResult {
    fun onSendFeedbackSuccess()
    fun onCancel()
}

class TripSurveyActivity : AbstractActivity() {

    companion object {

        private const val EXTRA_TRIP_ID = "EXTRA_TRIP_ID"
        private const val EXTRA_TRAVELLER_ID = "EXTRA_TRAVELLER_ID"

        fun create(context: Context, tripId: String?, travellerId: String?): Intent {
            return Intent(context, TripSurveyActivity::class.java).apply {
                putExtra(EXTRA_TRIP_ID, tripId)
                putExtra(EXTRA_TRAVELLER_ID, travellerId)
            }
        }

        fun onActivityResult(resultCode: Int, data: Intent?, onTripSurveyActivityResult: OnTripSurveyActivityResult) {
            when (resultCode) {
                Activity.RESULT_OK -> { onTripSurveyActivityResult.onSendFeedbackSuccess() }
                Activity.RESULT_CANCELED -> { onTripSurveyActivityResult.onCancel()}
            }
        }
    }

    private var tripId: String? = null
    private var travellerId: String? = null

    private var callFeedback: Call<PageFeedback>? = null
    private var callSendFeedback: Call<SimpleResponse>? = null
    private val adapter = FeedbackAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip_survey)

        tripId = intent?.getStringExtra(EXTRA_TRIP_ID)
        travellerId = intent?.getStringExtra(EXTRA_TRAVELLER_ID)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.trip_survey_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        recyclerView.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recyclerView.adapter = adapter

        sendButton.setOnClickListener { performSendFeedback() }

        fetchData()
    }

    private fun fetchData() {
        callFeedback?.cancel()
        callFeedback = getApiClient().getFeedback(tripId)
        callFeedback?.enqueue(object : MainThreadCallback<PageFeedback>(this, simpleProgressBar) {
            override fun onSuccess(result: PageFeedback?) {
                result?.entities?.let {
                    val feedbackItems = ArrayList<FeedbackItem>()
                    feedbackItems.addAll(it.mapNotNull { FeedbackItem(it, 0) })
                    adapter.addAll(feedbackItems)
                }
            }

            override fun onError(response: Response<PageFeedback>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    private fun performSendFeedback() {
        val suggested = suggestedEditText.text.toString()
        val feedbackForms = adapter.items.map { FeedbackForm(it.feedback?.type ?: -1, it.rating) }
        val form = SendFeedbackForm(feedbackForms, suggested)
        callSendFeedback?.cancel()
        callSendFeedback = getApiClient().sendFeedback(travellerId, form)
        callSendFeedback?.enqueue(object : MainThreadCallback<SimpleResponse>(this, simpleProgressBar){
            override fun onSuccess(result: SimpleResponse?) {
                result?.let {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }

            override fun onError(response: Response<SimpleResponse>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    override fun onDestroy() {
        callFeedback?.cancel()
        callSendFeedback?.cancel()
        super.onDestroy()
    }

    private class FeedbackAdapter() : androidx.recyclerview.widget.RecyclerView.Adapter<FeedbackViewHolder>() {
        val items = ArrayList<FeedbackItem>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedbackViewHolder {
            val viewHolder = FeedbackViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list_feedback, parent, false))

            return viewHolder
        }

        override fun getItemCount(): Int {
            return items.size
        }

        override fun onBindViewHolder(holder: FeedbackViewHolder, position: Int) {
            holder.bindData(items[position])
        }

        fun addAll(entities: ArrayList<FeedbackItem>?) {
            entities?.let {
                items.clear()
                items.addAll(it)
            }
            notifyDataSetChanged()
        }
    }

    private class FeedbackViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
        private var feedbackItem: FeedbackItem? = null

        init {
            itemView.rating.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
                feedbackItem?.rating = rating.toInt()
            }
        }

        fun bindData(item: FeedbackItem) {
            feedbackItem = item
            feedbackItem?.feedback?.let {
                itemView.titleTextView.text = it.title?.en
            }

            itemView.rating.rating = item.rating.toFloat()
        }

    }
}
