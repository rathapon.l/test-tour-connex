package com.tourconnex.android.trip.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tourconnex.android.R
import com.tourconnex.android.domain.trip.Image
import kotlinx.android.synthetic.main.view_list_day_image.view.*

class TripDayImageAdapter(val onItemClick: (Image, Boolean) -> Unit) : RecyclerView.Adapter<ProductViewHolder>() {
    val items = ArrayList<Image>()
    var maxNumberOfPhoto = -1
    var diffOfPhotoNumberDiffOfPhotoNumber = -1

    fun addAll(list: List<Image>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_list_day_image, parent, false), onItemClick)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val image = items[position]
        holder.bindData(image, position, maxNumberOfPhoto, diffOfPhotoNumberDiffOfPhotoNumber)
    }
}

class ProductViewHolder(itemView: View, val onItemClick: (Image, Boolean) -> Unit) : RecyclerView.ViewHolder(itemView) {
    private var image: Image? = null

    init {
//        itemView.setOnClickListener {
//            i?.let(onItemClick(it))
//        }
    }

    fun bindData(item: Image, position: Int, maxNumberOfPhotos: Int, numberOfDiffPhotos: Int) {
        this.image = item
        Glide.with(itemView.context).load(item.thumbnailImageUrl).into(itemView.imageView)

        val isSeeMoreItem = position == (maxNumberOfPhotos - 1)
        itemView.imageOverlay.visibility = if (isSeeMoreItem) View.VISIBLE else View.GONE
        itemView.moreTextView.text = "$numberOfDiffPhotos +"

        itemView.setOnClickListener {
            image?.let {onItemClick(it, !isSeeMoreItem)}
        }
    }
}