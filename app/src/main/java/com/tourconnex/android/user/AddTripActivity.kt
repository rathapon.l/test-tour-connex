package com.tourconnex.android.user

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.CaptureManager
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.trip.TripTraveller
import com.tourconnex.android.framework.http.MainThreadCallback
import kotlinx.android.synthetic.main.activity_add_trip.*
import kotlinx.android.synthetic.main.view_toolbar.*
import retrofit2.Call
import retrofit2.Response

interface OnAddTripActivityResult {
    fun onAddedTrip(traveller: TripTraveller?)
    fun onCancel()
}

class AddTripActivity : AbstractActivity() {

    companion object {

        private const val RESULT_TRIP = "RESULT_TRIP"

        fun create(context: Context): Intent {
            return Intent(context, AddTripActivity::class.java)
        }

        fun onActivityResult(resultCode: Int, data: Intent?, onAddTripActivityResult: OnAddTripActivityResult?) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val traveller = data?.getParcelableExtra<TripTraveller>(RESULT_TRIP)
                    onAddTripActivityResult?.onAddedTrip(traveller)
                }

                Activity.RESULT_CANCELED -> {
                    onAddTripActivityResult?.onCancel()
                }
            }
        }
    }

    private var capture: CaptureManager? = null
    private var callAddTrip: Call<TripTraveller>? = null

    private var scanning = false

    private var scannedQRCode = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_trip)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.trip_add_trip)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        barcodeScannerView.setStatusText("")

        capture = CaptureManager(this, barcodeScannerView)
        capture?.initializeFromIntent(intent, savedInstanceState)

        barcodeScannerView.decodeContinuous(object: BarcodeCallback {
            override fun barcodeResult(result: BarcodeResult?) {
                Log.d("tag", "found barcodeResult")
                handleQrCodeIfNeeded(result?.result?.text ?: "")
            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
                Log.d("tag", "found possibleResultPoints")
            }
        })

        barcodeScannerView.setTorchOff()
    }

    private fun handleQrCodeIfNeeded(qrCode: String) {
        if (qrCode.isEmpty() || isQRCodeExist(qrCode)) {
            return
        }

        performRedeemQRCode(qrCode)
    }

    private fun performRedeemQRCode(qrCode: String) {
        if (!scanning) {
            scanning = true
            callAddTrip?.cancel()
            callAddTrip = getApiClient().getTripPreview(qrCode)
            callAddTrip?.enqueue(object: MainThreadCallback<TripTraveller>(this, simpleProgressBar) {
                override fun onSuccess(result: TripTraveller?) {
                    scanning = false
                    //scannedQRCode.add(qrCode)
                    setResult(Activity.RESULT_OK, Intent().apply {
                        putExtra(RESULT_TRIP, result)
                    })

                    finish()
                }

                override fun onError(response: Response<TripTraveller>?, error: Error?) {
                    scanning = false
                    scannedQRCode.add(qrCode)
                    showErrorDialog(error)
                }
            })
        }
    }

    private fun isQRCodeExist(qrCode: String): Boolean {
        return scannedQRCode.contains(qrCode)
    }

    override fun onResume() {
        super.onResume()
        capture?.onResume()
    }

    override fun onPause() {
        super.onPause()
        capture?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        capture?.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        capture?.onSaveInstanceState(outState)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event)
    }
}
