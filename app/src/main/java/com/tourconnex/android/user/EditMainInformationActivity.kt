package com.tourconnex.android.user

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.users.User
import com.tourconnex.android.domain.users.form.EditProfileForm
import com.tourconnex.android.framework.http.MainThreadCallback
import kotlinx.android.synthetic.main.activity_edit_main_information.*
import kotlinx.android.synthetic.main.view_toolbar.*
import retrofit2.Call
import retrofit2.Response

interface OnEditMainInformationActivityResult {
    fun onEditProfileSuccess(user: User)
    fun onCancel()
}

class EditMainInformationActivity : AbstractActivity() {

    companion object {

        private const val EXTRA_USER = "EXTRA_USER"
        private const val RESULT_USER = "EXTRA_USER"

        fun create(context: Context, user: User?): Intent {
            return Intent(context, EditMainInformationActivity::class.java).apply {
                putExtra(EXTRA_USER, user)
            }
        }

        fun onActivityResult(resultCode: Int, data: Intent?, onEditMainInformationActivity: OnEditMainInformationActivityResult) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.getParcelableExtra<User>(RESULT_USER)?.let {
                        onEditMainInformationActivity.onEditProfileSuccess(it)
                    }
                }
                Activity.RESULT_CANCELED -> {
                    onEditMainInformationActivity.onCancel()
                }
            }
        }
    }

    private var user: User? = null
    private var callEditProfile: Call<User>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_main_information)

        user = intent?.getParcelableExtra(EXTRA_USER)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.edit_main_information_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        bindData()
        submitButton.setOnClickListener { performUpdateUserInformation() }
    }

    private fun getValidator(): String? {
        val citizenId = citizenTextView.text.toString()
        val passportNumber = passportNumberTextView.text.toString()

        var message: String? = null

        if (citizenId.isNullOrEmpty()) {
            message = getString(R.string.edit_main_passport_fill_citizen_id)
        } else if (citizenId.length < 13) {
            message = getString(R.string.edit_main_passport_citizen_id_length_mismatch)
        } else if (passportNumber.isNullOrEmpty()) {
            message = getString(R.string.edit_main_passport_fill_passport_number)
        }

        return message
    }

    private fun bindData() {
        citizenTextView.setText(user?.citizenId)
        passportNumberTextView.setText(user?.passportNumber)
    }

    private fun performUpdateUserInformation() {
        val validator = getValidator()
        if (validator != null) {
            showMessageDialog(validator)
        } else {
            val citizenId = citizenTextView.text.toString()
            val passportNumber = passportNumberTextView.text.toString()
            val form = EditProfileForm(citizenId, passportNumber)
            callEditProfile?.cancel()
            callEditProfile = getApiClient().editProfile(form)
            callEditProfile?.enqueue(object: MainThreadCallback<User>(this, simpleProgressBar) {
                override fun onSuccess(result: User?) {
                    result?.let {
                        setResult(Activity.RESULT_OK, Intent().apply {
                            putExtra(RESULT_USER, it)
                        })
                        finish()
                    }
                }

                override fun onError(response: Response<User>?, error: Error?) {
                    showErrorDialog(error)
                }
            })
        }
    }

    override fun onDestroy() {
        callEditProfile?.cancel()
        super.onDestroy()
    }
}
