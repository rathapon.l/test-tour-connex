package com.tourconnex.android.user


import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.users.User
import com.tourconnex.android.domain.users.form.EditGuideProfileForm
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.framework.utils.ImageUtils
import com.tourconnex.android.framework.utils.StringUtils
import kotlinx.android.synthetic.main.activity_edit_guide_profile.*
import kotlinx.android.synthetic.main.view_toolbar.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Response
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class EditProfileActivity : AbstractActivity() {

    companion object {
        private const val REQUEST_GALLERY = 112
        private const val REQUEST_CAMERA = 113
        private const val RC_CAMERA_AND_EXTERNAL_STORAGE = 164


        fun create(context: Context): Intent {
            return Intent(context, EditProfileActivity::class.java)
        }
    }

    private var file: File? = null
    private var callEdit: Call<User>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_guide_profile)

        setSupportActionBar(actionBarView)
        supportActionBar?.title = getString(R.string.common_edit)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        profileImageView.setOnClickListener { openAddPhotoOptions() }

        emailView.visibility = if (isGuideOrTourLeader()) View.VISIBLE else View.GONE
        nationalityView.visibility = if (isGuideOrTourLeader()) View.VISIBLE else View.GONE
        passportNumberView.visibility = if (isGuideOrTourLeader()) View.VISIBLE else View.GONE
        issueAtView.visibility = if (isGuideOrTourLeader()) View.VISIBLE else View.GONE

        bindData()
    }

    private fun bindData() {
        val user = TourConnex.getInstance().getUser()

        Glide.with(this).load(user?.profilePicture?.largeImageUrl).into(profileImageView)

        setText(user?.firstName , firstnameEditText)
        setText(user?.lastName , lastnameEditText)
        setText(user?.email , emailEditText)
        setText(user?.nationality, nationalityEditText)
        setText(user?.passportNumber, passportNumberEditText)
        setText(user?.passportCountryIssue, issueAtEditText)
    }

    private fun setDate(date: Date?, editText: EditText) {
        date?.let {
            val format = SimpleDateFormat("dd/MM/yy")
            editText.setText(format.format(it))
        } ?: kotlin.run {
            editText.setText("-")
        }
    }

    private fun isGuideOrTourLeader(): Boolean {
        val user = TourConnex.getInstance().getUser()
        val activeTrip = TourConnex?.getInstance().getActiveTrip()
        return (user?.isGuide == true || activeTrip?.guideType == 1)
    }

    private fun isValidToEdit(): Boolean {
        var message: String? = null
        var valid = true
        if (emailEditText.text.isNullOrEmpty() && isGuideOrTourLeader()) {
            message = getString(R.string.login_error_empty_username)
        } else if (!StringUtils.isEmailValid(emailEditText.text.toString().trim()) && isGuideOrTourLeader()) {
            message = getString(R.string.login_error_invalid_email)
        } else if (firstnameEditText.text.isNullOrEmpty()) {
            message = getString(R.string.register_error_fill_first_name)
        } else if (lastnameEditText.text.isNullOrEmpty()) {
            message = getString(R.string.register_error_fill_last_name)
        }

        message?.let {
            valid = false
            showMessageDialog(it)
        }

        return valid
    }

    private fun openAddPhotoOptions() {
        val builder = AlertDialog.Builder(this)
        builder.setItems(R.array.picture_selection_option) { dialog, which ->
            when (which) {
                0 -> openCamera()
                1 -> openGallery()
            }
            dialog.dismiss()
        }
        builder.show()
    }

    private fun openGallery() {
        EasyImage.openGallery(this, REQUEST_GALLERY)
    }

    @AfterPermissionGranted(RC_CAMERA_AND_EXTERNAL_STORAGE)
    private fun openCamera() {
        val perms = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            EasyImage.openCameraForImage(this, REQUEST_CAMERA)
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.register_camera_permission),
                RC_CAMERA_AND_EXTERNAL_STORAGE, *perms)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, onImagePickedCallback)
    }

    private val onImagePickedCallback = object : DefaultCallback() {
        override fun onImagesPicked(files: MutableList<File>, p1: EasyImage.ImageSource?, type: Int) {
            val f = files.firstOrNull()
            f?.let {
                this@EditProfileActivity.file = ImageUtils.createRotatedAndResizedImage(it)
                this@EditProfileActivity.file?.let { nonNullFile ->
                    performUpdateUserImage(nonNullFile)
                }
            }
        }
    }

    private fun updateImageProfileUI() {
        file?.let {
            Glide.with(this).load(file).apply(RequestOptions.centerCropTransform()).into(profileImageView)
        } ?: kotlin.run {
            Glide.with(this).load(R.drawable.icon_placeholder).apply(RequestOptions.centerInsideTransform()).into(profileImageView)
        }
    }

    private fun performUpdateUserImage(file: File) {
        val filePart = createRequestBody(file)
        callEdit?.cancel()
        callEdit = getApiClient().editGuideProfileImage(filePart)
        callEdit?.enqueue(object: MainThreadCallback<User>(this, simpleProgressBar) {
            override fun onSuccess(result: User?) {
                result?.let {
                    TourConnex.getInstance().setUser(it)
                }

                updateImageProfileUI()
                setResult(Activity.RESULT_OK)
            }

            override fun onError(response: Response<User>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    private fun createRequestBody(file: File): MultipartBody.Part {
        val body = RequestBody.create(MediaType.parse("image/*"), file)
        return MultipartBody.Part.createFormData("image", file.name, body)
    }

    private fun performEditProfile() {

        if (isValidToEdit()) {
            val form = EditGuideProfileForm(
                emailEditText.text.toString().trim(),
                firstnameEditText.text.toString().trim(),
                lastnameEditText.text.toString().trim(),
                passportNumberEditText.text.toString().trim(),
                nationalityEditText.text.toString().trim(),
                issueAtEditText.text.toString().trim()
                )
            callEdit?.cancel()
            callEdit = getApiClient().editGuideProfile(form)
            callEdit?.enqueue(object: MainThreadCallback<User>(this, simpleProgressBar) {
                override fun onSuccess(result: User?) {
                    result?.let {
                        TourConnex.getInstance().setUser(it)
                    }

                    setResult(Activity.RESULT_OK)
                    finish()
                }

                override fun onError(response: Response<User>?, error: Error?) {
                    showErrorDialog(error)
                }
            })
        }


    }

    private fun setText(text: String?, editText: EditText) {
        if (text?.isNullOrEmpty() == false) {
            editText.setText(text)
        } else {
            editText.setText("-")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.common_save, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun save() {
        performEditProfile()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.actionSave -> {
                save()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        callEdit?.cancel()
        super.onDestroy()
    }
}
