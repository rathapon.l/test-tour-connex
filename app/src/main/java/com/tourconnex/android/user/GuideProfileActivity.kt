package com.tourconnex.android.user

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.bumptech.glide.Glide
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.users.Guide
import com.tourconnex.android.domain.users.Traveller
import kotlinx.android.synthetic.main.activity_user_profile.*
import java.text.SimpleDateFormat
import java.util.*

class GuideProfileActivity : UserProfileActivity() {

    companion object {

        private const val EXTRA_GUIDE = "EXTRA_TRAVELLER"
        private const val EXTRA_TRIP_ID = "EXTRA_TRIP_ID"

        fun create(context: Context, guide: Traveller?, tripId: String?): Intent {
            return Intent(context, GuideProfileActivity::class.java).apply {
                putExtra(EXTRA_GUIDE, guide)
                putExtra(EXTRA_TRIP_ID, tripId)
            }
        }
    }

    private var guide: Traveller? = null
    private var tripId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        guide = intent?.getParcelableExtra(EXTRA_GUIDE)
        tripId = intent?.getStringExtra(EXTRA_TRIP_ID)

        supportActionBar?.title = guide?.user?.getFullName()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        roomRequestContainerView.visibility = View.GONE
        seatRequestContainerView.visibility = View.GONE
        specialRequestContainerView.visibility = View.GONE

        val user = TourConnex.getInstance().getUser()
        actionToUserView.visibility = if (guide?.user?.id == user?.id) View.GONE else View.VISIBLE

        if (user?.isGuide == true) {
            messageView.visibility = if (guide?.guideType == 1) View.VISIBLE else View.GONE
        }

        updateUI()
    }

    private fun updateUI() {
        val userObj = guide

        Glide.with(this).load(userObj?.user?.profilePicture?.largeImageUrl).into(profileImageView)

        nameTextView.text = userObj?.user?.getFullName()
        setText(userObj?.nationality, nationalityTextView)
        setDate(userObj?.dateOfBirth, dateOfBirthTextView)
        setText(userObj?.passportNumber, passportNumberTextView)
        setText(userObj?.passportCountryIssue, issueAtTextView)
        setDate(userObj?.passportIssueDate, issueDateTextView)
        setDate(userObj?.passportExpiryDate, issueExpireAtTextView)

//        val tourConnex = TourConnex.getInstance()
//        val tripTraveller = tourConnex.getActiveTrip()
//        val user = tourConnex.getUser()
//        val isGuide = user?.isGuide == true && tripTraveller?.isTourLeader() != true
//        val isOpponentIsTourLeader = guide.
//        messageView.is
    }

    private fun setDate(date: Date?, textView: TextView) {
        date?.let {
            val format = SimpleDateFormat("dd/MM/yy")
            textView.text = format.format(it)
        } ?: kotlin.run {
            textView.text = "-"
        }
    }

    private fun setText(text: String?, textView: TextView) {
        if (text?.isNullOrEmpty() == false) {
            textView.text = text
        } else {
            textView.text = "-"
        }
    }

    override fun onSelectCall() {

    }

    override fun onSelectMessage() {
        createConversation(guide, tripId, true)
    }
}
