package com.tourconnex.android.user

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.domain.trip.AssignTravellerEvent
import com.tourconnex.android.domain.trip.TripTraveller
import com.tourconnex.android.domain.users.User
import kotlinx.android.synthetic.main.activity_user_qrcode.*
import kotlinx.android.synthetic.main.view_toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

interface OnMyQRCodeActivityResult {
    fun onAddedTripTraveller(tripTraveller: TripTraveller?)
    fun onCheckedIn(travellerId: String?)
    fun onCancel()
}

class MyQRCodeActivity : UserQRCodeActivity() {

    companion object {
        private const val EXTRA_USER = "EXTRA_USER"

        private const val REQUEST_ADD_TRIP = 12343

        private const val RESULT_TRAVELLER = "RESULT_TRAVELLER"
        private const val RESULT_TRAVELLER_ID = "RESULT_TRAVELLER_ID"

        fun create(context: Context, user: User?): Intent {
            return Intent(context, MyQRCodeActivity::class.java).apply {
                putExtra(EXTRA_USER, user)
            }
        }

        fun onActivityResult(resultCode: Int, data: Intent?, onMyQRCodeActivityResult: OnMyQRCodeActivityResult) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val traveller = data?.getParcelableExtra<TripTraveller>(RESULT_TRAVELLER)
                    val travellerId = data?.getStringExtra(RESULT_TRAVELLER_ID)
                    if (traveller != null) {
                        onMyQRCodeActivityResult.onAddedTripTraveller(traveller)
                    } else if (travellerId != null) {
                        onMyQRCodeActivityResult.onCheckedIn(travellerId)
                    }
                }

                Activity.RESULT_CANCELED -> {
                    onMyQRCodeActivityResult.onCancel()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        user = intent.getParcelableExtra(EXTRA_USER)
        super.onCreate(savedInstanceState)


        addTripButton.setOnClickListener { openAddTrip() }
    }

    override fun updateUI() {
        super.updateUI()
        supportActionBar?.title = getString(R.string.my_qr_code_title)
    }

    private fun openAddTrip() {
        startActivityForResult(AddTripActivity.create(this), REQUEST_ADD_TRIP)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun didReciveAssignTravellerEvent(s: AssignTravellerEvent) {
        setResult(Activity.RESULT_OK, Intent().apply {
            putExtra(RESULT_TRAVELLER_ID, s.contentId)
        })
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_ADD_TRIP -> {
                AddTripActivity.onActivityResult(resultCode, data, object: OnAddTripActivityResult {
                    override fun onAddedTrip(traveller: TripTraveller?) {
                        setResult(Activity.RESULT_OK, Intent().apply {
                            putExtra(RESULT_TRAVELLER, traveller)
                        })
                        finish()
                    }

                    override fun onCancel() {

                    }
                })
            }
        }

    }
}
