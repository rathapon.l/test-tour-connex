package com.tourconnex.android.user

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.bumptech.glide.Glide
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.color.ColorSelectionActivity
import com.tourconnex.android.color.OnColorSelectionActivityResult
import com.tourconnex.android.domain.common.Location
import com.tourconnex.android.domain.common.Type
import com.tourconnex.android.domain.users.Traveller
import com.tourconnex.android.domain.users.UserInterface
import com.tourconnex.android.framework.http.MainThreadCallback
import com.tourconnex.android.map.TravellerLocationActivity
import com.tourconnex.android.trip.AddTravellersActivity
import com.tourconnex.android.trip.OnAddTravellerResult
import kotlinx.android.synthetic.main.activity_user_profile.*
import retrofit2.Call
import java.text.SimpleDateFormat
import java.util.*

interface OnTravellerProfileActivityResult {
    fun onSelectColor(colorType: Type?, travellerId: String?)
    fun onCancel()
}

class TravellerProfileActivity : UserProfileActivity() {

    companion object {

        private const val EXTRA_TRAVELLER = "EXTRA_TRAVELLER"
        private const val EXTRA_TRIP_ID = "EXTRA_TRIP_ID"
        private const val EXTRA_LOCATION = "EXTRA_LOCATION"
        private const val EXTRA_TRAVELLER_ID = "EXTRA_TRAVELLER_ID"

        private const val RESULT_COLOR_TYPE = "RESULT_COLOR_TYPE"
        private const val RESULT_TRAVELLER_ID = "RESULT_TRAVELLER_ID"

        private const val REQUEST_ADD_TRAVELLERS = 12333
        private const val REQUEST_CHANGE_GROUP_COLOR = 12334

        fun create(context: Context, traveller: Traveller?, travellerId: String?, tripId: String?, location: Location?): Intent {
            return Intent(context, TravellerProfileActivity::class.java).apply {
                putExtra(EXTRA_TRAVELLER, traveller)
                putExtra(EXTRA_TRIP_ID, tripId)
                putExtra(EXTRA_LOCATION, location)
                putExtra(EXTRA_TRAVELLER_ID, travellerId)
            }
        }

        fun onActivityResult(resultCode: Int, data: Intent?, onTravellerProfileActivityResult: OnTravellerProfileActivityResult?) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val colorType = data?.getParcelableExtra<Type>(RESULT_COLOR_TYPE)
                    val traverllerId = data?.getStringExtra(RESULT_TRAVELLER_ID)
                    onTravellerProfileActivityResult?.onSelectColor(colorType, traverllerId)
                }

                Activity.RESULT_CANCELED -> {
                    onTravellerProfileActivityResult?.onCancel()
                }
            }
        }
    }

    private var traveller: Traveller? = null
    private var tripId: String? = null
    private var location: Location? = null
    private var callTraveller: Call<Traveller>? = null
    private var travellerId: String? = null

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.common_more_vertical, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        traveller = intent?.getParcelableExtra(EXTRA_TRAVELLER)
        tripId = intent?.getStringExtra(EXTRA_TRIP_ID)
        location = intent?.getParcelableExtra(EXTRA_LOCATION)
        travellerId = intent?.getStringExtra(EXTRA_TRAVELLER_ID)

        supportActionBar?.title = traveller?.getFullName()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val user = TourConnex.getInstance().getUser()
        actionToUserView.visibility = if (traveller?.user?.id == user?.id) View.GONE else View.VISIBLE

        qrCodeView.visibility = View.VISIBLE
        locationView.visibility = if (location != null) View.VISIBLE else View.GONE

        locationView.setOnClickListener { openTravellerLocation() }

        val isTourLeader = TourConnex.getInstance().getActiveTrip()?.isTourLeader()
        messageView.visibility = if (isTourLeader == true) View.VISIBLE else View.GONE

        updateUI()
        fetchData()
    }

    private fun fetchData() {
        callTraveller?.cancel()
        callTraveller = getApiClient().getTraveller(travellerId)
        callTraveller?.enqueue(object: MainThreadCallback<Traveller>(this, simpleProgressBar) {
            override fun onSuccess(result: Traveller?) {
                this@TravellerProfileActivity.traveller = result
                updateUI()
            }
        })
    }

    private fun updateUI() {
        val userObj = traveller

        Glide.with(this).load(userObj?.user?.profilePicture?.largeImageUrl).into(profileImageView)

        nameTextView.text = userObj?.getFullName()
        setText(userObj?.nationality, nationalityTextView)
        setDate(userObj?.dateOfBirth, dateOfBirthTextView)
        setText(userObj?.passportNumber, passportNumberTextView)
        setText(userObj?.passportCountryIssue, issueAtTextView)
        setDate(userObj?.passportIssueDate, issueDateTextView)
        setDate(userObj?.passportExpiryDate, issueExpireAtTextView)
        setText(userObj?.roomRequest, roomRequestTextView)
        setText(userObj?.seatRequest, seatRequestTextView)
        setText(userObj?.specialRequest, specialRequestTextView)
    }

    private fun handleSetTravellerGroupColor() {
        startActivityForResult(ColorSelectionActivity.create(this), REQUEST_CHANGE_GROUP_COLOR)
    }

    override fun onSelectScanQR() {
        startActivityForResult(AddTravellersActivity.create(this, tripId, traveller), REQUEST_ADD_TRAVELLERS)
    }

    private fun openTravellerLocation() {
        startActivity(TravellerLocationActivity.create(this, traveller, location))
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.actionMore -> {
                handleSetTravellerGroupColor()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_ADD_TRAVELLERS -> {
                AddTravellersActivity.onActivityResult(resultCode, data, object: OnAddTravellerResult {
                    override fun onAssignUserSuccess() {
                        showMessageDialog(getString(R.string.traveller_add_traveller_success))
                    }

                    override fun onCancel() {

                    }
                })
            }

            REQUEST_CHANGE_GROUP_COLOR -> {
                ColorSelectionActivity.onActivityResult(resultCode, data, object: OnColorSelectionActivityResult {
                    override fun onCancel() {

                    }

                    override fun onSelectColorType(colorType: Type?) {
                        setResult(Activity.RESULT_OK, Intent().apply {
                            putExtra(RESULT_COLOR_TYPE, colorType)
                            putExtra(RESULT_TRAVELLER_ID, traveller?.id)
                        })
                    }
                })
            }
        }
    }

    private fun setDate(date: Date?, textView: TextView) {
        date?.let {
            val format = SimpleDateFormat("dd/MM/yy")
            textView.text = format.format(it)
        } ?: kotlin.run {
            textView.text = "-"
        }
    }

    private fun setText(text: String?, textView: TextView) {
        if (text?.isNullOrEmpty() == false) {
            textView.text = text
        } else {
            textView.text = "-"
        }
    }

    override fun onSelectCall() {

    }

    override fun onSelectMessage() {
        createConversation(traveller, tripId, false)
    }

    override fun onDestroy() {
        callTraveller?.cancel()
        super.onDestroy()
    }
}
