package com.tourconnex.android.user

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.tourconnex.android.R
import com.tourconnex.android.TourConnex
import com.tourconnex.android.conversation.ConversationActivity
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.common.Error
import com.tourconnex.android.domain.conversation.Conversation
import com.tourconnex.android.domain.conversation.form.CreateConversationForm
import com.tourconnex.android.domain.users.Traveller
import com.tourconnex.android.domain.users.User
import com.tourconnex.android.domain.users.UserInterface
import com.tourconnex.android.framework.http.MainThreadCallback
import kotlinx.android.synthetic.main.activity_user_profile.*
import kotlinx.android.synthetic.main.view_toolbar.*
import retrofit2.Call
import retrofit2.Response

open class UserProfileActivity : AbstractActivity() {

    private var callConversation: Call<Conversation>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        setSupportActionBar(actionBarView)

        callView.setOnClickListener { onSelectCall() }
        messageView.setOnClickListener { onSelectMessage() }
        qrCodeView.setOnClickListener { onSelectScanQR() }

        qrCodeView.visibility = View.GONE
        locationView.visibility = View.GONE


        val isGuide = TourConnex?.getInstance().getActiveTrip()?.isTourLeader() == true || TourConnex.getInstance().getUser()?.isGuide == true
        nationalityView.visibility = if (isGuide) View.VISIBLE else View.GONE
        dateOfBirthView.visibility = if (isGuide) View.VISIBLE else View.GONE
        passportNumberView.visibility = if (isGuide) View.VISIBLE else View.GONE
        issueAtView.visibility = if (isGuide) View.VISIBLE else View.GONE
        issueDateView.visibility = if (isGuide) View.VISIBLE else View.GONE
        expiryView.visibility = if (isGuide) View.VISIBLE else View.GONE
        roomRequestContainerView.visibility = if (isGuide) View.VISIBLE else View.GONE
        seatRequestContainerView.visibility = if (isGuide) View.VISIBLE else View.GONE
        specialRequestContainerView.visibility = if (isGuide) View.VISIBLE else View.GONE
    }

    open fun onSelectCall() {

    }

    open fun onSelectMessage() {

    }

    open fun onSelectScanQR() {

    }

    open fun createConversation(user: Traveller?, tripId: String?, isGuide: Boolean) {
        val form = CreateConversationForm(tripId, user?.user?.id, isGuide)
        callConversation?.cancel()
        callConversation = getApiClient().createConversation(form)
        callConversation?.enqueue(object: MainThreadCallback<Conversation>(this, simpleProgressBar) {
            override fun onSuccess(result: Conversation?) {
                didGetConversation(result, user)
            }

            override fun onError(response: Response<Conversation>?, error: Error?) {
                showErrorDialog(error)
            }
        })
    }

    private fun didGetConversation(conversation: Conversation?, user: Traveller?) {
        var list: ArrayList<Traveller>? = null
        user?.let {
            list = arrayListOf(it)
        }

        startActivity(ConversationActivity.create(this, conversation?.id, user?.user?.getFullName(), list))
    }

    override fun onDestroy() {
        callConversation?.cancel()
        super.onDestroy()
    }
}
