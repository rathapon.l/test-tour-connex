package com.tourconnex.android.user

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.tourconnex.android.R
import com.tourconnex.android.domain.activity.AbstractActivity
import com.tourconnex.android.domain.users.User
import com.tourconnex.android.framework.utils.ImageUtils
import kotlinx.android.synthetic.main.activity_user_qrcode.*
import kotlinx.android.synthetic.main.view_toolbar.*

open class UserQRCodeActivity : AbstractActivity() {

    companion object {
        private const val EXTRA_USER = "EXTRA_USER"
        fun create(context: Context, user: User?): Intent {
            return Intent(context, UserQRCodeActivity::class.java).apply {
                putExtra(EXTRA_USER, user)
            }
        }
    }

    protected var user: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_qrcode)

        user = intent.getParcelableExtra(EXTRA_USER)

        setSupportActionBar(actionBarView)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        updateUI()
    }

    open fun updateUI() {
        user?.let {
            val firstName = it.firstName ?: ""
            var lastname: String? = it.lastName ?: ""

            supportActionBar?.title = "$firstName $lastname"

            val bmp = ImageUtils.genQRFromFreeText(it.id, 400, 400)
            bmp?.let { bitmap ->
                qrCodeImageView.setImageBitmap(bitmap)
            }
        }

    }
}
